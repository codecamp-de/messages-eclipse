package de.codecamp.messages.eclipse.ide.services;


import de.codecamp.messages.shared.conf.BundleMapping;
import de.codecamp.messages.shared.model.MessageModule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.eclipse.core.resources.IProject;


public class IdeModule
  extends
    AbstractItem
  implements
    MessageModule
{

  private final List<Locale> targetLocales;

  private final List<BundleMapping> bundleMappings;

  private final List<String> importedModules;

  private final String messageFormat;


  public IdeModule(IProject project, MessageModule module, boolean imported, boolean fromWorkspace)
  {
    this(project, module.getModuleName(), module.getTargetLocales(), module.getBundleMappings(),
         module.getImportedModules(), module.getMessageFormat(), imported, fromWorkspace);
  }

  public IdeModule(IProject project, String moduleName, List<Locale> targetLocales,
      List<BundleMapping> bundleMappings, List<String> importedModules, String messageFormat,
      boolean imported, boolean fromWorkspace)
  {
    super(project, moduleName, imported, fromWorkspace);

    if (bundleMappings == null)
      this.bundleMappings = Collections.emptyList();
    else
      this.bundleMappings = Collections.unmodifiableList(new ArrayList<>(bundleMappings));

    if (importedModules == null)
      this.targetLocales = Collections.emptyList();
    else
      this.targetLocales = Collections.unmodifiableList(new ArrayList<>(targetLocales));

    if (importedModules == null)
      this.importedModules = Collections.emptyList();
    else
      this.importedModules = Collections.unmodifiableList(new ArrayList<>(importedModules));

    this.messageFormat = messageFormat;
  }


  @Override
  public List<Locale> getTargetLocales()
  {
    return targetLocales;
  }

  @Override
  public List<BundleMapping> getBundleMappings()
  {
    return bundleMappings;
  }

  @Override
  public List<String> getImportedModules()
  {
    return importedModules;
  }

  @Override
  public String getMessageFormat()
  {
    return messageFormat;
  }


  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + getProject().hashCode();
    result = prime * result + getModuleName().hashCode();
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;

    IdeModule other = (IdeModule) obj;

    if (!getProject().equals(other.getProject()))
      return false;
    if (!getModuleName().equals(other.getModuleName()))
      return false;

    return true;
  }

  @Override
  public String toString()
  {
    return new ToStringBuilder(this).append("project", getProject().getName())
        .append("module", getModuleName()).append("isImported", isImported())
        .append("isFromWorkspace", isFromWorkspace()).build();
  }

}
