package de.codecamp.messages.eclipse.ide.ui;


import java.util.Locale;


public class IdeMessage
{

  private final String code;

  private final Locale locale;

  private final String message;


  public IdeMessage(String code, Locale locale, String message)
  {
    this.code = code;
    this.locale = locale;
    this.message = message;
  }


  public String getCode()
  {
    return code;
  }

  public Locale getLocale()
  {
    return locale;
  }

  public String getMessage()
  {
    return message;
  }

}
