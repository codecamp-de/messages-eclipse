package de.codecamp.messages.eclipse.ide.services;


import static com.googlecode.cqengine.query.QueryFactory.and;
import static com.googlecode.cqengine.query.QueryFactory.applyThresholds;
import static com.googlecode.cqengine.query.QueryFactory.ascending;
import static com.googlecode.cqengine.query.QueryFactory.attribute;
import static com.googlecode.cqengine.query.QueryFactory.equal;
import static com.googlecode.cqengine.query.QueryFactory.in;
import static com.googlecode.cqengine.query.QueryFactory.nullableAttribute;
import static com.googlecode.cqengine.query.QueryFactory.orderBy;
import static com.googlecode.cqengine.query.QueryFactory.queryOptions;
import static com.googlecode.cqengine.query.QueryFactory.threshold;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.support.SimpleFunction;
import com.googlecode.cqengine.index.hash.HashIndex;
import com.googlecode.cqengine.index.navigable.NavigableIndex;
import com.googlecode.cqengine.query.Query;
import com.googlecode.cqengine.query.logical.And;
import com.googlecode.cqengine.query.option.EngineThresholds;
import com.googlecode.cqengine.resultset.ResultSet;
import de.codecamp.messages.eclipse.ide.build.MessageKeyChangeHandler;
import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import de.codecamp.messages.eclipse.ide.util.jdt.JavaProjectResourceLoader;
import de.codecamp.messages.eclipse.ide.util.jdt.JdtUtils;
import de.codecamp.messages.eclipse.ide.validation.BundleFileValidator;
import de.codecamp.messages.shared.bundle.BundleFile;
import de.codecamp.messages.shared.bundle.MessageBundleManager;
import de.codecamp.messages.shared.bundle.MessageBundleManager.BundleFileCoordinates;
import de.codecamp.messages.shared.conf.ProjectConf;
import de.codecamp.messages.shared.conf.ProjectConfException;
import de.codecamp.messages.shared.model.AbstractPersistableData.PersistableDataException;
import de.codecamp.messages.shared.model.MessageKeyIndex;
import de.codecamp.messages.shared.validation.MessageValidationUtils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.apt.core.util.AptConfig;
import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IElementChangedListener;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaElementDelta;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.ServiceScope;


@Component(service = MessageKeyService.class, scope = ServiceScope.SINGLETON)
public class MessageKeyServiceImpl
  implements
    MessageKeyService,
    IResourceChangeListener,
    IElementChangedListener
{

  // private static final String DTE_EDITOR_ID =
  // org.eclipse.ui.editors.text.EditorsUI.DEFAULT_TEXT_EDITOR_ID;

  private static final Attribute<IdeMessageKey, IProject> MESSAGEKEY_ATTR_PROJECT =
      attribute(IdeMessageKey.class, IProject.class, "project", IdeMessageKey::getProject);

  private static final Attribute<IdeMessageKey, String> MESSAGEKEY_ATTR_MODULE_NAME =
      attribute(IdeMessageKey.class, String.class, "moduleName", IdeMessageKey::getModuleName);

  private static final Attribute<IdeMessageKey, String> MESSAGEKEY_ATTR_SOURCE_TYPE =
      attribute(IdeMessageKey.class, String.class, "sourceType", IdeMessageKey::getSourceType);

  private static final Attribute<IdeMessageKey, String> MESSAGEKEY_ATTR_MESSAGE_KEY_CODE =
      attribute(IdeMessageKey.class, String.class, "messageKeyCode", IdeMessageKey::getCode);

  private static final Attribute<IdeMessageKey, Boolean> MESSAGEKEY_ATTR_IMPORTED =
      attribute(IdeMessageKey.class, Boolean.class, "imported", IdeMessageKey::isImported);


  private static final Attribute<IMarker, IProject> MARKER_ATTR_PROJECT =
      attribute(IMarker.class, IProject.class, "project",
          (SimpleFunction<IMarker, IProject>) m -> m.getResource().getProject());

  private static final Attribute<IMarker, String> MARKER_ATTR_SOURCE_TYPE = nullableAttribute(
      IMarker.class, String.class, "sourceType", (SimpleFunction<IMarker, String>) m -> m
          .getAttribute(BundleFileValidator.MARKER_ATTR_SOURCE_TYPE, null));

  private static final Attribute<IMarker, String> MARKER_ATTR_MESSAGE_KEY = nullableAttribute(
      IMarker.class, String.class, "messageKey", (SimpleFunction<IMarker, String>) m -> m
          .getAttribute(BundleFileValidator.MARKER_ATTR_MESSAGE_KEY, null));


  private final ListenerList<MessageKeyListener> listeners =
      new ListenerList<>(ListenerList.IDENTITY);

  // project -> MessageKeyIndex; the message keys directly declared within this project/module
  private final ConcurrentMap<IProject, MessageKeyIndex> messageKeyIndexPerProject =
      new ConcurrentHashMap<>();

  private final ConcurrentMap<IProject, IFile> messageKeyIndexFilePerProject =
      new ConcurrentHashMap<>();


  private final IndexedCollection<IdeMessageKey> messageKeys = new ConcurrentIndexedCollection<>();

  private final ConcurrentMap<IProject, Map<String, IdeModule>> importedModulesPerProject =
      new ConcurrentHashMap<>();


  // project -> bundlename:locale -> message key -> message available (i.e. not empty)
  private final ConcurrentMap<IProject, Map<String, Map<String, Boolean>>> availableMessagesPerProject =
      new ConcurrentHashMap<>();

  private final SetMultimap<IProject, IProject> importedLocalProjectsRequiredBy =
      HashMultimap.create();


  private final IndexedCollection<IMarker> markers = new ConcurrentIndexedCollection<>();

  private MessageKeyChangeHandler messageKeyChangeHandler;


  public MessageKeyServiceImpl()
  {
    messageKeys.addIndex(HashIndex.onAttribute(MESSAGEKEY_ATTR_PROJECT));
    messageKeys.addIndex(HashIndex.onAttribute(MESSAGEKEY_ATTR_MODULE_NAME));
    messageKeys.addIndex(NavigableIndex.onAttribute(MESSAGEKEY_ATTR_SOURCE_TYPE));
    messageKeys.addIndex(NavigableIndex.onAttribute(MESSAGEKEY_ATTR_MESSAGE_KEY_CODE));
    messageKeys.addIndex(HashIndex.onAttribute(MESSAGEKEY_ATTR_IMPORTED));

    markers.addIndex(HashIndex.onAttribute(MARKER_ATTR_PROJECT));
    markers.addIndex(HashIndex.onAttribute(MARKER_ATTR_SOURCE_TYPE));
    markers.addIndex(HashIndex.onAttribute(MARKER_ATTR_MESSAGE_KEY));
  }


  @Activate
  protected void activate()
  {
    ResourcesPlugin.getWorkspace().addResourceChangeListener(this,
        IResourceChangeEvent.POST_CHANGE | IResourceChangeEvent.POST_BUILD
            | IResourceChangeEvent.PRE_BUILD | IResourceChangeEvent.PRE_REFRESH
            | IResourceChangeEvent.PRE_CLOSE | IResourceChangeEvent.PRE_DELETE);

    JavaCore.addElementChangedListener(this, ElementChangedEvent.POST_CHANGE);

    WorkspaceJob initJob = new WorkspaceJob("Initializing message key service.")
    {

      @Override
      public IStatus runInWorkspace(IProgressMonitor monitor)
        throws CoreException
      {
        for (IProject project : ResourcesPlugin.getWorkspace().getRoot().getProjects())
        {
          refreshProject(project);
        }

        return Status.OK_STATUS;
      }

    };
    initJob.schedule();

    messageKeyChangeHandler = new MessageKeyChangeHandler(this);
    addMessageKeyChangeListener(messageKeyChangeHandler);
  }

  @Deactivate
  protected void deactivate()
  {
    if (messageKeyChangeHandler != null)
      removeMessageKeyChangeListener(messageKeyChangeHandler);

    ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);

    JavaCore.removeElementChangedListener(this);
  }


  @Override
  public void addMessageKeyChangeListener(MessageKeyListener listener)
  {
    listeners.add(listener);
  }

  @Override
  public void removeMessageKeyChangeListener(MessageKeyListener listener)
  {
    listeners.remove(listener);
  }

  protected void fireProjectAddedEvent(IProject project)
  {
    fireMessageKeyChangeEvent(MessageKeyEvent.createProjectAdded(project));
  }

  protected void fireProjectRemovedEvent(IProject project)
  {
    fireMessageKeyChangeEvent(MessageKeyEvent.createProjectRemoved(project));
  }

  protected void fireProjectRefreshedEvent(IProject project)
  {
    fireMessageKeyChangeEvent(MessageKeyEvent.createProjectRefreshed(project));
  }

  protected void fireProjectUpdatedEvent(IProject project)
  {
    fireMessageKeyChangeEvent(MessageKeyEvent.createProjectUpdated(project));
  }

  protected void fireProblemsUpdatedEvent(IProject project)
  {
    fireMessageKeyChangeEvent(MessageKeyEvent.createProblemsUpdated(project));
  }

  private void fireDeltaMessageKeysUpdatedEvent(IProject project, Stream<IdeMessageKey> newKeys,
      Stream<IdeMessageKey> oldKeys)
  {
    Set<IdeMessageKey> addedKeys = newKeys.collect(toSet());
    Set<IdeMessageKey> removedKeys;

    if (oldKeys != null)
    {
      removedKeys = oldKeys.collect(toSet());

      Set<IdeMessageKey> unchangedKeys = new HashSet<>(addedKeys);
      unchangedKeys.retainAll(removedKeys);

      addedKeys.removeAll(unchangedKeys);
      removedKeys.removeAll(unchangedKeys);
    }
    else
    {
      removedKeys = null;
    }

    if ((addedKeys == null || addedKeys.isEmpty())
        && (removedKeys == null || removedKeys.isEmpty()))
    {
      return;
    }

    MessageKeyEvent event =
        MessageKeyEvent.createMessageKeysUpdated(project, addedKeys, removedKeys);

    fireMessageKeyChangeEvent(event);
  }


  private void fireMessageKeyChangeEvent(MessageKeyEvent event)
  {
    for (MessageKeyListener listener : listeners)
    {
      try
      {
        listener.onUpdate(event);
      }
      catch (RuntimeException ex)
      {
        StatusUtils.logError(getClass(),
            "An error occurred while notifying MessageKeyChangeListeners.", ex);
      }
    }
  }


  @Override
  public Optional<IdeProjectConf> getProjectConf(IProject project)
  {
    return getProjectConf(project, false);
  }

  @Override
  public Optional<IdeProjectConf> getProjectConf(IProject project, boolean resolveImports)
  {
    try
    {
      return getProjectConfChecked(project, resolveImports);
    }
    catch (@SuppressWarnings("unused") ProjectConfException ex)
    {
      return Optional.empty();
    }
  }

  @Override
  public Optional<IdeProjectConf> getProjectConfChecked(IProject project)
    throws ProjectConfException,
      CoreException
  {
    return getProjectConfChecked(project, true);
  }

  private Optional<IdeProjectConf> getProjectConfChecked(IProject project, boolean resolveImports)
    throws ProjectConfException
  {
    if (!JdtUtils.isJavaProject(project))
      return Optional.empty();

    IJavaProject javaProject = JavaCore.create(project);
    Map<String, String> processorOptions = AptConfig.getProcessorOptions(javaProject, false);
    if (Collections.disjoint(processorOptions.keySet(), ProjectConf.ALL_CONF_NAMES))
      return Optional.empty();

    IdeProjectConf projectConf = new IdeProjectConf(project, processorOptions::get);

    if (resolveImports)
    {
      projectConf
          .resolveBundleMappings(moduleName -> getImportedModule(project, moduleName).orElse(null));
    }

    return Optional.ofNullable(projectConf);
  }

  @Override
  public Stream<IdeModule> getModulesInWorkspace()
  {
    return Stream.of(ResourcesPlugin.getWorkspace().getRoot().getProjects()).map(this::getModule)
        .filter(Optional::isPresent).map(Optional::get);
  }

  @Override
  public Optional<IdeModule> getModule(IProject project)
  {
    return getProjectConf(project)
        .map(projectConf -> new IdeModule(project, projectConf, false, true));
  }

  @Override
  public Stream<IdeModule> getImportedModules(IdeModule module)
  {
    List<IdeModule> result = new ArrayList<>();

    for (String importedModuleName : module.getImportedModules())
    {
      IdeModule importedModule =
          getImportedModule(module.getProject(), importedModuleName).orElse(null);

      if (importedModule != null)
      {
        result.add(new IdeModule(module.getProject(), importedModule, true,
            importedModule.isFromWorkspace()));
      }
    }

    return result.stream();
  }


  @Override
  public Stream<IdeSourceType> getSourceTypes(IProject project, boolean includeLocal,
      boolean includeImports, String filterText, boolean onlyWithProblems)
  {
    return getMessageKeys(project, null, null, null, includeLocal, includeImports, filterText,
        onlyWithProblems)
        .map(mk -> new IdeSourceType(project, mk.getModuleName(), mk.getSourceType(),
            mk.isImported(), mk.isFromWorkspace()))
        .distinct();
  }

  @Override
  public int countSourceTypes(IProject project, boolean includeLocal, boolean includeImports,
      String filterText, boolean onlyWithProblems)
  {
    return (int) getSourceTypes(project, includeLocal, includeImports, filterText, onlyWithProblems)
        .count();
  }

  @Override
  public Stream<IdeSourceType> getSourceTypes(IdeModule module, boolean includeImportedModules,
      String filterText, boolean onlyWithProblems)
  {
    Set<String> moduleNames = new HashSet<>();
    moduleNames.add(module.getModuleName());
    if (includeImportedModules)
      getImportedModules(module).map(IdeModule::getModuleName).forEach(moduleNames::add);

    Set<IdeSourceType> seen = new HashSet<>();
    return getMessageKeys(module.getProject(), moduleNames, null, null, true, true, filterText,
        onlyWithProblems)
        .map(mk -> new IdeSourceType(module.getProject(), mk.getModuleName(), mk.getSourceType(),
            mk.isImported(), mk.isFromWorkspace()))
        // .distinct();
        /*
         * distinct() + Stream.concat(..) throws NPEs. This filter should have the exact same effect
         * as distinct() without the Exception.
         */
        .filter(seen::add);
  }

  @Override
  public int countSourceTypes(IdeModule module, boolean includeImportedModules, String filterText,
      boolean onlyWithProblems)
  {
    return (int) getSourceTypes(module, includeImportedModules, filterText, onlyWithProblems)
        .count();
  }


  @Override
  public Stream<IdeMessageKey> getMessageKeys(IProject project, boolean includeLocal,
      boolean includeImports, String filterText, boolean onlyWithProblems)
  {
    return getMessageKeys(project, null, null, null, includeLocal, includeImports, filterText,
        onlyWithProblems);
  }

  @Override
  public int countMessageKeys(IProject project, boolean includeLocal, boolean includeImports,
      String filterText, boolean onlyWithProblems)
  {
    return (int) getMessageKeys(project, includeLocal, includeImports, filterText, false).count();
  }

  @Override
  public Stream<IdeMessageKey> getMessageKeys(IdeModule module, boolean includeImportedModules,
      String filterText, boolean onlyWithProblems)
  {
    Set<String> moduleNames = new HashSet<>();
    moduleNames.add(module.getModuleName());
    if (includeImportedModules)
      getImportedModules(module).map(IdeModule::getModuleName).forEach(moduleNames::add);

    return getMessageKeys(module.getProject(), moduleNames, null, null, true, true, filterText,
        onlyWithProblems);
  }

  @Override
  public int countMessageKeys(IdeModule module, boolean includeImportedModules, String filterText,
      boolean onlyWithProblems)
  {
    return (int) getMessageKeys(module, includeImportedModules, filterText, onlyWithProblems)
        .count();
  }

  @Override
  public Stream<IdeMessageKey> getMessageKeys(IdeSourceType sourceType, boolean includeLocal,
      boolean includeImports, String filterText, boolean onlyWithProblems)
  {
    return getMessageKeys(sourceType.getProject(),
        Collections.singleton(sourceType.getModuleName()), sourceType.getSourceTypeName(), null,
        includeLocal, includeImports, filterText, onlyWithProblems);
  }

  @Override
  public int countMessageKeys(IdeSourceType sourceType, boolean includeLocal,
      boolean includeImports, String filterText, boolean onlyWithProblems)
  {
    return (int) getMessageKeys(sourceType, includeLocal, includeImports, filterText,
        onlyWithProblems).count();
  }


  @Override
  public Optional<IdeSourceType> findSourceType(IProject project, String sourceTypeName,
      boolean includeLocal, boolean includeImports)
  {
    return getMessageKeys(project, null, sourceTypeName, null, includeLocal, includeImports, null,
        false)
        .map(mk -> new IdeSourceType(project, mk.getModuleName(), mk.getSourceType(),
            mk.isImported(), mk.isFromWorkspace()))
        .findFirst();
  }

  @Override
  public Optional<IdeMessageKey> findMessageKey(IProject project, String code, boolean includeLocal,
      boolean includeImports)
  {
    return findMessageKeys(project, Collections.singleton(code), includeLocal, includeImports)
        .findAny();
  }

  @Override
  public Stream<IdeMessageKey> findMessageKeys(IProject project, Set<String> messageKeyCodes,
      boolean includeLocal, boolean includeImports)
  {
    return getMessageKeys(project, null, null, messageKeyCodes, includeLocal, includeImports, null,
        false);
  }

  @Override
  public Optional<IdeMessageKey> findMessageKey(String messageKeyCode, boolean includeLocal,
      boolean includeImports)
  {
    return getMessageKeys(null, null, null, Collections.singleton(messageKeyCode), includeLocal,
        includeImports, null, false).findFirst();
  }

  private Stream<IdeMessageKey> getMessageKeys(IProject project, Collection<String> moduleNames,
      String sourceTypeName, Collection<String> messageKeyCodes, boolean includeLocal,
      boolean includeImports, String filterText, boolean onlyWithProblems)
  {
    if (!includeLocal && !includeImports)
      return Stream.empty();

    List<Query<IdeMessageKey>> queries = new ArrayList<>();

    if (project != null)
      queries.add(equal(MESSAGEKEY_ATTR_PROJECT, project));

    if (moduleNames != null)
      queries.add(in(MESSAGEKEY_ATTR_MODULE_NAME, moduleNames));

    if (sourceTypeName != null)
      queries.add(equal(MESSAGEKEY_ATTR_SOURCE_TYPE, sourceTypeName));

    if (messageKeyCodes != null)
      queries.add(in(MESSAGEKEY_ATTR_MESSAGE_KEY_CODE, messageKeyCodes));

    if (includeLocal && !includeImports)
      queries.add(equal(MESSAGEKEY_ATTR_IMPORTED, false));
    else if (!includeLocal && includeImports)
      queries.add(equal(MESSAGEKEY_ATTR_IMPORTED, true));


    Query<IdeMessageKey> query;
    if (queries.size() > 1)
      query = new And<>(queries);
    else
      query = queries.get(0);

    Stream<IdeMessageKey> messageKeyStream = messageKeys
        .retrieve(query,
            queryOptions(orderBy(ascending(MESSAGEKEY_ATTR_MESSAGE_KEY_CODE)),
                applyThresholds(threshold(EngineThresholds.INDEX_ORDERING_SELECTIVITY, 1.0))))
        .stream();

    if (StringUtils.isNotBlank(filterText))
    {
      String[] filterTextTokens = filterText.trim().split("\\s+");

      for (String token : filterTextTokens)
      {
        messageKeyStream =
            messageKeyStream.filter(k -> StringUtils.containsIgnoreCase(k.getSourceType(), token)
                || StringUtils.containsIgnoreCase(k.getCode(), token));
      }
    }

    if (onlyWithProblems)
    {
      messageKeyStream = messageKeyStream.filter(k ->
      {
        IMarker marker = findMostSevereBundleValidationMarker(k, null);
        boolean hasProblems = (marker != null
            && marker.getAttribute(IMarker.SEVERITY, -1) >= IMarker.SEVERITY_WARNING);
        return hasProblems;
      });
    }

    return messageKeyStream;
  }

  private Optional<IdeModule> getImportedModule(IProject project, String importedModuleName)
  {
    return Optional.ofNullable(importedModulesPerProject
        .getOrDefault(project, Collections.emptyMap()).get(importedModuleName));
  }

  @Override
  public MessageAvailability getMessageAvailability(IProject project, String bundleName,
      Locale locale, String code)
  {
    Map<String, Map<String, Boolean>> availableBundleFiles =
        availableMessagesPerProject.get(project);
    if (availableBundleFiles == null)
      return new MessageAvailability(false, false, false);

    String bundleFileKey = bundleName + ":" + locale.toString();

    Map<String, Boolean> availableMessageKeys = availableBundleFiles.get(bundleFileKey);
    if (availableMessageKeys == null)
      return new MessageAvailability(false, false, false);

    Boolean messageAvailable = availableMessageKeys.get(code);
    if (messageAvailable == null)
      return new MessageAvailability(true, false, false);

    return new MessageAvailability(true, true, messageAvailable);
  }


  @Override
  public Optional<Set<Locale>> getRequiredLocales(IdeMessageKey messageKey)
  {
    IdeModule rootModule = getModule(messageKey.getProject()).orElse(null);
    if (rootModule == null)
      return Optional.empty();

    Map<String, IdeModule> importedModules = new HashMap<>();
    for (String importedModuleName : rootModule.getImportedModules())
    {
      IdeModule importedModule =
          getImportedModule(messageKey.getProject(), importedModuleName).orElse(null);
      if (importedModule == null)
        return Optional.empty();

      importedModules.put(importedModuleName, importedModule);
    }

    return Optional.of(MessageValidationUtils.getRequiredLocales(messageKey.getCode(), rootModule,
        importedModules, (module, code) ->
        {
          return messageKeys.retrieve(and(equal(MESSAGEKEY_ATTR_PROJECT, messageKey.getProject()),
              equal(MESSAGEKEY_ATTR_MODULE_NAME, module.getModuleName()),
              equal(MESSAGEKEY_ATTR_MESSAGE_KEY_CODE, code))).isNotEmpty();
        }));
  }

  @Override
  public Set<Locale> getTargetLocales(IdeMessageKey messageKey, boolean importsOnly)
  {
    IdeModule rootModule = getModule(messageKey.getProject()).orElse(null);
    if (rootModule == null)
      return Collections.emptySet();

    Map<String, IdeModule> importedModules = new HashMap<>();
    for (String importedModuleName : rootModule.getImportedModules())
    {
      IdeModule importedModule =
          getImportedModule(messageKey.getProject(), importedModuleName).orElse(null);
      if (importedModule != null)
      {
        importedModules.put(importedModuleName, importedModule);
      }
    }

    return MessageValidationUtils.getTargetLocales(messageKey.getCode(), importsOnly, rootModule,
        importedModules, (module, code) ->
        {
          return messageKeys.retrieve(and(equal(MESSAGEKEY_ATTR_PROJECT, messageKey.getProject()),
              equal(MESSAGEKEY_ATTR_MODULE_NAME, module.getModuleName()),
              equal(MESSAGEKEY_ATTR_MESSAGE_KEY_CODE, code))).isNotEmpty();
        });
  }


  @Override
  public IMarker findMostSevereBundleValidationMarker(IProject project)
  {
    return getMostSevereMarker(markers.retrieve(equal(MARKER_ATTR_PROJECT, project)).stream())
        .orElse(null);
  }

  @Override
  public IMarker findMostSevereBundleValidationMarker(IdeSourceType sourceType)
  {
    return getMostSevereMarker(
        markers.retrieve(and(equal(MARKER_ATTR_PROJECT, sourceType.getProject()),
            equal(MARKER_ATTR_SOURCE_TYPE, sourceType.getSourceTypeName()))).stream())
        .orElse(null);
  }


  @Override
  public IMarker findMostSevereBundleValidationMarker(IdeMessageKey messageKey, Locale locale)
  {
    return getMostSevereMarker(findBundleValidationMarkers(messageKey, locale)).orElse(null);
  }

  private Optional<IMarker> getMostSevereMarker(Stream<IMarker> markers)
  {
    return markers.reduce((m1, m2) ->
    {
      return (m1.getAttribute(IMarker.SEVERITY, -1) >= m2.getAttribute(IMarker.SEVERITY, -1)) ? m1
          : m2;
    });
  }

  @Override
  public Stream<IMarker> findBundleValidationMarkers(IdeMessageKey messageKey, Locale locale)
  {
    Stream<IMarker> markerStream =
        markers.retrieve(and(equal(MARKER_ATTR_PROJECT, messageKey.getProject()),
            equal(MARKER_ATTR_SOURCE_TYPE, messageKey.getSourceType()),
            equal(MARKER_ATTR_MESSAGE_KEY, messageKey.getCode()))).stream();
    if (locale != null)
    {
      String localeString = locale.toString();
      markerStream = markerStream.filter(
          m -> localeString.equals(m.getAttribute(BundleFileValidator.MARKER_ATTR_LOCALE, null)));
    }
    return markerStream;
  }


  @Override
  public void cleanProject(IProject project)
  {
    cleanProject(project, false);
  }

  private void cleanProject(IProject project, boolean forceRemoved)
  {
    messageKeyIndexPerProject.remove(project);
    messageKeyIndexFilePerProject.remove(project);

    messageKeys.update(messageKeys.retrieve(equal(MESSAGEKEY_ATTR_PROJECT, project)),
        Collections.emptySet());
    importedLocalProjectsRequiredBy.values().removeAll(Collections.singleton(project));

    availableMessagesPerProject.remove(project);

    markers.update(markers.retrieve(equal(MARKER_ATTR_PROJECT, project)), Collections.emptySet());

    Optional<IdeProjectConf> projectConf = getProjectConf(project);
    if (!projectConf.isPresent() || forceRemoved)
      fireProjectRemovedEvent(project);
    else
      fireProjectRefreshedEvent(project);
  }

  @Override
  public void refreshProject(IProject project)
  {
    boolean refreshed = messageKeyIndexPerProject.containsKey(project);

    refreshLocalMessageKeys(project);
    refreshImportedModules(project);
    refreshAvailableMessages(project);
    refreshBundleValidationMarkers(project);

    if (refreshed)
      fireProjectRefreshedEvent(project);
    else
      fireProjectAddedEvent(project);
  }

  @Override
  public void resourceChanged(IResourceChangeEvent event)
  {
    try
    {
      switch (event.getType()) // NOPMD:SwitchStmtsShouldHaveDefault
      {
        case IResourceChangeEvent.PRE_REFRESH ->
        {
          if (event.getResource() != null)
            refreshProject((IProject) event.getResource());
        }

        case IResourceChangeEvent.PRE_CLOSE, IResourceChangeEvent.PRE_DELETE ->
        {
          cleanProject((IProject) event.getResource(), true);
        }

        case IResourceChangeEvent.PRE_BUILD ->
        {
          Map<IProject, Set<String>> removedTypesPerProject = new HashMap<>();

          /*
           * When a Java file is deleted, remove all associated message keys from the message key
           * index file.
           */
          event.getDelta().accept(delta ->
          {
            IResource resource = delta.getResource();

            IProject project = resource.getProject();
            if (project == null) // e.g. for the workspace root
              return true;

            IdeProjectConf projectConf = getProjectConf(project).orElse(null);
            if (projectConf == null)
              return true;

            if (resource instanceof IFile)
            {
              IFile file = (IFile) resource;

              if (delta.getKind() == IResourceDelta.REMOVED && project.isOpen()
                  && project.hasNature(JavaCore.NATURE_ID))
              {
                IJavaElement element = JavaCore.create(file);
                if (element instanceof ICompilationUnit)
                {
                  ICompilationUnit compilationUnit = (ICompilationUnit) element;

                  /*
                   * The file is already deleted, so there's no way to get the actually deleted Java
                   * types it contains -> guess the top-level type name based on the file name.
                   */
                  IPackageFragment packageFragment = (IPackageFragment) compilationUnit.getParent();
                  String simpleTypeName = StringUtils.removeEnd(element.getElementName(), ".java");

                  String removedTopLevelType;
                  if (packageFragment.isDefaultPackage())
                    removedTopLevelType = simpleTypeName;
                  else
                    removedTopLevelType = packageFragment.getElementName() + "." + simpleTypeName;

                  removedTypesPerProject.computeIfAbsent(project, p -> new HashSet<>())
                      .add(removedTopLevelType);
                }
              }

              if (delta.getKind() == IResourceDelta.REMOVED
                  && projectConf.isMessageBundleFile(file))
              {
                Map<String, Map<String, Boolean>> availableBundleFiles =
                    availableMessagesPerProject.get(project);
                IFolder bundleDir = projectConf.getBundleDirAsFolder().orElse(null);
                if (availableBundleFiles != null && bundleDir != null)
                {
                  BundleFileCoordinates coordinates = MessageBundleManager.toBundleFileCoordinates(
                      resource.getFullPath().makeRelativeTo(bundleDir.getFullPath()).toString());
                  String bundleFileKey =
                      coordinates.getBundleName() + ":" + coordinates.getLocale().toString();
                  availableBundleFiles.remove(bundleFileKey);
                }

                fireProjectUpdatedEvent(project);
              }
            }

            return true;
          });

          if (!removedTypesPerProject.isEmpty())
            removeMessageKeysForTopLevelTypes(removedTypesPerProject);
        }

        case IResourceChangeEvent.POST_BUILD ->
        {
          event.getDelta().accept(delta ->
          {
            IResource resource = delta.getResource();

            IProject project = resource.getProject();
            if (project == null) // e.g. for the workspace root
              return true;

            IFile indexFile = findMessageKeyIndex(project);
            if (indexFile != null && indexFile.equals(resource))
            {
              boolean refreshed = messageKeyIndexPerProject.containsKey(project);

              refreshLocalMessageKeys(project);

              if (refreshed)
              {
                // FIXME seems redundant; refreshLocalMessageKeys will already fire MKCHANGED event
                // fireProjectRefreshedEvent(project);
              }
              else
                fireProjectAddedEvent(project);
            }

            return true;
          });
        }

        case IResourceChangeEvent.POST_CHANGE ->
        {
          Set<IProject> problemsUpdated = new HashSet<>();
          Set<IProject> messagesUpdated = new HashSet<>();

          event.getDelta().accept(delta ->
          {
            if ((delta.getFlags() & IResourceDelta.MARKERS) != 0)
            {
              for (IMarkerDelta markerDelta : delta.getMarkerDeltas())
              {
                if (markerDelta.isSubtypeOf(BundleFileValidator.MARKER_ID_BUNDLE_PROBLEM))
                {
                  IProject project = markerDelta.getResource().getProject();
                  IMarker marker = markerDelta.getMarker();

                  switch (markerDelta.getKind()) // NOPMD:SwitchStmtsShouldHaveDefault
                  {
                    case IResourceDelta.ADDED, IResourceDelta.CHANGED ->
                    {
                      if (markers.add(marker))
                        problemsUpdated.add(project);
                    }

                    case IResourceDelta.REMOVED ->
                    {
                      if (markers.remove(marker))
                        problemsUpdated.add(project);
                    }
                  }
                }
              }
            }

            return true;
          });

          event.getDelta().accept(delta ->
          {
            IResource resource = delta.getResource();
            IProject project = resource.getProject();

            if (resource.getType() != IResource.FILE
                || !"properties".equals(resource.getFileExtension())
                || (delta.getFlags() & IResourceDelta.CONTENT) == 0)
            {
              return true;
            }

            IdeProjectConf projectConf = getProjectConf(project).orElse(null);
            if (projectConf == null)
              return true;

            IFolder bundleDir = projectConf.getBundleDirAsFolder().orElse(null);
            if (bundleDir == null || !bundleDir.exists())
              return true;
            if (!bundleDir.getFullPath().isPrefixOf(resource.getFullPath()))
              return true;

            IFile bundleFileResource = (IFile) resource;

            EclipseMessageBundleManager bundleManager =
                projectConf.createEclipseMessageBundleManager();

            BundleFile<IFile, CoreException> bundleFile =
                bundleManager.getBundleFileAt(bundleFileResource);
            if (bundleFile == null)
              return true;

            processAvailableMessages(resource.getProject(), bundleFile);
            messagesUpdated.add(project);

            return true;
          });

          problemsUpdated.forEach(project -> fireProblemsUpdatedEvent(project));
          messagesUpdated.forEach(project -> fireProjectUpdatedEvent(project));
        }
      }
    }
    catch (CoreException ex)
    {
      StatusUtils.logError(getClass(), "Failed to process resource delta.", ex);
    }
  }

  @Override
  public void elementChanged(ElementChangedEvent event)
  {
    Deque<IJavaElementDelta> stack = new ArrayDeque<>();

    stack.push(event.getDelta());

    while (!stack.isEmpty())
    {
      IJavaElementDelta delta = stack.pop();

      IJavaElement element = delta.getElement();

      if (element.getElementType() == IJavaElement.JAVA_MODEL)
      {
        for (IJavaElementDelta cd : delta.getAffectedChildren())
          stack.push(cd);
      }
      else if (element.getElementType() == IJavaElement.JAVA_PROJECT)
      {
        IProject project = ((IJavaProject) element).getProject();

        boolean classpathChanged =
            (delta.getFlags() & IJavaElementDelta.F_RESOLVED_CLASSPATH_CHANGED) != 0;
        if (classpathChanged)
        {
          refreshProject(project);
        }
      }
    }
  }

  private void removeMessageKeysForTopLevelTypes(Map<IProject, Set<String>> removedTypesPerProject)
  {
    removedTypesPerProject.forEach((project, types) ->
    {
      try
      {
        MessageKeyIndex index = messageKeyIndexPerProject.get(project);
        if (index == null)
          return;

        /*
         * Don't edit the in-memory MessageKeyIndex; otherwise the changes won't be detected by the
         * usual mechanisms.
         */
        index = MessageKeyIndex.copyOf(index);

        Set<String> typesAndInnerTypes = new HashSet<>(types);
        for (String removedType : types)
        {
          index.getSourceTypes().forEach(st ->
          {
            if (st.startsWith(removedType + "."))
              typesAndInnerTypes.add(st);
          });
        }

        for (String sourceType : typesAndInnerTypes)
        {
          index.updateMessageKeysFor(sourceType, null);
        }

        saveMessageKeys(index, messageKeyIndexFilePerProject.get(project));
      }
      catch (CoreException ex)
      {
        StatusUtils.logError(getClass(), "Failed to update message keys.", ex);
      }
    });
  }

  private void refreshLocalMessageKeys(IProject project)
  {
    MessageKeyIndex oldIndex = messageKeyIndexPerProject.remove(project);
    messageKeyIndexFilePerProject.remove(project);


    Collection<IProject> requiredBys = importedLocalProjectsRequiredBy.get(project);

    Map<IProject, List<IdeMessageKey>> oldKeysPerProject = new HashMap<>();


    try (ResultSet<IdeMessageKey> oldKeys = messageKeys.retrieve(
        and(equal(MESSAGEKEY_ATTR_PROJECT, project), equal(MESSAGEKEY_ATTR_IMPORTED, false))))
    {
      oldKeysPerProject.put(project, oldKeys.stream().collect(toList()));

      messageKeys.update(oldKeys, Collections.emptySet());
    }

    if (oldIndex != null)
    {
      for (IProject requiredByProject : requiredBys)
      {
        try (ResultSet<IdeMessageKey> oldKeys =
            messageKeys.retrieve(and(equal(MESSAGEKEY_ATTR_PROJECT, requiredByProject),
                equal(MESSAGEKEY_ATTR_MODULE_NAME, oldIndex.getModuleName()),
                equal(MESSAGEKEY_ATTR_IMPORTED, true))))
        {
          messageKeys.update(oldKeys, Collections.emptySet());

          oldKeysPerProject.put(requiredByProject, oldKeys.stream().collect(toList()));
        }
      }
    }

    try
    {
      IFile indexFile = findMessageKeyIndex(project);
      if (indexFile != null)
      {
        MessageKeyIndex newIndex = loadMessageKeys(indexFile);
        if (newIndex != null)
        {
          messageKeyIndexPerProject.put(project, newIndex);
          messageKeyIndexFilePerProject.put(project, indexFile);

          {
            List<IdeMessageKey> newKeys = newIndex.getKeys()
                .map(key -> new IdeMessageKey(project, newIndex.getModuleName(), key, false, true))
                .collect(toList());

            messageKeys.addAll(newKeys);

            List<IdeMessageKey> oldKeys = oldKeysPerProject.get(project);
            if (oldKeys != null)
            {
              fireDeltaMessageKeysUpdatedEvent(project, newKeys.stream(), oldKeys.stream());
            }
          }

          for (IProject requiredByProject : requiredBys)
          {
            List<IdeMessageKey> newKeys =
                newIndex.getKeys().map(key -> new IdeMessageKey(requiredByProject,
                    newIndex.getModuleName(), key, true, true)).collect(toList());

            messageKeys.addAll(newKeys);

            List<IdeMessageKey> oldKeys = oldKeysPerProject.get(requiredByProject);
            if (oldKeys != null)
            {
              fireDeltaMessageKeysUpdatedEvent(project, newKeys.stream(), oldKeys.stream());
            }
          }
        }
      }
    }
    catch (CoreException ex)
    {
      String msg = "Failed to load local message keys of project ''{0}''.";
      StatusUtils.logError(getClass(), msg, project.getName(), ex);
    }
  }

  private void refreshImportedModules(IProject project)
  {
    importedModulesPerProject.remove(project);
    try (ResultSet<IdeMessageKey> result = messageKeys.retrieve(
        and(equal(MESSAGEKEY_ATTR_PROJECT, project), equal(MESSAGEKEY_ATTR_IMPORTED, true))))
    {
      messageKeys.update(result, Collections.emptySet());
    }

    importedLocalProjectsRequiredBy.values().removeAll(Collections.singleton(project));

    Optional<IJavaProject> javaProject = JdtUtils.getJavaProject(project);
    if (!javaProject.isPresent())
      return;

    ProjectConf projectConf = getProjectConf(project).orElse(null);
    if (projectConf == null)
      return;


    Deque<String> importedModulesToLoad = new ArrayDeque<>(projectConf.getImports());
    Set<String> loadedModules = new HashSet<>();
    while (!importedModulesToLoad.isEmpty())
    {
      String moduleName = importedModulesToLoad.pop();

      if (!loadedModules.add(moduleName))
        continue;

      String keysFilePath = MessageKeyIndex.getPreferredFilePath(moduleName);
      JavaProjectResourceLoader.loadResource(javaProject.get(), keysFilePath, (source, in) ->
      {
        if (source instanceof IProject)
        {
          IProject sourceProject = (IProject) source;
          importedLocalProjectsRequiredBy.put(sourceProject, project);

          ProjectConf importedProjectConf = getProjectConf(sourceProject).orElse(null);
          if (importedProjectConf == null)
            return;

          importedModulesPerProject.computeIfAbsent(project, k -> new HashMap<>()).put(moduleName,
              new IdeModule(project, importedProjectConf, true, true));

          getMessageKeys(sourceProject, Collections.singleton(moduleName), null, null, true, false,
              null, false).map(mk -> new IdeMessageKey(project, mk.getModuleName(), mk, true, true))
              .forEach(messageKeys::add);
        }
        else if (source instanceof File)
        {
          try
          {
            MessageKeyIndex importedKeysIndex = MessageKeyIndex.readFrom(in);

            importedKeysIndex.getImportedModules().stream().forEach(importedModulesToLoad::push);

            importedModulesPerProject.computeIfAbsent(project, k -> new HashMap<>()).put(moduleName,
                new IdeModule(project, importedKeysIndex, true, false));

            importedKeysIndex.getKeys().map(key -> new IdeMessageKey(project,
                importedKeysIndex.getModuleName(), key, true, false)).forEach(messageKeys::add);
          }
          catch (PersistableDataException | RuntimeException ex)
          {
            String msg = "Failed to load imported message keys ''{0}'' [{1}].";
            msg = String.format(msg, moduleName, keysFilePath);
            StatusUtils.logError(getClass(), msg, moduleName, keysFilePath, ex);
          }
        }
      });
    }
  }

  private void refreshAvailableMessages(IProject project)
  {
    availableMessagesPerProject.remove(project);

    IdeProjectConf projectConf = getProjectConf(project).orElse(null);
    if (projectConf == null || !projectConf.getBundleDirAsFolder().isPresent())
      return;

    EclipseMessageBundleManager bundleManager = projectConf.createEclipseMessageBundleManager();

    for (BundleFile<IFile, CoreException> bundleFile : bundleManager.getBundleFiles())
    {
      processAvailableMessages(project, bundleFile);
    }
  }

  private void processAvailableMessages(IProject project,
      BundleFile<IFile, CoreException> bundleFile)
  {
    Map<String, Map<String, Boolean>> availableBundleFiles =
        availableMessagesPerProject.computeIfAbsent(project, p -> new HashMap<>());

    String bundleFileKey = bundleFile.getBundleName() + ":" + bundleFile.getLocale().toString();

    Map<String, Boolean> availableMessageKeys = new HashMap<>();

    for (String key : bundleFile.getKeys())
    {
      availableMessageKeys.put(key, bundleFile.hasMessage(key));
    }

    availableBundleFiles.put(bundleFileKey, availableMessageKeys);
  }

  private void refreshBundleValidationMarkers(IProject project)
  {
    markers.update(markers.retrieve(equal(MARKER_ATTR_PROJECT, project)), Collections.emptySet());

    IdeProjectConf projectConf = getProjectConf(project).orElse(null);
    if (projectConf == null)
      return;

    IFolder bundleDir = projectConf.getBundleDirAsFolder().orElse(null);
    if (bundleDir == null || !bundleDir.isAccessible())
      return;

    IMarker[] markers;
    try
    {
      markers = bundleDir.findMarkers(BundleFileValidator.MARKER_ID_BUNDLE_PROBLEM, true,
          IResource.DEPTH_INFINITE);
    }
    catch (CoreException ex)
    {
      StatusUtils.logError(getClass(),
          "Failed to retrieve bundle validation markers for bundle directory of project ''{0}''.",
          project.getName(), ex);
      return;
    }

    this.markers.addAll(Arrays.asList(markers));
  }


  private IFile findMessageKeyIndex(IProject project)
  {
    IdeProjectConf projectConf = getProjectConf(project).orElse(null);
    if (projectConf == null)
      return null;

    IJavaProject javaProject = JavaCore.create(project);
    if (javaProject == null)
      return null;

    try
    {
      for (IClasspathEntry entry : javaProject.getResolvedClasspath(true))
      {
        IPath outputLocation = entry.getOutputLocation();
        if (outputLocation == null)
          continue;

        IFile indexFile = project.getWorkspace().getRoot().getFolder(outputLocation)
            .getFile(MessageKeyIndex.getPreferredFilePath(projectConf.getModuleName()));
        if (indexFile.isAccessible())
          return indexFile;
      }
    }
    catch (@SuppressWarnings("unused") JavaModelException ex)
    {
      // continue;
    }

    try
    {
      IFile indexFile = project.getWorkspace().getRoot().getFolder(javaProject.getOutputLocation())
          .getFile(MessageKeyIndex.getPreferredFilePath(projectConf.getModuleName()));
      if (indexFile.isAccessible())
        return indexFile;
    }
    catch (@SuppressWarnings("unused") JavaModelException ex)
    {
      // continue;
    }

    return null;
  }

  private static MessageKeyIndex loadMessageKeys(IFile file)
    throws CoreException
  {
    try (InputStream in = file.getContents(true))
    {
      return MessageKeyIndex.readFrom(in);
    }
    catch (CoreException ex)
    {
      /* resource.exists() doesn't guarantee that the file actually exists... */
      if (ex.getMessage().contains("File not found"))
        return null;

      throw ex;
    }
    catch (IOException | PersistableDataException ex)
    {
      throw new CoreException(StatusUtils.createError(MessageKeyServiceImpl.class,
          "Failed to load message keys from ''{0}''.", file.getProjectRelativePath(), ex));
    }
  }

  private static void saveMessageKeys(MessageKeyIndex index, IFile file)
    throws CoreException
  {
    try
    {
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      index.writeTo(buffer);
      file.setContents(new ByteArrayInputStream(buffer.toByteArray()), true, false, null);
    }
    catch (PersistableDataException ex)
    {
      throw new CoreException(StatusUtils.createError(MessageKeyServiceImpl.class,
          "Failed to load message keys from ''{0}''.", file.getProjectRelativePath(), ex));
    }
  }

}
