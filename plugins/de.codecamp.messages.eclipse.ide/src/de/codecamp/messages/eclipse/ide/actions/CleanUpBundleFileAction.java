package de.codecamp.messages.eclipse.ide.actions;


import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.shared.bundle.BundleFile;
import de.codecamp.messages.shared.messageformat.MessageFormatSupport;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Stream;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;


/**
 * <ul>
 * <li>Adds message stubs where required.</li>
 * <li>Removes empty messages where not required.</li>
 * <li>Adds message arg descriptions as comments.</li>
 * </ul>
 */
public class CleanUpBundleFileAction
{

  private final MessageKeyService messageKeyService;


  public CleanUpBundleFileAction(MessageKeyService messageKeyService)
  {
    this.messageKeyService = messageKeyService;
  }


  public void execute(IdeProjectConf projectConf, EclipseMessageBundleManager bundleManager,
      Stream<IdeMessageKey> addedKeys)
  {
    boolean localBundleManager = false;
    if (bundleManager == null)
    {
      localBundleManager = true;
      bundleManager = projectConf.createEclipseMessageBundleManager();
    }

    if (addedKeys == null)
    {
      addedKeys =
          messageKeyService.getMessageKeys(projectConf.getProject(), true, true, null, false);
    }

    MessageFormatSupport messageFormatSupport = MessageFormatSupport.getSupport(projectConf);

    for (IdeMessageKey messageKey : (Iterable<IdeMessageKey>) addedKeys::iterator)
    {
      String targetBundleName = projectConf.toTargetBundleName(messageKey.getCode()).orElse(null);
      if (targetBundleName == null)
        continue;


      BundleFile<IFile, CoreException> rootBundleFile =
          bundleManager.getBundleFile(targetBundleName, Locale.ROOT, false);

      boolean localDefaultMessageAvailable =
          rootBundleFile != null && rootBundleFile.hasMessageKey(messageKey.getCode());
      if (rootBundleFile != null && localDefaultMessageAvailable)
        rootBundleFile.setComment(messageKey.getCode(),
            messageFormatSupport.createMessageBundleComment(messageKey));


      Set<Locale> requiredLocales = messageKeyService.getRequiredLocales(messageKey).orElse(null);
      if (requiredLocales == null)
        return;

      for (Locale locale : projectConf.getTargetLocales())
      {
        BundleFile<IFile, CoreException> bundleFile =
            bundleManager.getBundleFile(targetBundleName, locale, false);

        if (bundleFile != null && bundleFile.hasMessage(messageKey.getCode()))
        {
          bundleFile.setComment(messageKey.getCode(),
              messageFormatSupport.createMessageBundleComment(messageKey));
        }

        boolean requiredInBundleFile = requiredLocales.contains(locale)
            && !messageKey.isDefaultMessageAvailable() && !localDefaultMessageAvailable;

        if (bundleFile == null)
          bundleFile = bundleManager.getBundleFile(targetBundleName, locale, true);

        if (requiredInBundleFile)
        {
          if (!bundleFile.hasMessageKey(messageKey.getCode()))
            bundleFile.setMessage(messageKey.getCode(), "");

          bundleFile.setComment(messageKey.getCode(),
              messageFormatSupport.createMessageBundleComment(messageKey));
        }
        else
        {
          if (bundleFile.hasMessageKey(messageKey.getCode())
              && bundleFile.isMessageEmpty(messageKey.getCode()))
            bundleFile.removeMessage(messageKey.getCode());
        }
      }
    }

    if (localBundleManager)
    {
      bundleManager.save();
    }
  }

}
