package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.eclipse.ide.actions.ShowMessageBundleDirectoryAction;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import javax.inject.Inject;
import javax.inject.Named;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;


public class ShowMessageBundleDirectoryHandler
{

  @Inject
  private MessageKeyService messageKeyService;


  @CanExecute
  public boolean canExecute(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    if (selection.size() != 1)
      return false;

    Object element = selection.getFirstElement();
    return HandlerUtils.hasProject(element);
  }

  @Execute
  public void execute(@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
      IProgressMonitor monitor)
  {
    Object element = selection.getFirstElement();

    IProject project = HandlerUtils.getProject(element).orElse(null);
    if (project == null)
      return;

    try
    {
      ShowMessageBundleDirectoryAction action =
          new ShowMessageBundleDirectoryAction(messageKeyService);
      action.execute(project, monitor);
    }
    catch (CoreException | RuntimeException ex)
    {
      StatusUtils.showError(getClass(), "", "Failed to open the message bundle directory.", ex);
    }
  }

}
