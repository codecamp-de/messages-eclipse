package de.codecamp.messages.eclipse.ide.validation;


import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.shared.bundle.BundleFile;
import java.util.Map;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;


public class UndeclaredKeysValidationTask
  implements
    BundleFileValidationTask
{

  @Override
  public void validate(ValidationResultAdapter result, BundleFileValidationContext context,
      BundleFile<IFile, CoreException> bundleFile, BundleFile<IFile, CoreException> rootBundleFile)
  {
    Map<String, IdeMessageKey> declaredMessageKeys = context.getDeclaredMessageKeys();
    for (String key : bundleFile.getKeys())
    {
      IdeMessageKey mkWithSource = declaredMessageKeys.get(key);
      if (mkWithSource == null)
      {
        String msg = "Undeclared message key.";
        result.addBundleWarning(bundleFile, key, null, msg);
      }
    }
  }

}
