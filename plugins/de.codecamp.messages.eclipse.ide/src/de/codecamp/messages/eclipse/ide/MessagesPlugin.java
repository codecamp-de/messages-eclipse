package de.codecamp.messages.eclipse.ide;


import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ResourceLocator;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;


public class MessagesPlugin
  extends
    AbstractUIPlugin
{

  private static MessagesPlugin plugin;


  public MessagesPlugin()
  {
  }


  @Override
  public void start(BundleContext context)
    throws Exception
  {
    super.start(context);

    plugin = this;

    /*
     * Trigger activation of MessageKeyService. Letting OSGi do it immediately is too early. Here
     * seems to be fine, though it doesn't seem very elegant.
     */
    ServiceReference<MessageKeyService> serviceReference =
        context.getServiceReference(MessageKeyService.class);
    context.getService(serviceReference);
    context.ungetService(serviceReference);
  }

  @Override
  public void stop(BundleContext context)
    throws Exception
  {
    plugin = null;

    super.stop(context);
  }


  public static MessagesPlugin getDefault()
  {
    return plugin;
  }

  public static String getPluginId()
  {
    return getDefault().getBundle().getSymbolicName();
  }


  /**
   * Creates an image descriptor for the image file at the given plug-in relative path.
   *
   * @param path
   *          the path
   * @return the image descriptor
   */
  public static ImageDescriptor createImageDescriptor(String path)
  {
    return ResourceLocator.imageDescriptorFromBundle(getPluginId(), path)
        .orElse(ImageDescriptor.getMissingImageDescriptor());
  }

  /**
   * Returns the (cached) {@link Image} loaded from the given path.
   *
   * @param path
   *          the path to the image, relative to the root directory of the plug-in
   *
   * @return the image
   */
  public Image getImage(String path)
  {
    Image img = getImageRegistry().get(path);
    if (img == null)
    {
      getImageRegistry().put(path, createImageDescriptor(path));
      img = getImageRegistry().get(path);
    }
    return img;
  }

  /**
   * Returns the (cached) {@link ImageDescriptor} loaded from the given path.
   *
   * @param path
   *          the path to the image, relative to the root directory of the plug-in
   *
   * @return the image descriptor
   */
  public ImageDescriptor getImageDescriptor(String path)
  {
    ImageDescriptor imgDesc = getImageRegistry().getDescriptor(path);
    if (imgDesc == null)
    {
      imgDesc = createImageDescriptor(path);
      getImageRegistry().put(path, imgDesc);
    }
    return imgDesc;
  }

  public Image getOverlayImage(String basePath, String overlayPath, int quadrant)
  {
    String key = "overlay:" + basePath + ":" + overlayPath + ":" + quadrant;

    Image img = getImageRegistry().get(key);
    if (img == null)
    {
      getOverlayImageDescriptor(basePath, overlayPath, quadrant);
      img = getImageRegistry().get(key);
    }
    return img;
  }

  public ImageDescriptor getOverlayImageDescriptor(String basePath, String overlayPath,
      int quadrant)
  {
    String key = "overlay:" + basePath + ":" + overlayPath + ":" + quadrant;

    ImageDescriptor imgDesc = getImageRegistry().getDescriptor(key);
    if (imgDesc == null)
    {
      ImageDescriptor base = getImageDescriptor(basePath);
      ImageDescriptor overlay = getImageDescriptor(overlayPath);
      imgDesc = new DecorationOverlayIcon(base, overlay, quadrant);
      getImageRegistry().put(key, imgDesc);
    }
    return imgDesc;
  }

}
