package de.codecamp.messages.eclipse.ide.actions;


import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import java.lang.annotation.ElementType;
import java.util.Optional;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.JavaUI;


public class OpenMessageKeyDeclarationAction
{

  private final MessageKeyService messageKeyService;


  public OpenMessageKeyDeclarationAction(MessageKeyService messageKeyService)
  {
    this.messageKeyService = messageKeyService;
  }


  public void execute(IdeMessageKey messageKey, IProgressMonitor monitor)
    throws CoreException
  {
    IJavaProject javaProject = JavaCore.create(messageKey.getProject());
    IType type = javaProject.findType(messageKey.getSourceType());
    if (type == null)
      return;

    ElementType sourceElementType = messageKey.getSourceElementType();
    String sourceElementName = messageKey.getSourceElementName();

    IJavaElement javaElement;
    switch (sourceElementType)
    {
      case TYPE ->
      {
        javaElement = type;
      }

      case METHOD ->
      {
        javaElement = type;
        for (IMethod m : type.getMethods())
        {
          if (m.getElementName().equals(sourceElementName))
          {
            javaElement = m;
            break;
          }
        }
      }

      case FIELD ->
      {
        javaElement = type.getField(sourceElementName);
        if (!javaElement.exists())
          javaElement = null;
      }

      default ->
      {
        javaElement = type;
      }
    }

    JavaUI.openInEditor(javaElement);
  }

  public void execute(String messageKey, IProgressMonitor monitor)
    throws CoreException
  {
    Optional<IdeMessageKey> key = messageKeyService.findMessageKey(messageKey, true, false);
    if (!key.isPresent())
      return;

    execute(key.get(), monitor);
  }

}
