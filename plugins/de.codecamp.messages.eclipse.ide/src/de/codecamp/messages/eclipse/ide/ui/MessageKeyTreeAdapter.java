package de.codecamp.messages.eclipse.ide.ui;


import org.eclipse.core.resources.IWorkspace;
import org.eclipse.jface.viewers.TreeViewer;


public class MessageKeyTreeAdapter
{

  private final TreeViewer treeViewer;

  private final MessageKeyTreeContentProvider treeViewerContentProvider;


  public MessageKeyTreeAdapter(TreeViewer treeViewer,
      MessageKeyTreeContentProvider treeViewerContentProvider)
  {
    this.treeViewer = treeViewer;
    this.treeViewerContentProvider = treeViewerContentProvider;
  }


  public boolean getGroupBySourceType()
  {
    return treeViewerContentProvider.getGroupBySourceType();
  }

  public void setGroupBySourceType(boolean groupBySourceType)
  {
    if (getGroupBySourceType() != groupBySourceType)
    {
      treeViewerContentProvider.setGroupBySourceType(groupBySourceType);
      treeViewer.refresh();
    }
  }

  public boolean getGroupByModules()
  {
    return treeViewerContentProvider.getGroupByModules();
  }

  public void setGroupByModules(boolean groupImportedKeys)
  {
    if (getGroupByModules() != groupImportedKeys)
    {
      treeViewerContentProvider.setGroupByModules(groupImportedKeys);
      treeViewer.refresh();
    }
  }

  public boolean getShowOnlyKeysWithProblems()
  {
    return treeViewerContentProvider.getShowOnlyKeysWithProblems();
  }

  public void setShowOnlyKeysWithProblems(boolean onlyShowKeysWithProblems)
  {
    if (getShowOnlyKeysWithProblems() != onlyShowKeysWithProblems)
    {
      treeViewerContentProvider.setShowOnlyKeysWithProblems(onlyShowKeysWithProblems);
      treeViewer.refresh();
    }
  }

  public boolean getShowImportedKeys()
  {
    return treeViewerContentProvider.getShowImportedKeys();
  }


  public void setShowImportedKeys(boolean showImportedKeys)
  {
    if (getShowImportedKeys() != showImportedKeys)
    {
      treeViewerContentProvider.setShowImportedKeys(showImportedKeys);
      treeViewer.refresh();
    }
  }


  public void expandAll()
  {
    treeViewer.expandAll(true);
  }

  public void collapseAll()
  {
    treeViewer.collapseAll();
    if (treeViewer.getInput() instanceof IWorkspace)
      treeViewer.setAutoExpandLevel(2);
  }

}
