package de.codecamp.messages.eclipse.ide.ui;


public class Constants
{

  public static final String VIEWSTATE_GROUP_BY_SOURCE_TYPE = "groupBySourceType";

  public static final String VIEWSTATE_SHOW_IMPORTED_KEYS = "showImportedKeys";

  public static final String VIEWSTATE_GROUP_BY_MODULES = "groupByModules";

  public static final String VIEWSTATE_SHOW_ONLY_KEYS_WITH_PROBLEMS = "showOnlyKeysWithProblems";


  public static final String EDITORSTATE_GROUP_BY_SOURCE_TYPE = "editor_groupBySourceType";

  public static final String EDITORSTATE_SHOW_IMPORTED_KEYS = "editor_showImportedKeys";

  public static final String EDITORSTATE_GROUP_BY_MODULES = "editor_groupByModules";

  public static final String EDITORSTATE_SHOW_ONLY_KEYS_WITH_PROBLEMS =
      "editor_showOnlyKeysWithProblems";


  public static final boolean STATE_GROUP_BY_SOURCE_TYPE_DEFAULT = false;

  public static final boolean STATE_SHOW_IMPORTED_KEYS_DEFAULT = true;

  public static final boolean STATE_GROUP_BY_MODULES_DEFAULT = true;

  public static final boolean STATE_SHOW_ONLY_KEYS_WITH_PROBLEMS_DEFAULT = false;


  private Constants()
  {
    // utility class
  }

}
