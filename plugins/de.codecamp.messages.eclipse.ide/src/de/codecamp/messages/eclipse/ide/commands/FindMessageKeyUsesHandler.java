package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.eclipse.ide.actions.StartMessageKeyReferenceSearchAction;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import javax.inject.Inject;
import javax.inject.Named;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;


/**
 * Opens a Java editor for the file and location where the message key was declared.
 */
public class FindMessageKeyUsesHandler
{

  @Inject
  private MessageKeyService messageKeyService;


  @CanExecute
  @Evaluate
  public boolean canExecute(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    return selection.getFirstElement() instanceof IdeMessageKey;
  }

  @Execute
  public void execute(@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
    throws CoreException
  {
    IdeMessageKey messageKey = (IdeMessageKey) selection.getFirstElement();

    StartMessageKeyReferenceSearchAction action =
        new StartMessageKeyReferenceSearchAction(messageKeyService);
    action.execute(messageKey, null);
  }

}
