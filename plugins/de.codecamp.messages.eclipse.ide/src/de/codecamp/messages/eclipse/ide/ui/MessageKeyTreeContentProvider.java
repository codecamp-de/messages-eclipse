package de.codecamp.messages.eclipse.ide.ui;


import static java.util.stream.Collectors.toList;

import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.IdeModule;
import de.codecamp.messages.eclipse.ide.services.IdeSourceType;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import java.util.List;
import java.util.stream.Stream;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.jface.viewers.ILazyTreeContentProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;


public class MessageKeyTreeContentProvider
  implements
    ILazyTreeContentProvider
{

  private final MessageKeyService messageKeyService;


  private boolean showImportedKeys = true;

  private boolean groupByModules = true;

  private boolean groupBySourceType = false;

  private boolean showOnlyKeysWithProblems = false;

  private String filterText;


  private TreeViewer treeViewer;


  private long preloadTimestamp;

  private Object preloadParent;

  private int preloadOffset;

  private List<Object> preloadElements;


  public MessageKeyTreeContentProvider(MessageKeyService messageKeyService)
  {
    this.messageKeyService = messageKeyService;
  }


  public boolean getShowImportedKeys()
  {
    return showImportedKeys;
  }

  public void setShowImportedKeys(boolean showImportedKeys)
  {
    this.showImportedKeys = showImportedKeys;
    preloadElements = null;
  }

  public boolean getGroupByModules()
  {
    return groupByModules;
  }

  public void setGroupByModules(boolean groupByModules)
  {
    this.groupByModules = groupByModules;
    preloadElements = null;
  }

  public boolean getGroupBySourceType()
  {
    return groupBySourceType;
  }

  public void setGroupBySourceType(boolean groupBySourceType)
  {
    this.groupBySourceType = groupBySourceType;
    preloadElements = null;
  }

  public boolean getShowOnlyKeysWithProblems()
  {
    return showOnlyKeysWithProblems;
  }

  public void setShowOnlyKeysWithProblems(boolean showOnlyKeysWithProblems)
  {
    this.showOnlyKeysWithProblems = showOnlyKeysWithProblems;
    preloadElements = null;
  }

  public String getFilterText()
  {
    return filterText;
  }

  public void setFilterText(String filterText)
  {
    this.filterText = filterText;
    preloadElements = null;
  }


  @Override
  public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
  {
    this.treeViewer = (TreeViewer) viewer;
    preloadElements = null;
  }

  @Override
  public void updateChildCount(Object element, int currentChildCount)
  {
    int count = -1;
    if (element instanceof IWorkspace)
    {
      count = (int) messageKeyService.getModulesInWorkspace().count();
    }
    else if (element instanceof IdeModule)
    {
      IdeModule module = (IdeModule) element;
      if (groupByModules && showImportedKeys)
      {
        long modulesCount = messageKeyService.getImportedModules(module).count();
        if (groupBySourceType)
        {
          count = (int) (modulesCount + messageKeyService.countSourceTypes(module, false,
              filterText, showOnlyKeysWithProblems));
        }
        else
        {
          count = (int) (modulesCount + messageKeyService.countMessageKeys(module, false,
              filterText, showOnlyKeysWithProblems));
        }
      }
      else if (groupBySourceType)
      {
        count = messageKeyService.countSourceTypes(module.getProject(), true, showImportedKeys,
            filterText, showOnlyKeysWithProblems);
      }
      else
      {
        count = messageKeyService.countMessageKeys(module.getProject(), true, showImportedKeys,
            filterText, showOnlyKeysWithProblems);
      }
    }
    else if (element instanceof IdeSourceType)
    {
      IdeSourceType sourceType = (IdeSourceType) element;
      count = messageKeyService.countMessageKeys(sourceType, true, showImportedKeys, filterText,
          showOnlyKeysWithProblems);
    }
    else if (element instanceof IdeMessageKey)
    {
      count = 0;
    }

    if (count > -1)
      treeViewer.setChildCount(element, count);
  }


  @Override
  public void updateElement(Object parent, int index)
  {
    Object element = null;

    /*
     * Requesting individual indexes can be highly inefficient. So implement a simple preload that
     * always retrieves a whole block around the requested index, if the requested index cannot
     * already be found in the preloaded block.
     *
     * To avoid problems with stale data, only use the preloaded blocks for a very limited time.
     */
    if (preloadElements != null && parent == preloadParent && index >= preloadOffset // NOPMD:CompareObjectsWithEquals
        && index - preloadOffset < preloadElements.size()
        && Math.abs(System.currentTimeMillis() - preloadTimestamp) < 5000)
    {
      element = preloadElements.get(index - preloadOffset);
    }
    else
    {
      Stream<?> stream = null;

      if (parent instanceof IWorkspace)
      {
        element = messageKeyService.getModulesInWorkspace().skip(index).findFirst().orElse(null);
      }
      else if (parent instanceof IdeModule)
      {
        IdeModule module = (IdeModule) parent;

        if (groupByModules && showImportedKeys)
        {
          Stream<IdeModule> modulesStream = messageKeyService.getImportedModules(module);

          if (groupBySourceType)
          {
            stream = Stream.concat(modulesStream, messageKeyService.getSourceTypes(module, false,
                filterText, showOnlyKeysWithProblems));
          }
          else
          {
            stream = Stream.concat(modulesStream, messageKeyService.getMessageKeys(module, false,
                filterText, showOnlyKeysWithProblems));
          }
        }
        else if (groupBySourceType)
        {
          stream = messageKeyService.getSourceTypes(module.getProject(), true, showImportedKeys,
              filterText, showOnlyKeysWithProblems);
        }
        else
        {
          stream = messageKeyService.getMessageKeys(module.getProject(), true, showImportedKeys,
              filterText, showOnlyKeysWithProblems);
        }
      }
      else if (parent instanceof IdeSourceType)
      {
        IdeSourceType sourceType = (IdeSourceType) parent;
        stream = messageKeyService.getMessageKeys(sourceType, true, showImportedKeys, filterText,
            showOnlyKeysWithProblems);
      }

      if (stream != null)
      {
        preloadTimestamp = System.currentTimeMillis();
        preloadParent = parent;
        preloadOffset = Math.max(index - 50, 0);
        preloadElements = stream.skip(preloadOffset).limit(100).collect(toList());

        // in case retrieval did not return the expected amount of elements
        if (index - preloadOffset < preloadElements.size())
        {
          element = preloadElements.get(index - preloadOffset);
        }
      }
    }

    if (element != null)
    {
      treeViewer.replace(parent, index, element);
      updateChildCount(element, -1);
    }
  }

  @Override
  public Object getParent(Object element)
  {
    return null;
  }


  public TreePath getTreePathFor(String messageKey)
  {
    IdeMessageKey key = messageKeyService.findMessageKey(messageKey, true, false).orElse(null);
    if (key == null)
      return null;

    IProject project = key.getProject();
    String moduleName = key.getModuleName();

    TreePath treePath;
    if (getGroupBySourceType())
    {
      treePath = new TreePath(
          new Object[] {new IdeModule(project, moduleName, null, null, null, null, false, true),
              new IdeSourceType(project, moduleName, key.getSourceType(), false, true), key});
    }
    else
    {
      treePath = new TreePath(new Object[] {
          new IdeModule(project, moduleName, null, null, null, null, false, true), key});
    }
    return treePath;
  }

}
