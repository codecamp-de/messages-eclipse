package de.codecamp.messages.eclipse.ide.ui;


import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;
import static java.util.stream.Collectors.toList;

import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.validation.BundleFileValidator;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.eclipse.core.resources.IMarker;


public final class MessageUiUtils
{

  private MessageUiUtils()
  {
    // utility class
  }


  public static String getBundleErrorsTooltipText(MessageKeyService messageKeyService,
      Object element)
  {
    if (element instanceof IdeMessageKey)
    {
      IdeMessageKey messageKey = (IdeMessageKey) element;

      List<IMarker> markers =
          messageKeyService.findBundleValidationMarkers(messageKey, null).collect(toList());
      if (markers.isEmpty())
        return null;

      Collections.sort(markers,
          comparing(m -> m.getAttribute(BundleFileValidator.MARKER_ATTR_LOCALE, null),
              nullsFirst(naturalOrder())));

      StringBuilder tooltip = new StringBuilder();

      boolean first = true;
      for (IMarker marker : markers)
      {
        if (first)
          first = false;
        else
          tooltip.append("\n");

        String locale = marker.getAttribute(BundleFileValidator.MARKER_ATTR_LOCALE, null);
        String message = marker.getAttribute(IMarker.MESSAGE, null);

        if (message != null)
        {
          if (locale != null)
          {
            if (Locale.ROOT.toString().equals(locale))
              tooltip.append("Base");
            else
              tooltip.append(locale);
            tooltip.append(": ");
          }
          tooltip.append(message);
        }
      }
      return tooltip.toString();
    }
    return null;
  }

  public static String getBundleErrorsTooltipText(MessageKeyService messageKeyService,
      Object element, Locale locale)
  {
    if (element instanceof IdeMessageKey)
    {
      IdeMessageKey messageKey = (IdeMessageKey) element;

      List<IMarker> markers =
          messageKeyService.findBundleValidationMarkers(messageKey, locale).collect(toList());
      if (markers.isEmpty())
        return null;

      StringBuilder tooltip = new StringBuilder();

      boolean first = true;
      for (IMarker marker : markers)
      {
        if (first)
          first = false;
        else
          tooltip.append("\n");

        String message = marker.getAttribute(IMarker.MESSAGE, null);

        if (message != null)
        {
          if (locale != null)
          {
            if (Locale.ROOT.equals(locale))
              tooltip.append("Base");
            else
              tooltip.append(locale);
            tooltip.append(": ");
          }
          tooltip.append(message);
        }
      }
      return tooltip.toString();
    }
    return null;
  }

}
