package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.eclipse.ide.actions.OpenMessageKeyDeclarationAction;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.IdeSourceType;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import javax.inject.Inject;
import javax.inject.Named;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.viewers.IStructuredSelection;


/**
 * Opens a Java editor for the file and location where the message key was declared.
 */
public class OpenMessageKeyDeclarationHandler
{

  public static final String COMMAND_ID = "de.codecamp.messages.messagekey.opendeclaration";


  @Inject
  private MessageKeyService messageKeyService;


  @CanExecute
  @Evaluate
  public boolean canExecute(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    return selection.getFirstElement() instanceof IdeSourceType
        || selection.getFirstElement() instanceof IdeMessageKey;
  }

  @Execute
  public void execute(@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    try
    {
      if (selection.getFirstElement() instanceof IdeSourceType)
      {
        IdeSourceType sourceType = (IdeSourceType) selection.getFirstElement();

        IJavaProject javaProject = JavaCore.create(sourceType.getProject());
        IType type = javaProject.findType(sourceType.getSourceTypeName());
        if (type != null)
          JavaUI.openInEditor(type);
      }

      else
      {
        IdeMessageKey messageKey = (IdeMessageKey) selection.getFirstElement();

        OpenMessageKeyDeclarationAction action =
            new OpenMessageKeyDeclarationAction(messageKeyService);
        action.execute(messageKey, null);
      }
    }
    catch (CoreException | RuntimeException ex)
    {
      StatusUtils.showError(getClass(), "",
          "Failed to open the type where the message key was declared.", ex);
    }
  }

}
