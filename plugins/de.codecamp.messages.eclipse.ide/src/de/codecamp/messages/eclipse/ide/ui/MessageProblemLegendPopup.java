package de.codecamp.messages.eclipse.ide.ui;


import de.codecamp.messages.eclipse.ide.MessagesPlugin;
import de.codecamp.messages.eclipse.ide.util.jface.Stylers;
import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.preference.JFacePreferences;
import org.eclipse.jface.viewers.BoldStylerProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;


public class MessageProblemLegendPopup
  extends
    PopupDialog
{

  private final Button legendButton;

  private final BoldStylerProvider boldStylerProvider;


  public MessageProblemLegendPopup(Button legendButton, BoldStylerProvider boldStylerProvider)
  {
    super(legendButton.getShell(), SWT.NONE, true, false, false, false, false,
          "Legend for Target Locales", null);

    this.legendButton = legendButton;
    this.boldStylerProvider = boldStylerProvider;
  }


  public static Button createLegendButton(Composite parent, BoldStylerProvider boldStylerProvider)
  {
    Button button = new Button(parent, SWT.NONE);
    button.setImage(MessagesPlugin.getDefault()
        .getImage("platform:/plugin/org.eclipse.ui/icons/full/etool16/help_contents.png"));
    button.addSelectionListener(SelectionListener.widgetSelectedAdapter(event ->
    {
      MessageProblemLegendPopup legendPopup =
          new MessageProblemLegendPopup(button, boldStylerProvider);
      legendPopup.open();
    }));
    return button;
  }

  @Override
  protected void adjustBounds()
  {
    super.adjustBounds();
    Point location = legendButton.getParent().toDisplay(legendButton.getLocation());
    location.y += legendButton.getSize().y;
    getShell().setLocation(location);

    setBlockOnOpen(true);
  }

  @Override
  protected Control createDialogArea(Composite parent)
  {
    Composite composite = (Composite) super.createDialogArea(parent);
    GridLayoutFactory.createFrom((GridLayout) composite.getLayout()).margins(15, 7)
        .applyTo(composite);

    StyledText legendItemsText = new StyledText(composite, SWT.READ_ONLY | SWT.NO_FOCUS);
    StyledString legendItemBuilder = new StyledString();

    legendItemBuilder.append("Bundle file doesn't exist (yet) for locale",
        StyledString.QUALIFIER_STYLER);
    legendItemBuilder.append("\n\n");
    legendItemBuilder.append("Required locale", boldStylerProvider.getBoldStyler());
    legendItemBuilder.append("\n\n");
    legendItemBuilder.append("Message key present in bundle file for locale",
        Stylers.underline(SWT.UNDERLINE_SINGLE));
    legendItemBuilder.append("\n\n");
    legendItemBuilder.append("Problems found",
        StyledString.createColorRegistryStyler(JFacePreferences.ERROR_COLOR, null));
    legendItemBuilder.append("\n\n");
    legendItemBuilder.append("Locale provided by imported message module", Stylers.strikeout());

    legendItemsText.setText(legendItemBuilder.getString());
    legendItemsText.setStyleRanges(legendItemBuilder.getStyleRanges());

    return composite;
  }

}
