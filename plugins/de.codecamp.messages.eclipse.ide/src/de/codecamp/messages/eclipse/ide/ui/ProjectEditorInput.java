package de.codecamp.messages.eclipse.ide.ui;


import de.codecamp.messages.eclipse.ide.MessagesPlugin;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.model.IWorkbenchAdapter;


public class ProjectEditorInput
  extends
    PlatformObject
  implements
    IProjectEditorInput,
    IPersistableElement
{

  private final IProject project;

  private IFile linkedFile;


  public ProjectEditorInput(IProject project)
  {
    if (project == null)
      throw new IllegalArgumentException("project must not be null");
    this.project = project;
  }

  @Override
  public IProject getProject()
  {
    return project;
  }

  @Override
  public String getName()
  {
    return project.getName();
  }

  @Override
  public String getToolTipText()
  {
    return project.getFullPath().makeRelative().toString();
  }

  @Override
  public ImageDescriptor getImageDescriptor()
  {
    return MessagesPlugin.getDefault().getImageDescriptor("/icons/messages_edit.png");
  }

  @Override
  public boolean exists()
  {
    return project.exists();
  }

  @Override
  public IPersistableElement getPersistable()
  {
    return this;
  }

  @Override
  public String getFactoryId()
  {
    return ProjectEditorInputFactory.getFactoryId();
  }

  @Override
  public void saveState(IMemento memento)
  {
    ProjectEditorInputFactory.saveState(memento, this);
  }

  @Override
  public IFile getLinkedFile()
  {
    return linkedFile;
  }

  @Override
  public void setLinkedFile(IFile linkedFile)
  {
    this.linkedFile = linkedFile;
  }

  @Override
  public <T> T getAdapter(Class<T> adapterType)
  {
    if (adapterType == IWorkbenchAdapter.class)
    {
      return adapterType.cast(new IWorkbenchAdapter()
      {

        @Override
        public Object[] getChildren(Object o)
        {
          return new Object[0];
        }

        @Override
        public ImageDescriptor getImageDescriptor(Object object)
        {
          return ProjectEditorInput.this.getImageDescriptor();
        }

        @Override
        public String getLabel(Object o)
        {
          return ProjectEditorInput.this.getName();
        }

        @Override
        public Object getParent(Object o)
        {
          return ProjectEditorInput.this.getProject().getParent();
        }

      });
    }
    else if (adapterType == IFile.class && linkedFile != null)
    {
      return adapterType.cast(linkedFile);
    }

    return super.getAdapter(adapterType);
  }


  @Override
  public boolean equals(Object obj)
  {
    if (obj == this)
      return true;
    if (!(obj instanceof IProjectEditorInput))
      return false;

    IProjectEditorInput other = (IProjectEditorInput) obj;

    return getProject().equals(other.getProject());
  }

  @Override
  public int hashCode()
  {
    return project.hashCode();
  }

  @Override
  public String toString()
  {
    return getClass().getName() + "(" + getProject().getFullPath() + ")";
  }

}
