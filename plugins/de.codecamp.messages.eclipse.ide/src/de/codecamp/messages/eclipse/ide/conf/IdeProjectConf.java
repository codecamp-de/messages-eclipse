package de.codecamp.messages.eclipse.ide.conf;


import de.codecamp.messages.eclipse.ide.util.eclipse.ResourceUtils;
import de.codecamp.messages.shared.bundle.MessageBundleManager;
import de.codecamp.messages.shared.bundle.MessageBundleManager.BundleFileCoordinates;
import de.codecamp.messages.shared.conf.ConfValueProvider;
import de.codecamp.messages.shared.conf.ProjectConf;
import de.codecamp.messages.shared.conf.ProjectConfException;
import java.util.Locale;
import java.util.Optional;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;


public class IdeProjectConf
  extends
    ProjectConf
{

  private final IProject project;


  public IdeProjectConf(IProject project, ConfValueProvider confProvider)
    throws ProjectConfException
  {
    super(confProvider);
    this.project = project;
  }


  public IProject getProject()
  {
    return project;
  }

  public Optional<IFolder> getBundleDirAsFolder()
  {
    return Optional.ofNullable(getBundleDir())
        .map(bundleDir -> ResourceUtils.getFolderInProjectByPath(bundleDir, project));
  }

  public boolean isMessageBundleFile(IResource resource)
  {
    if (resource.getType() != IResource.FILE)
      return false;

    if (!MessageBundleManager.PROPERTIES_EXT.equals(resource.getFileExtension()))
      return false;

    IFolder bundleDir = getBundleDirAsFolder().orElse(null);
    if (bundleDir == null)
      return false;

    if (!bundleDir.getFullPath().isPrefixOf(resource.getFullPath()))
      return false;

    BundleFileCoordinates coordinates = MessageBundleManager.toBundleFileCoordinates(
        resource.getFullPath().makeRelativeTo(bundleDir.getFullPath()).toString());
    if (coordinates == null)
      return false;

    if (!coordinates.getLocale().equals(Locale.ROOT)
        && !getTargetLocales().contains(coordinates.getLocale()))
      return false;

    if (getIgnoredBundles().contains(coordinates.getBundleName()))
      return false;

    return true;
  }

  public boolean isMessageBundleDir(IResource resource)
  {
    IFolder bundleDir = getBundleDirAsFolder().orElse(null);
    if (bundleDir == null)
      return false;

    return resource.equals(bundleDir);
  }

  public EclipseMessageBundleManager createEclipseMessageBundleManager()
  {
    if (!getBundleDirAsFolder().isPresent())
      throw new IllegalStateException("Bundle directory not found or not configured correctly.");

    return new EclipseMessageBundleManager(this);
  }

}
