package de.codecamp.messages.eclipse.ide.actions;


import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.shared.bundle.BundleFile;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;


public class FixRemovedKeysAction
{

  private final MessageKeyService messageKeyService;


  public FixRemovedKeysAction(MessageKeyService messageKeyService)
  {
    this.messageKeyService = messageKeyService;
  }


  public boolean execute(IdeProjectConf projectConf, EclipseMessageBundleManager bundleManager)
  {
    Map<String, IdeMessageKey> declaredMessageKeys =
        messageKeyService.getMessageKeys(projectConf.getProject(), true, true, null, false)
            .collect(toMap(IdeMessageKey::getCode, k -> k));

    boolean localBundleManager = false;
    if (bundleManager == null)
    {
      localBundleManager = true;
      bundleManager = projectConf.createEclipseMessageBundleManager();
    }

    Set<IdeMessageKey> addedKeys;
    Map<String, String> removedKeys = new HashMap<>();

    Set<String> messagesFound = new HashSet<>();

    for (BundleFile<IFile, CoreException> bundleFile : bundleManager.getBundleFiles())
    {
      for (String code : bundleFile.getKeys())
      {
        IdeMessageKey messageKey = declaredMessageKeys.get(code);
        if (messageKey == null)
        {
          // empty messages can just be removed
          if (bundleFile.isMessageEmpty(code))
            bundleFile.removeMessage(code);
          else
            removedKeys.put(code, null);
        }

        if (!bundleFile.isMessageEmpty(code))
          messagesFound.add(code);
      }
    }

    addedKeys = declaredMessageKeys.values().stream()
        .filter(k -> !messagesFound.contains(k.getCode())).collect(toSet());

    if (!removedKeys.isEmpty())
    {
      OpenKeysRemovedDialogAction openKeysRemovedDialogAction =
          new OpenKeysRemovedDialogAction(messageKeyService);
      openKeysRemovedDialogAction.execute(projectConf.getProject(), removedKeys,
          addedKeys.stream().collect(toMap(IdeMessageKey::getCode, IdeMessageKey::getSourceType)),
          bundleManager);

      if (localBundleManager)
      {
        bundleManager.save();
      }

      return true;
    }
    else
    {
      return false;
    }
  }

}
