package de.codecamp.messages.eclipse.ide.ui;


import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;


public class ProjectEditorInputFactory
  implements
    IElementFactory
{

  private static final String ID_FACTORY =
      "de.codecamp.messages.eclipse.ide.editors.ProjectEditorInputFactory";

  private static final String PROJECT_NAME = "projectName";


  public ProjectEditorInputFactory()
  {
  }


  @Override
  public IAdaptable createElement(IMemento memento)
  {
    String projectName = memento.getString(PROJECT_NAME);
    if (projectName == null)
    {
      return null;
    }

    IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
    if (project != null)
      return new ProjectEditorInput(project);

    return null;
  }

  public static String getFactoryId()
  {
    return ID_FACTORY;
  }

  public static void saveState(IMemento memento, ProjectEditorInput input)
  {
    IProject project = input.getProject();
    memento.putString(PROJECT_NAME, project.getFullPath().toString());
  }

}
