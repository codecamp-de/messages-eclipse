package de.codecamp.messages.eclipse.ide.util.eclipse;


import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.List;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;


public final class ResourceUtils
{

  private ResourceUtils()
  {
    // utility class
  }


  public static IFolder getFolderInProjectByPath(String folderPath, IProject project)
  {
    IPath projectLocation = project.getLocation();
    IPath folderLocation = Path.fromOSString(folderPath).makeAbsolute();
    if (!projectLocation.isPrefixOf(folderLocation))
      return null;

    IPath projectRelativePath = folderLocation.makeRelativeTo(projectLocation);
    return project.getFolder(projectRelativePath);
  }

  public static IFile getFileInProjectByPath(String filePath, IProject project)
  {
    return project.getFile(Path.fromOSString(filePath).makeRelativeTo(project.getLocation()));
  }

  /**
   * Detects if the given file is from the innermost project known to the workspace.
   *
   * @param file
   *          the file to be tested
   * @return whether the given file is from the innermost project known to the workspace
   */
  public static boolean isFromInnermostProject(IFile file)
  {
    IFile[] findFilesForLocationURI =
        ResourcesPlugin.getWorkspace().getRoot().findFilesForLocationURI(file.getRawLocationURI());
    Arrays.sort(findFilesForLocationURI,
        Comparator.comparingInt(f -> f.getProjectRelativePath().toString().length()));
    return file.equals(findFilesForLocationURI[0]);
  }


  public static List<IFile> findFilesInProjectByName(IProject project, String filename)
    throws CoreException
  {
    List<IFile> files = new ArrayList<>();
    project.accept(resource ->
    {
      if (resource instanceof IFile && resource.getName().equals(filename))
        files.add((IFile) resource);

      return true;
    });
    return files;
  }

  public static void createFolders(IFolder folder)
    throws CoreException
  {
    Deque<IFolder> folders = new ArrayDeque<>(folder.getProjectRelativePath().segmentCount());

    while (!folder.exists())
    {
      folders.push(folder);

      IContainer parent = folder.getParent();
      if (parent instanceof IFolder)
        folder = (IFolder) parent;
      else
        break;
    }

    while (!folders.isEmpty())
    {
      folders.pop().create(true, true, null);
    }
  }

}
