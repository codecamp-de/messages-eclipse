package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Shell;


/**
 * Copies the selected message key to the clipboard.
 */
public class CopyMessageKeyHandler
{

  @CanExecute
  @Evaluate
  public boolean canExecute(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    return selection.getFirstElement() instanceof IdeMessageKey;
  }

  @Execute
  public void execute(@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
      @Named(IServiceConstants.ACTIVE_SHELL) Shell shell)
  {
    try
    {
      IdeMessageKey messageKey = (IdeMessageKey) selection.getFirstElement();

      Clipboard clipboard = new Clipboard(shell.getDisplay());
      try
      {
        List<Object> datas = new ArrayList<>();
        List<ByteArrayTransfer> transfers = new ArrayList<>();

        datas.add(messageKey.getCode());
        transfers.add(TextTransfer.getInstance());

        setClipboardContents(clipboard, datas.toArray(),
            transfers.toArray(new Transfer[transfers.size()]));
      }
      finally
      {
        clipboard.dispose();
      }
    }
    catch (RuntimeException ex)
    {
      StatusUtils.showError(getClass(), "", "Failed to copy message key to clipboard.", ex);
    }
  }

  private void setClipboardContents(Clipboard clipboard, Object[] datas, Transfer[] transfers)
  {
    try
    {
      clipboard.setContents(datas, transfers);
    }
    catch (SWTError e)
    {
      if (e.code != DND.ERROR_CANNOT_SET_CLIPBOARD)
      {
        throw e;
      }
      // silently fail. see e.g. https://bugs.eclipse.org/bugs/show_bug.cgi?id=65975
    }
  }

}
