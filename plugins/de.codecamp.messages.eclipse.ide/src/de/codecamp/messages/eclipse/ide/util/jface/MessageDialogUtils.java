package de.codecamp.messages.eclipse.ide.util.jface;


import java.text.MessageFormat;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;


public final class MessageDialogUtils
{

  private MessageDialogUtils()
  {
    // utility class
  }


  public static void showInfo(String title, @NonNull String message, Object... formatArgs)
  {
    show(MessageDialog.INFORMATION, title, message, formatArgs);
  }

  private static void show(int kind, String title, @NonNull String message, Object... formatArgs)
  {
    message = MessageFormat.format(message, formatArgs);
    MessageDialog.open(kind, Display.getCurrent().getActiveShell(), title, message, SWT.NONE);
  }

}
