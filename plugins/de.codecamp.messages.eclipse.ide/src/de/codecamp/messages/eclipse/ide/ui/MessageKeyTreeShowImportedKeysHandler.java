package de.codecamp.messages.eclipse.ide.ui;


import de.codecamp.messages.eclipse.ide.util.eclipse.E4CompatibilityUtils;
import javax.inject.Inject;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.workbench.UIEvents;


public class MessageKeyTreeShowImportedKeysHandler
{

  @Inject
  private IEventBroker eventBroker;


  @CanExecute
  public boolean canExecute(MPart part, MItem item)
  {
    MessageKeyTreeAdapter adapter = E4CompatibilityUtils.adapt(part, MessageKeyTreeAdapter.class);
    if (adapter == null)
      return false;

    item.setSelected(adapter.getShowImportedKeys());

    return true;
  }

  @Execute
  public void execute(MPart part, MItem item)
  {
    MessageKeyTreeAdapter adapter = E4CompatibilityUtils.adapt(part, MessageKeyTreeAdapter.class);
    if (adapter == null)
      return;

    boolean flag = !adapter.getShowImportedKeys();

    adapter.setShowImportedKeys(flag);

    item.setSelected(flag);
    eventBroker.send(UIEvents.REQUEST_ENABLEMENT_UPDATE_TOPIC, UIEvents.ALL_ELEMENT_ID);
  }

}
