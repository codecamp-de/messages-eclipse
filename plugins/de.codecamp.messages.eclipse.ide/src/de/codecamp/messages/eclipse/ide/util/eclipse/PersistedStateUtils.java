package de.codecamp.messages.eclipse.ide.util.eclipse;


import java.util.Map;


public final class PersistedStateUtils
{

  private PersistedStateUtils()
  {
    // utility class
  }


  public static boolean getBoolean(Map<String, String> persistedState, String key,
      Boolean defaultValue)
  {
    Boolean value = null;

    String valueString = persistedState.get(key);

    if (valueString != null)
      value = Boolean.parseBoolean(valueString);

    if (value == null)
      value = defaultValue;

    return value;
  }

}
