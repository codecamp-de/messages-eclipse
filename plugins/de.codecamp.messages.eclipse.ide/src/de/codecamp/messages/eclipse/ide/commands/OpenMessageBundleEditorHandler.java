package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.ui.MessageBundleEditor;
import de.codecamp.messages.eclipse.ide.ui.ProjectEditorInput;
import de.codecamp.messages.eclipse.ide.util.eclipse.E4CompatibilityUtils;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;


public class OpenMessageBundleEditorHandler
  extends
    AbstractMessageBundlesHandler
{

  public static final String COMMAND_ID = "de.codecamp.messages.messagebundle.openeditor";


  @Inject
  private EPartService partService;


  @Override
  @CanExecute
  @Evaluate
  public boolean canExecute(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    MPart activePart = partService.getActivePart();
    if (activePart != null
        && E4CompatibilityUtils.getPartObject(activePart) instanceof MessageBundleEditor)
    {
      return false;
    }

    return true;
  }

  @Execute
  public void execute(@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
    throws CoreException
  {
    Map<IProject, IdeMessageKey> projects = new HashMap<>();
    for (Object element : selection.toList())
    {
      IdeMessageKey messageKey = null;
      if (element instanceof IdeMessageKey)
        messageKey = (IdeMessageKey) element;

      IProject project = HandlerUtils.getProject(element).orElse(null);
      if (project != null)
      {
        projects.putIfAbsent(project, messageKey);
      }
    }

    for (Map.Entry<IProject, IdeMessageKey> entry : projects.entrySet())
    {
      IEditorPart editor =
          IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(),
              new ProjectEditorInput(entry.getKey()), MessageBundleEditor.ID);

      if (editor instanceof MessageBundleEditor && entry.getValue() != null)
      {
        MessageBundleEditor bundleEditor = (MessageBundleEditor) editor;
        bundleEditor.selectMessageKey(entry.getValue().getCode(), null, null);
      }
    }
  }

}
