package de.codecamp.messages.eclipse.ide.ui;


import de.codecamp.messages.eclipse.ide.util.eclipse.E4CompatibilityUtils;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;


public class MessageKeyTreeCollapseAllHandler
{

  @Execute
  public void execute(MPart part)
  {
    MessageKeyTreeAdapter adapter = E4CompatibilityUtils.adapt(part, MessageKeyTreeAdapter.class);
    if (adapter == null)
      return;

    adapter.collapseAll();
  }

}
