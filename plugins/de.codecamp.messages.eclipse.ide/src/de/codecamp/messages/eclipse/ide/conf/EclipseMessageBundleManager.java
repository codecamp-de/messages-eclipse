package de.codecamp.messages.eclipse.ide.conf;


import de.codecamp.messages.eclipse.ide.services.EclipseFileSystemAdapter;
import de.codecamp.messages.shared.bundle.MessageBundleManager;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;


public class EclipseMessageBundleManager
  extends
    MessageBundleManager<IFolder, IFile, CoreException>
{

  public EclipseMessageBundleManager(IdeProjectConf projectConf)
  {
    super(projectConf, new EclipseFileSystemAdapter(projectConf.getProject()));
  }

}
