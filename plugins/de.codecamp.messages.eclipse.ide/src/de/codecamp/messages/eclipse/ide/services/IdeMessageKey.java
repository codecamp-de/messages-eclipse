package de.codecamp.messages.eclipse.ide.services;


import de.codecamp.messages.shared.model.MessageKeyWithSourceLocation;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;


public class IdeMessageKey
  extends
    MessageKeyWithSourceLocation
  implements
    IAdaptable
{

  private final IProject project;

  private final String moduleName;

  private final boolean imported;

  private final boolean fromWorkspace;


  public IdeMessageKey(IProject project, String moduleName, MessageKeyWithSourceLocation messageKey,
      boolean imported, boolean fromWorkspace)
  {
    super(messageKey.getCode(), messageKey.getArgTypes(), messageKey.getArgNames(),
          messageKey.getSourceType(), messageKey.getLocalPart(), messageKey.getSourceElementType(),
          messageKey.getSourceElementName(), messageKey.isDefaultMessageAvailable());

    this.project = project;
    this.moduleName = moduleName;
    this.imported = imported;
    this.fromWorkspace = fromWorkspace;
  }


  /**
   * Returns the queried project.
   *
   * @return the queried project
   */
  public IProject getProject()
  {
    return project;
  }

  /**
   * Returns the module in which the message key has been declared.
   *
   * @return the module in which the message key has been declared
   */
  public String getModuleName()
  {
    return moduleName;
  }

  /**
   * Returns whether the message key is imported from the perspective of the queried
   * {@link #getProject()}.
   *
   * @return whether the message key is imported
   */
  public boolean isImported()
  {
    return imported;
  }

  /**
   * Returns whether the message key (imported or not) is from a workspace project or from a
   * packaged dependency, i.e. a JAR.
   *
   * @return whether the message key is from a workspace project
   */
  public boolean isFromWorkspace()
  {
    return fromWorkspace;
  }


  @Override
  public <T> T getAdapter(Class<T> adapter)
  {
    // adapting to IProject messes too much with context menues

    return null;
  }


  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 0;
    result = prime * result + getProject().getName().hashCode();
    result = prime * result + getModuleName().hashCode();
    result = prime * result + getSourceType().hashCode();
    result = prime * result + getCode().hashCode();
    return result;
  }


  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
      return false;
    if (obj == this)
      return true;
    if (getClass() != obj.getClass())
      return false;

    IdeMessageKey other = (IdeMessageKey) obj;
    if (!getProject().getName().equals(other.getProject().getName()))
      return false;
    if (!getModuleName().equals(other.getModuleName()))
      return false;
    if (!getSourceType().equals(other.getSourceType()))
      return false;
    if (!getCode().equals(other.getCode()))
      return false;

    return true;
  }

}
