package de.codecamp.messages.eclipse.ide.commands;


import static java.util.stream.Collectors.joining;

import de.codecamp.messages.eclipse.ide.actions.CleanUpBundleFileAction;
import de.codecamp.messages.eclipse.ide.actions.FixBundleMismatchesAction;
import de.codecamp.messages.eclipse.ide.actions.FixRemovedKeysAction;
import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.util.jface.MessageDialogUtils;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;


public class FixMessageBundleProblemsHandler
  extends
    AbstractMessageBundlesHandler
{

  @Execute
  public void execute(@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
    throws CoreException
  {
    if (selection == null || selection.isEmpty())
      return;


    FixBundleMismatchesAction fixBundleMismatchesAction = new FixBundleMismatchesAction();

    FixRemovedKeysAction fixRemovedKeysAction = new FixRemovedKeysAction(messageKeyService);

    CleanUpBundleFileAction cleanUpAction = new CleanUpBundleFileAction(messageKeyService);

    int messagesMoved = 0;
    boolean orphanedMessagesFound = false;

    for (Object element : selection.toList())
    {
      IdeProjectConf projectConf = getProjectConfFromSelectionElement(element);
      if (projectConf == null)
        continue;


      EclipseMessageBundleManager bundleManager = projectConf.createEclipseMessageBundleManager();


      messagesMoved += fixBundleMismatchesAction.execute(projectConf, bundleManager);

      if (fixRemovedKeysAction.execute(projectConf, bundleManager))
        orphanedMessagesFound = true;

      cleanUpAction.execute(projectConf, bundleManager, null);


      bundleManager.save(true);
    }


    List<String> summaryItems = new ArrayList<>();

    if (messagesMoved > 0)
      summaryItems.add(MessageFormat
          .format("{0} messages have been moved to their proper message bundle.", messagesMoved));
    else
      summaryItems.add("No messages needed to be moved to a different message bundle.");

    if (!orphanedMessagesFound)
      summaryItems.add("No orphaned messages found to be fixed.");

    MessageDialogUtils.showInfo(null, summaryItems.stream().collect(joining("\n\n")));
  }

}
