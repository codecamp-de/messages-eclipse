package de.codecamp.messages.eclipse.ide.util.swt;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BinaryOperator;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Widget;


public class EventDelay
{

  /**
   * scheduler to delay the events
   */
  private static final ScheduledExecutorService EVENT_SCHEDULER =
      Executors.newSingleThreadScheduledExecutor();

  private static final int DEFAULT_DELAY = 500;


  private final Widget eventSource;

  private final Listener eventListener;

  private final BinaryOperator<Event> merger = (newEvent, previousEvent) -> newEvent;

  /**
   * widget -> event type -> listener entry
   */
  private final Map<Integer, EventTypeEntry> eventTypeEntries = new HashMap<>();


  /**
   * Constructs a new {@link EventDelay}.
   *
   * @param eventSource
   *          the original event source
   */
  public EventDelay(Widget eventSource)
  {
    this.eventSource = eventSource;

    eventSource.addDisposeListener(event -> dispose());

    eventListener = this::delayEvent;
  }


  private EventTypeEntry getEventTypeRegistration(int eventType, boolean register)
  {
    EventTypeEntry entry = eventTypeEntries.get(eventType);
    if (entry == null && register)
    {
      entry = new EventTypeEntry();
      eventTypeEntries.put(eventType, entry);
      eventSource.addListener(eventType, eventListener);
    }
    return entry;
  }

  private void tryUnregisterEventType(int eventType)
  {
    EventTypeEntry entry = eventTypeEntries.get(eventType);
    if (entry != null)
    {
      if (entry.listeners.isEmpty())
      {
        eventTypeEntries.remove(eventType);
        eventSource.removeListener(eventType, eventListener);
      }
    }
  }


  /**
   * Flush any delayed event for the given event type.
   *
   * @param eventType
   *          the event type
   */
  public void flush(int eventType)
  {
    EventTypeEntry entry = eventTypeEntries.get(eventType);
    if (entry == null)
    {
      throw new IllegalArgumentException("event type not registered for event source");
    }

    fireEvent(entry);
  }

  public void flushAll()
  {
    for (EventTypeEntry entry : eventTypeEntries.values())
    {
      if (entry.delayedEvent != null)
        flush(entry.delayedEvent.type);
    }
  }

  public void cancel(int eventType)
  {
    EventTypeEntry entry = eventTypeEntries.get(eventType);
    if (entry == null)
    {
      throw new IllegalArgumentException("Event type not registered for event source.");
    }

    synchronized (entry)
    {
      if (entry.delayedEventDispatch != null)
      {
        entry.delayedEventDispatch.cancel(false);
        entry.delayedEventDispatch = null;
        entry.delayedEvent = null;
      }
    }
  }



  public void addListener(int eventType, Listener listener)
  {
    getEventTypeRegistration(eventType, true).listeners.add(listener);
  }

  public void addListener(int eventType, Listener listener, int delay)
  {
    EventTypeEntry entry = getEventTypeRegistration(eventType, true);
    if (entry.delay != null && entry.delay != delay)
    {
      throw new IllegalArgumentException(
          "A different delay has already been registered for this event type.");
    }

    entry.delay = delay;
    entry.listeners.add(listener);
  }

  public void removeListener(int eventType, Listener listener)
  {
    EventTypeEntry entry = getEventTypeRegistration(eventType, false);
    if (entry != null)
    {
      entry.listeners.remove(listener);
      tryUnregisterEventType(eventType);
    }
  }

  private void delayEvent(Event event)
  {
    EventTypeEntry entry = eventTypeEntries.get(event.type);

    synchronized (entry)
    {
      if (entry.delayedEvent != null)
      {
        entry.delayedEvent = merger.apply(event, entry.delayedEvent);
      }
      else
      {
        entry.delayedEvent = event;
      }

      if (entry.delayedEventDispatch != null)
        entry.delayedEventDispatch.cancel(false);

      Event delayedEvent = entry.delayedEvent;

      Runnable dispatchEventCommand = () ->
      {
        if (delayedEvent.display.isDisposed())
          return;

        delayedEvent.display.asyncExec(() ->
        {
          fireEvent(entry);
        });
      };

      Integer delay = entry.delay;
      if (delay == null)
        delay = DEFAULT_DELAY;

      entry.delayedEventDispatch =
          EVENT_SCHEDULER.schedule(dispatchEventCommand, delay, TimeUnit.MILLISECONDS);
    }
  }

  /**
   * Fires the given event to all listeners registered for its source and event type.
   *
   * @param event
   *          the event to fire
   */
  private void fireEvent(EventTypeEntry entry)
  {
    synchronized (entry)
    {
      if (entry.delayedEvent == null)
        return;

      entry.delayedEventDispatch.cancel(false);
      entry.delayedEventDispatch = null;

      for (Listener listener : entry.listeners)
      {
        listener.handleEvent(entry.delayedEvent);
      }
      entry.delayedEvent = null;
    }
  }


  public void dispose()
  {
    for (EventTypeEntry entry : eventTypeEntries.values())
    {
      if (entry.delayedEventDispatch != null)
        entry.delayedEventDispatch.cancel(false);
    }
  }


  private static class EventTypeEntry
  {

    final List<Listener> listeners = new ArrayList<>();

    Integer delay;

    Event delayedEvent;

    ScheduledFuture<?> delayedEventDispatch;

  }

}
