package de.codecamp.messages.eclipse.ide.util.eclipse;


import java.text.MessageFormat;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.wst.validation.AbstractValidator;
import org.eclipse.wst.validation.ValidationResult;
import org.eclipse.wst.validation.ValidatorMessage;
import org.slf4j.helpers.MessageFormatter;


public abstract class AbstractResourceValidator
  extends
    AbstractValidator
{

  protected String getMarkerId()
  {
    return null;
  }

  protected ValidatorMessage addInfoMessage(ValidationResult result, IResource resource,
      String message, Object... formatArgs)
  {
    return addMessage(result, IMarker.SEVERITY_INFO, resource, message, formatArgs);
  }

  protected ValidatorMessage addWarningMessage(ValidationResult result, IResource resource,
      String message, Object... formatArgs)
  {
    return addMessage(result, IMarker.SEVERITY_WARNING, resource, message, formatArgs);
  }

  protected ValidatorMessage addErrorMessage(ValidationResult result, IResource resource,
      String message, Object... formatArgs)
  {
    return addMessage(result, IMarker.SEVERITY_ERROR, resource, message, formatArgs);
  }

  private ValidatorMessage addMessage(ValidationResult result, int severity, IResource resource,
      String message, Object... formatArgs)
  {
    message = MessageFormatter.arrayFormat(message, formatArgs).getMessage();
    return addMessage(result, severity, resource, -1, message, formatArgs);
  }


  protected ValidatorMessage addInfoMessage(ValidationResult result, IResource resource,
      Integer lineNumber, String message, Object... formatArgs)
  {
    return addMessage(result, IMarker.SEVERITY_INFO, resource, lineNumber, message, formatArgs);
  }

  protected ValidatorMessage addWarningMessage(ValidationResult result, IResource resource,
      Integer lineNumber, String message, Object... formatArgs)
  {
    return addMessage(result, IMarker.SEVERITY_WARNING, resource, lineNumber, message, formatArgs);
  }

  protected ValidatorMessage addErrorMessage(ValidationResult result, IResource resource,
      Integer lineNumber, String message, Object... formatArgs)
  {
    return addMessage(result, IMarker.SEVERITY_ERROR, resource, lineNumber, message, formatArgs);
  }

  protected ValidatorMessage addMessage(ValidationResult result, int severity, IResource resource,
      Integer lineNumber, String message, Object... formatArgs)
  {
    if (formatArgs.length > 0)
      message = MessageFormat.format(message, formatArgs);

    ValidatorMessage dsMessage = ValidatorMessage.create(message, resource);

    // the configured marker ID from plugin.xml is not automatically used
    // https://bugs.eclipse.org/bugs/show_bug.cgi?id=307093
    String markerId = getMarkerId();
    if (markerId != null)
      dsMessage.setType(markerId);

    dsMessage.setAttribute(IMarker.LOCATION, resource.getName());
    dsMessage.setAttribute(IMarker.SEVERITY, severity);
    if (lineNumber != null && lineNumber > 0)
      dsMessage.setAttribute(IMarker.LINE_NUMBER, lineNumber);
    result.add(dsMessage);
    return dsMessage;
  }

}
