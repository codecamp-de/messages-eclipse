package de.codecamp.messages.eclipse.ide.build;


import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.E4Utils;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.inject.Inject;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.compiler.BuildContext;
import org.eclipse.jdt.core.compiler.CompilationParticipant;


public class MessagesCompilationParticipant
  extends
    CompilationParticipant
{

  @Inject
  private MessageKeyService messageKeyService;

  private final ConcurrentMap<IProject, Boolean> isBatchPerProject = new ConcurrentHashMap<>();


  public MessagesCompilationParticipant()
  {
  }


  @Override
  public boolean isActive(IJavaProject project)
  {
    E4Utils.injectOsgiContext(this);
    return true;
  }

  @Override
  public void cleanStarting(IJavaProject project)
  {
    messageKeyService.cleanProject(project.getProject());
  }

  @Override
  public void buildStarting(BuildContext[] files, boolean isBatch)
  {
    IProject project = files[0].getFile().getProject();
    isBatchPerProject.put(project, isBatch);
  }

  @Override
  public void buildFinished(IJavaProject project)
  {
    Boolean isBatch = isBatchPerProject.remove(project.getProject());
    if (Boolean.TRUE.equals(isBatch))
    {
      messageKeyService.refreshProject(project.getProject());
    }
  }

}
