package de.codecamp.messages.eclipse.ide.services;


import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;


public class AbstractItem
  implements
    IAdaptable
{

  private final IProject project;

  private final String moduleName;

  private final boolean imported;

  private final boolean fromWorkspace;


  public AbstractItem(IProject project, String moduleName, boolean imported, boolean fromWorkspace)
  {
    this.project = project;
    this.moduleName = moduleName;
    this.imported = imported;
    this.fromWorkspace = fromWorkspace;
  }


  public IProject getProject()
  {
    return project;
  }

  public String getModuleName()
  {
    return moduleName;
  }

  /**
   * Returns whether this item is imported (from the perspective of the queried
   * {@link #getProject()}).
   *
   * @return whether the message key is imported
   */
  public boolean isImported()
  {
    return imported;
  }

  public boolean isFromWorkspace()
  {
    return fromWorkspace;
  }


  @Override
  public <T> T getAdapter(Class<T> adapter)
  {
    // adapting to IProject or IResource messes too much with context menues

    return null;
  }

}
