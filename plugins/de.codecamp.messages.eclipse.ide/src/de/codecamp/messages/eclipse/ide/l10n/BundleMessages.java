package de.codecamp.messages.eclipse.ide.l10n;


import org.eclipse.e4.core.services.nls.Message;


/**
 * This class can be injected to access (certain) messages {@code OSGI-INF/l10n/bundle.properties}.
 */
@Message
public class BundleMessages
{

  public String command_openMessageKeyDeclaration_name;

  public String command_findMessageKeyUses_name;

  public String command_showInMessageKeysView_name;

  public String command_openMessageBundleEditor_name;

}
