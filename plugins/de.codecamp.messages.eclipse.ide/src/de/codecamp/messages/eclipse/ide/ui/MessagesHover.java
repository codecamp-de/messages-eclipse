package de.codecamp.messages.eclipse.ide.ui;


import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.MessageKeyAstUtils;
import de.codecamp.messages.eclipse.ide.util.eclipse.E4Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.core.manipulation.SharedASTProviderCore;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jdt.ui.text.java.hover.IJavaEditorTextHover;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;


public class MessagesHover
  implements
    IJavaEditorTextHover,
    //    ITextHoverExtension,
    ITextHoverExtension2
{

  @Inject
  private MessageKeyService messageKeyService;


  private IEditorPart editor;


  public MessagesHover()
  {
    E4Utils.injectWorkbenchContext(this);
  }


  @Override
  public void setEditor(IEditorPart editor)
  {
    this.editor = editor;
  }

  @Override
  public String getHoverInfo(ITextViewer textViewer, IRegion hoverRegion)
  {
    throw new UnsupportedOperationException();
  }

  @Override
  public IRegion getHoverRegion(ITextViewer textViewer, int offset)
  {
    return null;
  }

  @Override
  public Object getHoverInfo2(ITextViewer textViewer, IRegion hoverRegion)
  {
    ITypeRoot editorInputElement = getEditorInputJavaElement();
    if (editorInputElement == null)
      return null;

    ASTNode astNode = getHoveredASTNode(editorInputElement, hoverRegion);
    if (astNode == null)
      return null;

    IProject project = editorInputElement.getJavaProject().getProject();

    IdeMessageKey messageKey = MessageKeyAstUtils
        .findMessageKeyFromAstNode(project, astNode, messageKeyService).orElse(null);

    if (messageKey == null)
      return null;

    IdeProjectConf projectConf = messageKeyService.getProjectConf(project, true).get();
    EclipseMessageBundleManager bundleManager = projectConf.createEclipseMessageBundleManager();

    StringBuilder text = new StringBuilder();

    text.append("<b>").append(messageKey.getCode()).append("</b><br/><br/>");

    boolean first = true;
    List<Locale> locales = new ArrayList<>();
    locales.add(Locale.ROOT);
    locales.addAll(projectConf.getTargetLocales());

    for (Locale locale : locales)
    {
      String message = bundleManager.getMessage(messageKey.getCode(), locale);

      if (first)
        first = false;
      else
        text.append("<br/><br/>");

      text.append("<b>");
      if (Locale.ROOT.equals(locale))
        text.append("Base");
      else
        text.append(locale.toString());
      text.append("</b>");

      text.append(": ");

      if (message == null)
        text.append("<em>&lt;not set&gt;</em>");
      else if (message.equals(MessageBundleEditor.EMPTY_MESSAGE))
        text.append("<em>&lt;empty&gt;</em>");
      else
        text.append(message);
    }

    return text.toString();
  }

  protected ITypeRoot getEditorInputJavaElement()
  {
    if (editor == null)
      return null;

    IEditorInput editorInput = editor.getEditorInput();
    if (editorInput == null)
      return null;

    IJavaElement je = JavaUI.getEditorInputJavaElement(editorInput);
    if (je instanceof ITypeRoot)
      return (ITypeRoot) je;

    return null;
  }

  private static ASTNode getHoveredASTNode(ITypeRoot editorInputElement, IRegion hoverRegion)
  {
    if (editorInputElement == null || hoverRegion == null)
      return null;

    CompilationUnit unit = SharedASTProviderCore.getAST(editorInputElement,
        SharedASTProviderCore.WAIT_ACTIVE_ONLY, null);
    if (unit == null)
      return null;

    return NodeFinder.perform(unit, hoverRegion.getOffset(), hoverRegion.getLength());
  }

  //  @Override
  //  public IInformationControlCreator getHoverControlCreator()
  //  {
  //    // TODO Auto-generated method stub
  //    return null;
  //  }

}
