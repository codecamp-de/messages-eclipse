package de.codecamp.messages.eclipse.ide.actions;


import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.ui.MessageKeysRemovedWindow;
import de.codecamp.messages.shared.bundle.BundleFile;
import de.codecamp.messages.shared.conf.ProjectConf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;


public class OpenKeysRemovedDialogAction
{

  private final MessageKeyService messageKeyService;


  public OpenKeysRemovedDialogAction(MessageKeyService messageKeyService)
  {
    this.messageKeyService = messageKeyService;
  }


  public void execute(IProject project, Map<String, String> removedKeys,
      Map<String, String> addedKeys, EclipseMessageBundleManager bundleManager)
  {
    IdeProjectConf projectConf = messageKeyService.getProjectConf(project, true).orElse(null);

    AtomicReference<Map<String, Object>> keyMapping = new AtomicReference<>();
    Display.getDefault().syncExec(() ->
    {
      Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
      MessageKeysRemovedWindow d = new MessageKeysRemovedWindow(shell, removedKeys, addedKeys);

      if (d.open() == Window.OK)
      {
        keyMapping.set(d.getChosenKeyMapping());
      }
    });

    if (keyMapping.get() != null)
    {
      // old key -> new key
      Map<String, String> keysToRename = new HashMap<>();
      Set<String> keysToDelete = new HashSet<>();

      for (Entry<String, Object> keyEntry : keyMapping.get().entrySet())
      {
        String key = keyEntry.getKey();
        Object value = keyEntry.getValue();

        if (value instanceof MessageKeysRemovedWindow.Action)
        {
          switch ((MessageKeysRemovedWindow.Action) value)
          {
            case REMOVE_MESSAGE ->
            {
              keysToDelete.add(key);
            }

            case KEEP_MESSAGE ->
            {
              // nothing to do
            }

            default ->
            {
              throw new RuntimeException("unknown action: " + value);
            }
          }
        }
        else if (value != null)
        {
          String newKey = (String) value;
          keysToRename.put(key, newKey);
        }
      }

      boolean localBundleManager = false;
      if (bundleManager == null)
      {
        bundleManager = projectConf.createEclipseMessageBundleManager();
        localBundleManager = true;
      }

      updateBundleFiles(projectConf, bundleManager, keysToRename, keysToDelete);

      if (localBundleManager)
      {
        bundleManager.save();
      }
    }
  }

  private void updateBundleFiles(ProjectConf projectConf, EclipseMessageBundleManager bundleManager,
      Map<String, String> keysToRename, Set<String> keysToDelete)
  {
    if (keysToRename != null)
    {
      for (String oldKey : keysToRename.keySet())
      {
        String newKey = keysToRename.get(oldKey);

        String targetBundleName = projectConf.toTargetBundleName(oldKey).orElse(null);
        if (targetBundleName == null)
          continue;

        List<Locale> locales = new ArrayList<>(projectConf.getTargetLocales());
        locales.add(Locale.ROOT);
        for (Locale locale : locales)
        {
          String message = null;
          String comment = null;

          BundleFile<IFile, CoreException> bundleFile =
              bundleManager.getBundleFile(targetBundleName, locale);
          if (bundleFile != null)
          {
            comment = bundleFile.getComment(oldKey);
            message = bundleFile.removeMessage(oldKey);
          }

          if (message == null)
          {
            for (String bundleName : bundleManager.getBundleNames())
            {
              if (bundleName.equals(targetBundleName))
                continue;

              bundleFile = bundleManager.getBundleFile(bundleName, locale);
              if (bundleFile != null)
              {
                comment = bundleFile.getComment(oldKey);
                message = bundleFile.removeMessage(oldKey);
              }
            }
          }

          if (message != null)
          {
            BundleFile<IFile, CoreException> targetBundleFile =
                bundleManager.getBundleFile(targetBundleName, locale, true);
            targetBundleFile.setMessage(newKey, message);
            targetBundleFile.setComment(newKey, comment);
          }
        }
      }
    }

    if (keysToDelete != null)
    {
      for (String key : keysToDelete)
      {
        bundleManager.removeMessage(key);
      }
    }
  }

}
