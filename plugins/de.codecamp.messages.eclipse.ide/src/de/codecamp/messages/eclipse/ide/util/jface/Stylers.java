package de.codecamp.messages.eclipse.ide.util.jface;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.TextStyle;


public final class Stylers
{

  private static final Styler STRIKEOUT = new Styler()
  {

    @Override
    public void applyStyles(TextStyle textStyle)
    {
      textStyle.strikeout = true;
    }

  };

  private static final Map<Integer, Styler> UNDERLINE_STYLERS = new HashMap<>();
  static
  {
    for (int style : Arrays.asList(SWT.UNDERLINE_SINGLE, SWT.UNDERLINE_DOUBLE,
        SWT.UNDERLINE_SQUIGGLE, SWT.UNDERLINE_ERROR, SWT.UNDERLINE_LINK))
    {
      UNDERLINE_STYLERS.put(style, new Styler()
      {

        @Override
        public void applyStyles(TextStyle textStyle)
        {
          textStyle.underline = true;
          textStyle.underlineStyle = style;
        }

      });
    }
  }

  private static final Styler DEFAULT_UNDERLINE = underline(SWT.UNDERLINE_SINGLE);


  private Stylers()
  {
    // utility class
  }


  public static Styler strikeout()
  {
    return STRIKEOUT;
  }

  public static Styler underline()
  {
    return DEFAULT_UNDERLINE;
  }

  public static Styler underline(int style)
  {
    Styler styler = UNDERLINE_STYLERS.get(style);
    if (styler == null)
      throw new IllegalArgumentException("Unknown underline style: " + style);

    return styler;
  }


  public static Styler sequence(Styler... stylers)
  {
    return sequence(Arrays.asList(stylers));
  }

  public static Styler sequence(List<Styler> stylers)
  {
    return new Styler()
    {

      @Override
      public void applyStyles(TextStyle textStyle)
      {
        for (Styler styler : stylers)
        {
          styler.applyStyles(textStyle);
        }
      }

    };
  }

}
