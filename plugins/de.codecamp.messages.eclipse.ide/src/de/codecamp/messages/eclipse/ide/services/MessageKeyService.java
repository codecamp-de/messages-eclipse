package de.codecamp.messages.eclipse.ide.services;


import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.shared.conf.ProjectConfException;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;


/**
 * The {@link MessageKeyService} collects information about message keys and (the availability of)
 * messages in projects.
 */
public interface MessageKeyService
{

  void addMessageKeyChangeListener(MessageKeyListener listener);

  void removeMessageKeyChangeListener(MessageKeyListener listener);


  Optional<IdeProjectConf> getProjectConf(IProject project);

  Optional<IdeProjectConf> getProjectConf(IProject project, boolean resolveImports);

  Optional<IdeProjectConf> getProjectConfChecked(IProject project)
    throws ProjectConfException,
      CoreException;

  /**
   * Returns the available modules from all workbench projects.
   *
   * @return the available modules from all workbench projects
   */
  Stream<IdeModule> getModulesInWorkspace();

  Optional<IdeModule> getModule(IProject project);

  Stream<IdeModule> getImportedModules(IdeModule module);


  Stream<IdeSourceType> getSourceTypes(IProject project, boolean includeLocal,
      boolean includeImports, String filterText, boolean onlyWithProblems);

  int countSourceTypes(IProject project, boolean includeLocal, boolean includeImports,
      String filterText, boolean onlyWithProblems);

  Stream<IdeSourceType> getSourceTypes(IdeModule module, boolean includeImportedModules,
      String filterText, boolean onlyWithProblems);

  int countSourceTypes(IdeModule module, boolean includeImportedModules, String filterText,
      boolean onlyWithProblems);


  Stream<IdeMessageKey> getMessageKeys(IProject project, boolean includeLocal,
      boolean includeImports, String filterText, boolean onlyWithProblems);

  int countMessageKeys(IProject project, boolean includeLocal, boolean includeImports,
      String filterText, boolean onlyWithProblems);

  Stream<IdeMessageKey> getMessageKeys(IdeModule module, boolean includeImportedModules,
      String filterText, boolean onlyWithProblems);

  int countMessageKeys(IdeModule module, boolean includeImportedModules, String filterText,
      boolean onlyWithProblems);

  Stream<IdeMessageKey> getMessageKeys(IdeSourceType sourceTypeName, boolean includeLocal,
      boolean includeImports, String filterText, boolean onlyWithProblems);

  int countMessageKeys(IdeSourceType sourceType, boolean includeLocal, boolean includeImports,
      String filterText, boolean onlyWithProblems);



  Optional<IdeSourceType> findSourceType(IProject project, String sourceType, boolean includeLocal,
      boolean includeImports);

  Optional<IdeMessageKey> findMessageKey(IProject project, String messageKeyCode,
      boolean includeLocal, boolean includeImports);

  Stream<IdeMessageKey> findMessageKeys(IProject project, Set<String> messageKeyCodes,
      boolean includeLocal, boolean includeImports);

  Optional<IdeMessageKey> findMessageKey(String messageKeyCode, boolean includeLocal,
      boolean includeImports);


  MessageAvailability getMessageAvailability(IProject project, String bundleName, Locale locale,
      String code);


  Optional<Set<Locale>> getRequiredLocales(IdeMessageKey messageKey);

  /**
   * Finds the available target locales for the given message key. Optionally only including the
   * imported modules or also the current module (of the provided project).
   *
   * @param messageKey
   *          the message key
   * @param importsOnly
   *          whether to only consider imported modules; otherwise the current module is also
   *          considered
   * @return the available target locales for the given message key
   */
  Set<Locale> getTargetLocales(IdeMessageKey messageKey, boolean importsOnly);


  IMarker findMostSevereBundleValidationMarker(IProject project);

  IMarker findMostSevereBundleValidationMarker(IdeSourceType sourceType);

  IMarker findMostSevereBundleValidationMarker(IdeMessageKey messageKey, Locale locale);

  Stream<IMarker> findBundleValidationMarkers(IdeMessageKey messageKey, Locale locale);


  void cleanProject(IProject project);

  void refreshProject(IProject project);


  class MessageAvailability
  {

    private final boolean bundleFileAvailable;

    private final boolean messageKeyAvailable;

    private final boolean messageAvailable;


    public MessageAvailability(boolean bundleFileAvailable, boolean messageKeyAvailable,
        boolean messageAvailable)
    {
      this.bundleFileAvailable = bundleFileAvailable;
      this.messageKeyAvailable = messageKeyAvailable;
      this.messageAvailable = messageAvailable;
    }

    public boolean isBundleFileAvailable()
    {
      return bundleFileAvailable;
    }

    public boolean isMessageKeyAvailable()
    {
      return messageKeyAvailable;
    }

    public boolean isMessageAvailable()
    {
      return messageAvailable;
    }

  }

}
