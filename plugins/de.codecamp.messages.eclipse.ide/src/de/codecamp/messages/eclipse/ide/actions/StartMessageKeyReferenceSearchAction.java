package de.codecamp.messages.eclipse.ide.actions;


import de.codecamp.messages.codegen.MessageCodegenUtils;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import de.codecamp.messages.eclipse.ide.util.jdt.JdtUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;


public class StartMessageKeyReferenceSearchAction
{

  private final MessageKeyService messageKeyService;


  public StartMessageKeyReferenceSearchAction(MessageKeyService messageKeyService)
  {
    this.messageKeyService = messageKeyService;
  }


  public void execute(String messageKey, IProgressMonitor monitor)
    throws CoreException
  {
    Optional<IdeMessageKey> key = messageKeyService.findMessageKey(messageKey, true, true);
    if (!key.isPresent())
    {
      StatusUtils.logWarning(getClass(),
          "Failed to search for message key references. Message key ''{0}'' not found.",
          messageKey);
      return;
    }

    execute(key.get(), monitor);
  }

  public void execute(IdeMessageKey messageKey, IProgressMonitor monitor)
    throws CoreException
  {
    try
    {
      IJavaProject javaProject = JavaCore.create(messageKey.getProject());
      if (javaProject == null)
        return;

      IType type = javaProject.findType(messageKey.getSourceType());
      if (type == null)
        return;

      String packagePart = type.getPackageFragment().getElementName();
      String classPart = type.getTypeQualifiedName('.');

      String constantsSimpleTypeName =
          MessageCodegenUtils.getMessageConstantsSimpleTypeNameFor(classPart);

      List<IJavaElement> elements = new ArrayList<>();
      IType constantsType = javaProject.findType(packagePart + "." + constantsSimpleTypeName);
      if (constantsType != null)
      {
        IField constantsField = constantsType.getField(messageKey.getLocalPart());
        elements.add(constantsField);

        for (IMethod method : constantsType.getMethods())
        {
          if (method.getElementName().equals(messageKey.getLocalPart()))
          {
            elements.add(method);
            break;
          }
        }
      }

      String proxySimpleTypeName =
          MessageCodegenUtils.getMessageProxiesSimpleTypeNameFor(classPart);
      IType proxyType = javaProject.findType(packagePart + "." + proxySimpleTypeName);
      if (proxyType != null)
      {
        /*
         * Message proxies don't require method overloading so just use the first method with a
         * matching name.
         */
        for (IMethod method : proxyType.getMethods())
        {
          if (method.getElementName().equals(messageKey.getLocalPart()))
          {
            elements.add(method);
            break;
          }
        }
      }

      JdtUtils.searchForReferences(elements);
    }
    catch (JavaModelException | RuntimeException ex)
    {
      throw new CoreException(StatusUtils.createError(getClass(),
          "Failed to find references of message key ''{0}''.", messageKey, ex));
    }
  }

}
