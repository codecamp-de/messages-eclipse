package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import javax.inject.Inject;
import javax.inject.Named;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;


public abstract class AbstractMessageBundlesHandler
{

  @Inject
  protected MessageKeyService messageKeyService;


  @Evaluate
  public boolean isVisible(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    return canExecute(selection);
  }

  @CanExecute
  public boolean canExecute(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    if (selection == null || selection.isEmpty())
      return false;

    for (Object element : selection.toList())
    {
      IProject project = HandlerUtils.getProject(element).orElse(null);
      if (project != null)
      {
        if (!messageKeyService.getProjectConf(project, true).isPresent())
          return false;
      }

      else if (element instanceof IResource)
      {
        IResource resource = (IResource) element;

        IdeProjectConf projectConf =
            messageKeyService.getProjectConf(resource.getProject(), true).orElse(null);
        if (projectConf == null || !projectConf.getBundleDirAsFolder().isPresent())
          return false;

        if (!projectConf.isMessageBundleDir(resource) && !projectConf.isMessageBundleFile(resource))
          return false;
      }

      else
      {
        return false;
      }
    }

    return true;
  }

  protected IdeProjectConf getProjectConfFromSelectionElement(Object element)
  {
    IProject project = HandlerUtils.getProject(element).orElse(null);
    if (project == null)
      return null;

    return messageKeyService.getProjectConf(project, true).orElse(null);
  }

}
