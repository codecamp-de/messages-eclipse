package de.codecamp.messages.eclipse.ide.ui;


import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.E4Utils;
import javax.inject.Inject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.navigator.ILinkHelper;


public class LinkHelper
  implements
    ILinkHelper
{

  @Inject
  private MessageKeyService messageKeyService;


  public LinkHelper()
  {
    E4Utils.injectOsgiContext(this);
  }


  @Override
  public IStructuredSelection findSelection(IEditorInput anInput)
  {
    if (anInput instanceof IProjectEditorInput)
    {
      return new StructuredSelection(((IProjectEditorInput) anInput).getLinkedFile());
    }
    return null;
  }

  @Override
  public void activateEditor(IWorkbenchPage aPage, IStructuredSelection aSelection)
  {
    if (aSelection.size() != 1)
      return;

    Object selection = aSelection.getFirstElement();
    if (!(selection instanceof IResource))
      return;

    IResource resource = (IResource) selection;
    messageKeyService.getProjectConf(resource.getProject()).ifPresent(pc ->
    {
      if (pc.isMessageBundleDir(resource) || pc.isMessageBundleFile(resource))
      {
        IEditorPart editorPart = aPage.findEditor(new ProjectEditorInput(resource.getProject()));
        if (editorPart != null)
          aPage.activate(editorPart);
      }
    });
  }

}
