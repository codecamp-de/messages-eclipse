package de.codecamp.messages.eclipse.ide.util.jdt;


import java.beans.Introspector;
import java.util.Optional;
import java.util.function.Predicate;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.IAnnotationBinding;
import org.eclipse.jdt.core.dom.IMemberValuePairBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.Modifier;


public final class JdtDomUtils
{

  private static final String GET_PREFIX = "get";

  private static final String IS_PREFIX = "is";


  private JdtDomUtils()
  {
    // utility class
  }


  public static Optional<ASTNode> findParent(ASTNode node, Predicate<ASTNode> test)
  {
    while (node != null)
    {
      if (test.test(node))
        return Optional.of(node);
      node = node.getParent();
    }
    return Optional.empty();
  }

  public static <T extends ASTNode> Optional<T> findParent(ASTNode node, Class<T> type)
  {
    return findParent(node, n -> type.isInstance(n)).map(type::cast);
  }

  public static <T> T getAnnotationElementValue(IAnnotationBinding anntationBinding,
      String elementName, Class<T> valueType)
  {
    return getAnnotationElementValue(anntationBinding, elementName, valueType, null);
  }

  public static <T> T getAnnotationElementValue(IAnnotationBinding anntationBinding,
      String elementName, Class<T> valueType, T defaultValue)
  {
    if (anntationBinding == null)
      return defaultValue;
    for (IMemberValuePairBinding pair : anntationBinding.getAllMemberValuePairs())
    {
      if (pair.getName().equals(elementName))
      {
        return valueType.cast(pair.getValue());
      }
    }
    return defaultValue;
  }


  public static boolean isBeanPropertyGetter(IMethodBinding methodBinding)
  {
    if (methodBinding.getParameterTypes().length > 0)
      return false;

    if ((methodBinding.getModifiers() & Modifier.PUBLIC) == 0)
      return false;
    if ((methodBinding.getModifiers() & Modifier.STATIC) != 0)
      return false;

    if (void.class.getName().equals(methodBinding.getReturnType().getQualifiedName()))
      return false;

    String methodName = methodBinding.getName();
    return (methodName.startsWith(GET_PREFIX) || methodName.startsWith(IS_PREFIX));
  }

  public static String getBeanPropertyName(IMethodBinding methodBinding)
  {
    if (!isBeanPropertyGetter(methodBinding))
      throw new IllegalArgumentException("not a bean getter");

    String methodName = methodBinding.getName();

    String propertyName;
    if (methodName.startsWith(GET_PREFIX))
      propertyName = methodName.substring(GET_PREFIX.length());
    else
      propertyName = methodName.substring(IS_PREFIX.length());

    propertyName = Introspector.decapitalize(propertyName);

    return propertyName;
  }

}
