package de.codecamp.messages.eclipse.ide.validation;


import de.codecamp.messages.shared.bundle.BundleFile;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;


public interface BundleFileValidationTask
{

  void validate(ValidationResultAdapter result, BundleFileValidationContext context,
      BundleFile<IFile, CoreException> bundleFile, BundleFile<IFile, CoreException> rootBundleFile);


  interface ValidationResultAdapter
  {

    void addBundleWarning(BundleFile<IFile, CoreException> bundleFile, String messageKey,
        String sourceType, String msg);

    void addBundleError(BundleFile<IFile, CoreException> bundleFile, String messageKey,
        String sourceType, String msg);

  }

}
