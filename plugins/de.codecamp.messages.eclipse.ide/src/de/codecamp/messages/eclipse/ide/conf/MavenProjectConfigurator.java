package de.codecamp.messages.eclipse.ide.conf;


import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.E4Utils;
import de.codecamp.messages.eclipse.ide.util.eclipse.ProjectUtils;
import de.codecamp.messages.shared.conf.Mode;
import de.codecamp.messages.shared.conf.ProjectConf;
import java.util.Map;
import javax.inject.Inject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.apt.core.util.AptConfig;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.m2e.core.project.configurator.AbstractProjectConfigurator;
import org.eclipse.m2e.core.project.configurator.ProjectConfigurationRequest;


/**
 * Uses m2e to override the {@link ProjectConf#CONF_MODE mode} with {@link Mode#DEV_ECLIPSE}. That
 * way it doesn't have to be declared in a Maven profile. This configurator runs each time after
 * m2e-apt has imported annotation processor options from the POM.
 */
public class MavenProjectConfigurator
  extends
    AbstractProjectConfigurator
{

  /*
   * org.eclipse.wst.validation.internal.plugin.ValidationPlugin.VALIDATION_BUILDER_ID
   */
  private static final String VALIDATION_BUILDER_ID =
      "org.eclipse.wst.validation.validationbuilder";


  @Inject
  private MessageKeyService messageKeyService;


  public MavenProjectConfigurator()
  {
    E4Utils.injectOsgiContext(this);
  }


  @Override
  public void configure(ProjectConfigurationRequest request, IProgressMonitor monitor)
    throws CoreException
  {
    ProjectConf projectConf =
        messageKeyService.getProjectConf(request.mavenProjectFacade().getProject()).orElse(null);
    if (projectConf == null)
      return;

    IJavaProject javaProject = JavaCore.create(request.mavenProjectFacade().getProject());

    Map<String, String> processorOptions = AptConfig.getProcessorOptions(javaProject, false);

    String mode = processorOptions.get(ProjectConf.CONF_MODE);
    if (!Mode.KEYS_ONLY.name().equals(mode))
    {
      AptConfig.addProcessorOption(javaProject, ProjectConf.CONF_MODE, Mode.DEV_ECLIPSE.name());

      overrideIfSet(javaProject, processorOptions, ProjectConf.CONF_MISSING_MESSAGE_POLICY,
          Mode.DEV_ECLIPSE.missingMessagePolicy().name());
      overrideIfSet(javaProject, processorOptions, ProjectConf.CONF_MESSAGE_ARG_POLICY,
          Mode.DEV_ECLIPSE.messageArgPolicy().name());
      overrideIfSet(javaProject, processorOptions, ProjectConf.CONF_UNDECLARED_KEY_POLICY,
          Mode.DEV_ECLIPSE.undeclaredKeyPolicy().name());
      overrideIfSet(javaProject, processorOptions, ProjectConf.CONF_BUNDLE_MISMATCH_POLICY,
          Mode.DEV_ECLIPSE.bundleMismatchPolicy().name());
    }


    ProjectUtils.addBuilder(request.mavenProjectFacade().getProject(), VALIDATION_BUILDER_ID,
        monitor);
  }

  private void overrideIfSet(IJavaProject javaProject, Map<String, String> processorOptions,
      String confKey, String confValue)
  {
    if (processorOptions.get(confKey) != null)
    {
      AptConfig.addProcessorOption(javaProject, confKey, confValue);
    }
  }

}
