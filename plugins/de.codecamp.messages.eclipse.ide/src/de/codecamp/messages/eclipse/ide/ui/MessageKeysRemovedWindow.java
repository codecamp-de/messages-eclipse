package de.codecamp.messages.eclipse.ide.ui;


import de.codecamp.messages.eclipse.ide.MessagesPlugin;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ComboBoxViewerCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;


public class MessageKeysRemovedWindow
  extends
    TitleAreaDialog
{

  private final Map<String, String> removedKeys;

  private final Map<String, String> addedKeys;

  private final String sharedPrefix;

  private final Map<String, Object> keyMapping = new HashMap<>();

  private final Map<String, Object> initialKeyMapping = new HashMap<>();


  public MessageKeysRemovedWindow(Shell parentShell, Map<String, String> removedKeys,
      Map<String, String> addedKeys)
  {
    super(parentShell);

    setShellStyle(getShellStyle() | SWT.RESIZE);

    if (removedKeys == null)
      this.removedKeys = Collections.emptyMap();
    else
      this.removedKeys = new TreeMap<>(removedKeys);

    if (addedKeys == null)
      this.addedKeys = Collections.emptyMap();
    else
      this.addedKeys = new TreeMap<>(addedKeys);

    Map<String, String> allKeys = new HashMap<>(removedKeys);
    this.addedKeys.forEach((code, sourceType) ->
    {
      allKeys.merge(code, sourceType, (os, s) -> os == null ? s : os);
    });

    String shortestKey = allKeys.keySet().stream()
        .reduce((str1, str2) -> str1.length() >= str2.length() ? str1 : str2).get();
    int dotIndex = -1;

    String prefix = null;
    while (true)
    {
      // Don't cut off type names. Assume that upper case letters indicate start of a simple type
      // name.
      if (dotIndex + 1 < shortestKey.length()
          && Character.isUpperCase(shortestKey.charAt(dotIndex + 1)))
      {
        break;
      }

      dotIndex = StringUtils.indexOf(shortestKey, ".", dotIndex + 1);

      if (dotIndex > -1)
      {
        String testPrefix = shortestKey.substring(0, dotIndex + 1);

        if (allKeys.keySet().stream().allMatch(key -> key.startsWith(testPrefix)))
        {
          prefix = testPrefix;
          continue;
        }
      }

      break;
    }

    sharedPrefix = prefix;

    createInitialKeyMapping();
  }


  @Override
  protected Control createDialogArea(Composite parent)
  {
    setTitle("Removed Message Keys");
    setHelpAvailable(false);


    Composite container = (Composite) super.createDialogArea(parent);

    Composite area = new Composite(container, SWT.NONE);
    GridDataFactory.fillDefaults().grab(true, true).applyTo(area);

    Label instruction = new Label(area, SWT.WRAP);
    GridDataFactory.fillDefaults().grab(true, false).hint(300, SWT.DEFAULT).applyTo(instruction);

    String msg = "Some message keys have been removed and/or renamed. For each removed message key"
        + " you can determine whether their messages should be removed,"
        + " kept as is or moved to a new key.";
    instruction.setText(msg);


    Composite buttonBar = new Composite(area, SWT.NONE);
    GridDataFactory.fillDefaults().grab(false, false).align(SWT.RIGHT, SWT.CENTER)
        .applyTo(buttonBar);
    GridLayoutFactory.swtDefaults().numColumns(3).applyTo(buttonBar);

    Button keepSelectedButton = new Button(buttonBar, SWT.NONE);
    keepSelectedButton.setText("Keep Selected");
    keepSelectedButton.setImage(MessagesPlugin.getDefault().getImage("/icons/message.png"));
    Button removeSelectedButton = new Button(buttonBar, SWT.NONE);
    removeSelectedButton.setText("Remove Selected");
    removeSelectedButton
        .setImage(MessagesPlugin.getDefault().getImage("/icons/message_remove.png"));
    Button resetSelectedButton = new Button(buttonBar, SWT.NONE);
    resetSelectedButton.setText("Reset Selected");


    Composite keysViewerContainer = new Composite(area, SWT.NONE);
    GridDataFactory.fillDefaults().grab(true, true).hint(800, -1).applyTo(keysViewerContainer);
    TableColumnLayout tableColumnLayout = new TableColumnLayout();
    keysViewerContainer.setLayout(tableColumnLayout);

    TableViewer keysViewer =
        new TableViewer(keysViewerContainer, SWT.FULL_SELECTION | SWT.BORDER | SWT.MULTI);
    ColumnViewerToolTipSupport.enableFor(keysViewer);

    keysViewer.getTable().setHeaderVisible(true);
    keysViewer.getTable().setLinesVisible(true);

    TableViewerColumn oldKey = new TableViewerColumn(keysViewer, SWT.NONE);
    oldKey.getColumn().setText("Removed Key");
    oldKey.setLabelProvider(new ColumnLabelProvider()
    {

      @Override
      public String getText(Object element)
      {
        String messageKey = (String) element;
        return messageKeyToLabelText(messageKey);
      }

      @Override
      public String getToolTipText(Object element)
      {
        String messageKey = (String) element;
        return messageKey;
      }

      @Override
      public boolean useNativeToolTip(Object object)
      {
        return true;
      }

    });

    TableViewerColumn newKey = new TableViewerColumn(keysViewer, SWT.NONE);
    newKey.getColumn().setText("Action / New Key");
    newKey.setLabelProvider(new ColumnLabelProvider()
    {

      @Override
      public String getText(Object element)
      {
        String oldKey = (String) element;
        Object newKeyOrAction = keyMapping.get(oldKey);

        if (newKeyOrAction instanceof Action)
        {
          return newKeyOrAction.toString().replace("_", " ");
        }
        else
        {
          String newKey = (String) newKeyOrAction;
          return "MOVE TO: " + messageKeyToLabelText(newKey);
        }
      }

      @Override
      public Image getImage(Object element)
      {
        String oldKey = (String) element;
        Object newKeyOrAction = keyMapping.get(oldKey);

        Image image;
        if (newKeyOrAction instanceof Action)
        {
          image = switch ((Action) newKeyOrAction)
          {
            case REMOVE_MESSAGE -> MessagesPlugin.getDefault()
                .getImage("/icons/message_remove.png");
            case KEEP_MESSAGE -> MessagesPlugin.getDefault().getImage("/icons/message.png");
          };
        }
        else
        {
          image = MessagesPlugin.getDefault().getImage("/icons/message_move.png");
        }
        return image;
      }

    });
    newKey.setEditingSupport(new EditingSupport(keysViewer)
    {

      private ComboBoxViewerCellEditor editor;


      @Override
      protected CellEditor getCellEditor(Object element)
      {
        if (editor == null)
        {
          editor = new ComboBoxViewerCellEditor((Composite) getViewer().getControl(), SWT.READ_ONLY)
          {

            @Override
            protected Control createControl(Composite parent)
            {
              CCombo combo = (CCombo) super.createControl(parent);
              combo.addSelectionListener(new SelectionAdapter()
              {

                @Override
                public void widgetSelected(SelectionEvent e)
                {
                  deactivate();
                }

              });
              return combo;
            }

          };
          editor.setActivationStyle(ComboBoxViewerCellEditor.DROP_DOWN_ON_MOUSE_ACTIVATION);
          editor.setLabelProvider(new LabelProvider()
          {

            @Override
            public String getText(Object element)
            {
              if (element instanceof Action)
              {
                return ((Action) element).name().replace("_", " ");
              }
              else
              {
                String messageKey = (String) element;
                return messageKey;
              }
            }

          });
          editor.setContentProvider(ArrayContentProvider.getInstance());

          List<Object> input = new ArrayList<>();

          input.addAll(Arrays.asList(Action.values()));
          input.addAll(addedKeys.keySet());

          editor.setInput(input);
        }
        return editor;
      }

      @Override
      protected boolean canEdit(Object element)
      {
        return true;
      }

      @Override
      protected Object getValue(Object element)
      {
        Object mappingTarget = keyMapping.get(element);
        if (mappingTarget == null)
          mappingTarget = Action.REMOVE_MESSAGE;
        return mappingTarget;
      }

      @Override
      protected void setValue(Object element, Object value)
      {
        String key = (String) element;
        if (value == null)
          keyMapping.remove(key);
        else
          keyMapping.put(key, value);

        keysViewer.refresh(element);
      }

    });


    keepSelectedButton.addSelectionListener(new SelectionAdapter()
    {

      @Override
      public void widgetSelected(SelectionEvent e)
      {
        for (Object element : keysViewer.getStructuredSelection().toList())
        {
          String key = (String) element;
          keyMapping.put(key, Action.KEEP_MESSAGE);
          keysViewer.refresh(key);
        }
      }

    });
    removeSelectedButton.addSelectionListener(new SelectionAdapter()
    {

      @Override
      public void widgetSelected(SelectionEvent e)
      {
        for (Object element : keysViewer.getStructuredSelection().toList())
        {
          String key = (String) element;
          keyMapping.put(key, Action.REMOVE_MESSAGE);
          keysViewer.refresh(key);
        }
      }

    });
    resetSelectedButton.addSelectionListener(new SelectionAdapter()
    {

      @Override
      public void widgetSelected(SelectionEvent e)
      {
        for (Object element : keysViewer.getStructuredSelection().toList())
        {
          String key = (String) element;
          keyMapping.put(key, initialKeyMapping.get(key));
          keysViewer.refresh(key);
        }
      }

    });


    keysViewer.setContentProvider(ArrayContentProvider.getInstance());
    keysViewer.setInput(removedKeys.keySet());


    tableColumnLayout.setColumnData(oldKey.getColumn(), new ColumnWeightData(1));
    tableColumnLayout.setColumnData(newKey.getColumn(), new ColumnWeightData(1));

    GridLayoutFactory.fillDefaults().margins(15, 10).generateLayout(area);

    return area;
  }

  @Override
  protected Button createButton(Composite parent, int id, String label, boolean defaultButton)
  {
    Button button = super.createButton(parent, id, label, defaultButton);
    if (id == IDialogConstants.OK_ID)
    {
      button.setText("Apply");
    }
    else if (id == IDialogConstants.CANCEL_ID)
    {
      button.setText("Keep All");
    }
    return button;
  }


  private String messageKeyToLabelText(String messageKey)
  {
    String label = messageKey;
    label = StringUtils.removeStart(label, sharedPrefix);

    if (!messageKey.equals(label))
    {
      label = "..." + label;
    }

    return label;
  }


  @Override
  protected void okPressed()
  {
    super.okPressed();
  }

  @Override
  protected void cancelPressed()
  {
    super.cancelPressed();
  }


  private void createInitialKeyMapping()
  {
    for (Entry<String, String> oldKey : removedKeys.entrySet())
    {
      String bestMatch = null;
      float bestMatchSimilarity = 0;

      for (Entry<String, String> newKey : addedKeys.entrySet())
      {
        float similarity =
            getSimilarity(oldKey.getKey(), oldKey.getValue(), newKey.getKey(), newKey.getValue());

        if (bestMatch == null || similarity > bestMatchSimilarity)
        {
          bestMatch = newKey.getKey();
          bestMatchSimilarity = similarity;
        }
      }

      if (bestMatchSimilarity > 0)
      {
        initialKeyMapping.put(oldKey.getKey(), bestMatch);
      }
      else
      {
        initialKeyMapping.put(oldKey.getKey(), Action.REMOVE_MESSAGE);
      }
    }
    keyMapping.putAll(initialKeyMapping);
  }

  private static float getSimilarity(String code1, String sourceType1, String code2,
      String sourceType2)
  {
    String localPart1 = StringUtils.removeStart(code1, sourceType1 + ".");
    String localPart2 = StringUtils.removeStart(code2, sourceType2 + ".");

    /*
     * For typical refactorings only the source type or the local part changes. Don't try to match
     * where both change at the same time.
     */
    if (sourceType1 != null && sourceType2 != null && !sourceType1.equals(sourceType2)
        && !localPart1.equals(localPart2))
    {
      return 0;
    }

    float similarity = 0;

    if (sourceType1 != null && sourceType2 != null)
    {
      similarity += getSimilarity(sourceType1, sourceType2) * 0.5;
      similarity += getSimilarity(localPart1, localPart2) * 0.5;
    }
    else
    {
      similarity += getSimilarity(code1, code2);
    }

    return similarity;
  }

  private static float getSimilarity(String s1, String s2)
  {
    return 1
        - (StringUtils.getLevenshteinDistance(s1, s2) / (float) Math.max(s1.length(), s2.length()));
  }

  public Map<String, Object> getChosenKeyMapping()
  {
    return keyMapping;
  }


  public enum Action
  {

    KEEP_MESSAGE,
    REMOVE_MESSAGE

  }

}
