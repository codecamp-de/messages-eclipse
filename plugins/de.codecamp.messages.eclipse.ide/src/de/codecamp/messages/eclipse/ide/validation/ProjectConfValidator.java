package de.codecamp.messages.eclipse.ide.validation;


import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.IdeModule;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.AbstractResourceValidator;
import de.codecamp.messages.eclipse.ide.util.eclipse.E4Utils;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import de.codecamp.messages.shared.conf.ProjectConf;
import de.codecamp.messages.shared.conf.ProjectConfException;
import de.codecamp.messages.shared.messageformat.MessageFormatSupport;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.wst.validation.ValidationResult;
import org.eclipse.wst.validation.ValidationState;


/**
 * Checks the messages project configuration if available.
 */
public class ProjectConfValidator
  extends
    AbstractResourceValidator
{

  public static final String MARKER_ID_PROJECT_CONF_PROBLEM =
      "de.codecamp.messages.eclipse.ide.problem.projectconf";


  @Inject
  private MessageKeyService messageKeyService;


  public ProjectConfValidator()
  {
    E4Utils.injectOsgiContext(this);
  }


  @Override
  protected String getMarkerId()
  {
    return MARKER_ID_PROJECT_CONF_PROBLEM;
  }

  @Override
  public ValidationResult validate(IResource resource, int kind, ValidationState state,
      IProgressMonitor monitor)
  {
    ValidationResult result = new ValidationResult();

    if (resource instanceof IFile
        && resource.getProjectRelativePath().toString().equals(ProjectConf.PROJECT_CONF_FILE_NAME))
    {
      IProject project = resource.getProject();

      IdeProjectConf projectConf;
      try
      {
        projectConf = messageKeyService.getProjectConfChecked(project).orElse(null);
      }
      catch (ProjectConfException ex)
      {
        addErrorMessage(result, project, ex.getMessage());
        return result;
      }
      catch (CoreException ex)
      {
        String msg = "An error occurred while checking Messanthropist project configuration.";
        addErrorMessage(result, project, msg, ex);
        StatusUtils.logError(getClass(), msg, ex);
        return result;
      }

      if (projectConf == null)
        return result;


      if (StringUtils.isNotBlank(projectConf.getBundleDir())
          && !projectConf.getBundleDirAsFolder().isPresent())
      {
        String msg = "Bundle directory not found within the project or not configured correctly.";
        addErrorMessage(result, project, msg);
      }


      IdeModule rootModule = messageKeyService.getModule(project).orElse(null);
      if (rootModule == null)
        return result;

      Deque<List<IdeModule>> moduleStack = new ArrayDeque<>();

      moduleStack.push(new ArrayList<>(Arrays.asList(rootModule)));

      while (!moduleStack.isEmpty())
      {
        List<IdeModule> importChain = moduleStack.pop();

        IdeModule module = importChain.get(importChain.size() - 1);

        Map<String, IdeModule> importedModules = messageKeyService.getImportedModules(module)
            .collect(toMap(m -> m.getModuleName(), m -> m));

        for (String importedModuleName : module.getImportedModules())
        {
          IdeModule importedModule = importedModules.get(importedModuleName);
          if (importedModule == null)
          {
            String moduleChainText = importChain.stream().map(IdeModule::getModuleName)
                .collect(joining("' -> '", "'", "'"));

            String msg = "Imported message module {0} via {1} not found.";
            addErrorMessage(result, project, msg, "'" + importedModuleName + "'", moduleChainText);
          }
          else
          {
            List<IdeModule> newImportChain = new ArrayList<>(importChain);
            newImportChain.add(importedModule);
            moduleStack.push(newImportChain);

            MessageFormatSupport messageFormatSupport =
                MessageFormatSupport.getSupport(projectConf);
            if (!messageFormatSupport.supportsFormat(importedModule.getMessageFormat()))
            {
              String msg =
                  "Message format ''{0}'' used by imported message keys ''{1}'' not supported by message format ''{2}''.";
              addErrorMessage(result, project, msg, importedModule.getMessageFormat(),
                  importedModuleName, projectConf.getMessageFormat());
            }
          }
        }
      }
    }

    return result;
  }

}
