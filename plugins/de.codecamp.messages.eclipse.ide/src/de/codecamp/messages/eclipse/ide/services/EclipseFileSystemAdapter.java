package de.codecamp.messages.eclipse.ide.services;


import de.codecamp.messages.eclipse.ide.util.eclipse.ResourceUtils;
import de.codecamp.messages.shared.bundle.FileSystemAdapter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;


public class EclipseFileSystemAdapter
  implements
    FileSystemAdapter<IFolder, IFile, CoreException>
{

  private final IProject project;


  public EclipseFileSystemAdapter(IProject project)
  {
    this.project = project;
  }


  @Override
  public IFolder getDirectory(String path)
  {
    return ResourceUtils.getFolderInProjectByPath(path, project);
  }

  @Override
  public List<IFile> listFiles(IFolder dir, boolean recursive)
    throws CoreException
  {
    List<IFile> files = new ArrayList<>();

    if (!dir.isAccessible())
      return files;

    dir.accept(resource ->
    {
      if (resource.getType() == IResource.FILE)
      {
        files.add((IFile) resource);
        return false;
      }
      else
      {
        return true;
      }
    });

    return files;
  }

  @Override
  public IFile getFile(IFolder dir, String fileName)
  {
    return dir.getFile(fileName);
  }


  @Override
  public String getFileName(IFile file)
  {
    return file.getName();
  }

  @Override
  public String getRelativeFilePath(IFolder dir, IFile file)
  {
    return file.getProjectRelativePath().makeRelativeTo(dir.getProjectRelativePath()).toString();
  }

  @Override
  public String getDisplayPath(IFolder dir, IFile file)
  {
    return file.getProjectRelativePath().makeRelativeTo(dir.getProjectRelativePath()).toString();
  }

  @Override
  public boolean exists(IFile file)
  {
    if (!file.exists())
      return false;

    // check if the file actually exists in the file system
    URI locationUri = file.getLocationURI();
    if (locationUri == null)
      return false;

    Path filePath;
    try
    {
      filePath = Paths.get(locationUri);
    }
    catch (RuntimeException ex)
    {
      return false;
    }

    return Files.exists(filePath);
  }

  @Override
  public void createParentDirectories(IFile file)
    throws CoreException
  {
    IContainer parent = file.getParent();
    if (parent instanceof IFolder)
    {
      ResourceUtils.createFolders((IFolder) parent);
    }
  }

  @Override
  public void deleteIfExists(IFile file)
    throws CoreException
  {
    if (file.exists())
    {
      file.delete(true, null);
    }
  }

  @Override
  public InputStream newInputStream(IFile file)
    throws CoreException
  {
    return file.getContents(true);
  }

  @Override
  public OutputStream newOutputStream(IFile file)
    throws CoreException
  {
    return new ByteArrayOutputStream()
    {

      @Override
      public void close()
        throws IOException
      {
        try
        {
          ByteArrayInputStream bufferIn = new ByteArrayInputStream(buf, 0, count);
          if (file.exists())
            file.setContents(bufferIn, true, false, null);
          else
            file.create(bufferIn, true, null);

          if (!file.getCharset().equals("UTF-8"))
            file.setCharset("UTF-8", null);
        }
        catch (CoreException ex)
        {
          throw new IOException(ex);
        }
      }

    };
  }

}
