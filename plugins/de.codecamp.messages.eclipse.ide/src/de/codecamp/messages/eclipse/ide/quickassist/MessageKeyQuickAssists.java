package de.codecamp.messages.eclipse.ide.quickassist;


import de.codecamp.messages.eclipse.ide.MessagesPlugin;
import de.codecamp.messages.eclipse.ide.actions.OpenMessageKeyDeclarationAction;
import de.codecamp.messages.eclipse.ide.actions.StartMessageKeyReferenceSearchAction;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.l10n.BundleMessages;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.ui.MessageBundleEditor;
import de.codecamp.messages.eclipse.ide.ui.MessageKeysView;
import de.codecamp.messages.eclipse.ide.ui.ProjectEditorInput;
import de.codecamp.messages.eclipse.ide.util.MessageKeyAstUtils;
import de.codecamp.messages.eclipse.ide.util.eclipse.E4Utils;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import javax.inject.Inject;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.services.nls.Translation;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.jdt.ui.text.java.IInvocationContext;
import org.eclipse.jdt.ui.text.java.IJavaCompletionProposal;
import org.eclipse.jdt.ui.text.java.IProblemLocation;
import org.eclipse.jdt.ui.text.java.IQuickAssistProcessor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;


public class MessageKeyQuickAssists
  implements
    IQuickAssistProcessor
{

  private static final String FIND_OPEN_MESSAGE_KEY_DECLARATION_ICON =
      "platform:/plugin/org.eclipse.jdt.ui/icons/full/obj16/jcu_obj.png";

  private static final String FIND_MESSAGE_KEY_USES_ICON =
      "platform:/plugin/org.eclipse.jdt.ui/icons/full/obj16/jsearch_obj.png";

  private static final String OPEN_EDITOR_ICON =
      "platform:/plugin/de.codecamp.messages.eclipse.ide/icons/messages_edit.png";

  private static final String SHOW_IN_VIEW_ICON =
      "platform:/plugin/de.codecamp.messages.eclipse.ide/icons/language.png";


  @Inject
  private MessageKeyService messageKeyService;

  @Inject
  private EPartService partService;

  @Inject
  @Translation
  private BundleMessages msgs;


  public MessageKeyQuickAssists()
  {
    E4Utils.injectWorkbenchContext(this);
  }


  @Override
  public boolean hasAssists(IInvocationContext context)
    throws CoreException
  {
    return findMessageKeyFromContext(context) != null;
  }

  @Override
  public IJavaCompletionProposal[] getAssists(IInvocationContext context,
      IProblemLocation[] locations)
    throws CoreException
  {
    IdeMessageKey messageKey = findMessageKeyFromContext(context).orElse(null);
    if (messageKey == null)
      return null;

    IProject project = context.getCompilationUnit().getJavaProject().getProject();

    IdeProjectConf projectConf = messageKeyService.getProjectConf(project, true).orElse(null);
    if (projectConf == null)
      return null;

    String targetBundleName = projectConf.toTargetBundleName(messageKey.getCode()).orElse(null);
    if (targetBundleName == null)
      return null;


    List<Locale> locales = new ArrayList<>();
    locales.add(Locale.ROOT);
    locales.addAll(projectConf.getTargetLocales());

    List<IJavaCompletionProposal> proposals = new ArrayList<>(locales.size());

    // open key and message in editor
    IJavaCompletionProposal proposal = new IJavaCompletionProposal()
    {

      @Override
      public String getDisplayString()
      {
        return msgs.command_openMessageBundleEditor_name;
      }

      @Override
      public Image getImage()
      {
        return MessagesPlugin.getDefault().getImage(OPEN_EDITOR_ICON);
      }

      @Override
      public String getAdditionalProposalInfo()
      {
        return null;
      }

      @Override
      public void apply(IDocument document)
      {
        try
        {
          IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
          MessageBundleEditor bundleEditor =
              (MessageBundleEditor) IDE.openEditor(window.getActivePage(),
                  new ProjectEditorInput(project), MessageBundleEditor.ID);
          bundleEditor.selectMessageKey(messageKey.getCode(), null, null);
        }
        catch (PartInitException | RuntimeException ex)
        {
          StatusUtils.logError(getClass(), "Failed to open message bundle editor {0}.",
              MessageBundleEditor.ID, ex);
        }
      }

      @Override
      public Point getSelection(IDocument document)
      {
        return null;
      }

      @Override
      public IContextInformation getContextInformation()
      {
        return null;
      }

      @Override
      public int getRelevance()
      {
        return 100;
      }

    };
    proposals.add(proposal);

    // open message key declaration
    proposals.add(new IJavaCompletionProposal()
    {

      @Override
      public String getDisplayString()
      {
        return msgs.command_openMessageKeyDeclaration_name;
      }

      @Override
      public Image getImage()
      {
        return MessagesPlugin.getDefault().getImage(FIND_OPEN_MESSAGE_KEY_DECLARATION_ICON);
      }

      @Override
      public String getAdditionalProposalInfo()
      {
        return null;
      }

      @Override
      public void apply(IDocument document)
      {
        try
        {
          OpenMessageKeyDeclarationAction action =
              new OpenMessageKeyDeclarationAction(messageKeyService);
          action.execute(messageKey, null);
        }
        catch (CoreException | RuntimeException ex)
        {
          StatusUtils.logError(getClass(), "Failed to open declaration for message key ''{0}''.",
              messageKey.getCode(), ex);
        }
      }

      @Override
      public Point getSelection(IDocument document)
      {
        return null;
      }

      @Override
      public IContextInformation getContextInformation()
      {
        return null;
      }

      @Override
      public int getRelevance()
      {
        return 105;
      }

    });

    // find message key uses
    proposals.add(new IJavaCompletionProposal()
    {

      @Override
      public String getDisplayString()
      {
        return msgs.command_findMessageKeyUses_name;
      }

      @Override
      public Image getImage()
      {
        return MessagesPlugin.getDefault().getImage(FIND_MESSAGE_KEY_USES_ICON);
      }

      @Override
      public String getAdditionalProposalInfo()
      {
        return null;
      }

      @Override
      public void apply(IDocument document)
      {
        try
        {
          StartMessageKeyReferenceSearchAction action =
              new StartMessageKeyReferenceSearchAction(messageKeyService);
          action.execute(messageKey, null);
        }
        catch (CoreException | RuntimeException ex)
        {
          StatusUtils.logError(getClass(),
              "Failed to start search for uses of message key ''{0}''.", messageKey.getCode(), ex);
        }
      }

      @Override
      public Point getSelection(IDocument document)
      {
        return null;
      }

      @Override
      public IContextInformation getContextInformation()
      {
        return null;
      }

      @Override
      public int getRelevance()
      {
        return 104;
      }

    });

    // show key in view
    proposals.add(new IJavaCompletionProposal()
    {

      @Override
      public String getDisplayString()
      {
        return msgs.command_showInMessageKeysView_name;
      }

      @Override
      public Image getImage()
      {
        return MessagesPlugin.getDefault().getImage(SHOW_IN_VIEW_ICON);
      }

      @Override
      public String getAdditionalProposalInfo()
      {
        return null;
      }

      @Override
      public void apply(IDocument document)
      {
        try
        {
          MPart viewPart = partService.showPart(MessageKeysView.ID, PartState.ACTIVATE);
          MessageKeysView view = (MessageKeysView) viewPart.getObject();
          view.selectAndReveal(messageKey.getCode());
        }
        catch (RuntimeException ex)
        {
          StatusUtils.logError(getClass(),
              "Failed to start search for uses of message key ''{0}''.", messageKey.getCode(), ex);
        }
      }

      @Override
      public Point getSelection(IDocument document)
      {
        return null;
      }

      @Override
      public IContextInformation getContextInformation()
      {
        return null;
      }

      @Override
      public int getRelevance()
      {
        return 103;
      }

    });

    return proposals.toArray(new IJavaCompletionProposal[proposals.size()]);
  }


  protected Optional<IdeMessageKey> findMessageKeyFromContext(IInvocationContext context)
  {
    return MessageKeyAstUtils.findMessageKeyFromAstNode(
        context.getCompilationUnit().getJavaProject().getProject(), context.getCoveringNode(),
        messageKeyService);
  }

}
