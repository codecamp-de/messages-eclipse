package de.codecamp.messages.eclipse.ide.validation;


import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.shared.bundle.BundleFile;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;


public class BundleMismatchValidationTask
  implements
    BundleFileValidationTask
{

  @Override
  public void validate(ValidationResultAdapter result, BundleFileValidationContext context,
      BundleFile<IFile, CoreException> bundleFile, BundleFile<IFile, CoreException> rootBundleFile)
  {
    for (String key : bundleFile.getKeys())
    {
      IdeMessageKey mkWithSource = context.getDeclaredMessageKeys().get(key);

      String targetBundleName = context.getProjectConf().toTargetBundleName(key).orElse(null);

      if (targetBundleName == null || bundleFile.getBundleName().equals(targetBundleName))
        continue;

      String sourceType = mkWithSource != null ? mkWithSource.getSourceType() : null;
      String msg = "Bundle mismatch for message key ''{0}''. Should be moved to ''"
          + context.getMessageBundleManager()
              .getBundleFile(targetBundleName, bundleFile.getLocale(), true).getDisplayPath()
          + "'' instead.";
      result.addBundleError(bundleFile, key, sourceType, msg);
    }
  }

}
