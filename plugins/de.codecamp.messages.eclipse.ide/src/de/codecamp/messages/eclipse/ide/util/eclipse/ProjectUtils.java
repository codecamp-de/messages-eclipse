package de.codecamp.messages.eclipse.ide.util.eclipse;


import java.util.stream.Stream;
import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;


public final class ProjectUtils
{

  private ProjectUtils()
  {
    // utility class
  }


  public static boolean hasBuilder(IProject project, String builderId)
    throws CoreException
  {
    for (ICommand command : project.getDescription().getBuildSpec())
    {
      if (command.getBuilderName().equals(builderId))
      {
        return true;
      }
    }
    return false;
  }

  public static void addBuilder(IProject project, String builderId, IProgressMonitor monitor)
    throws CoreException
  {
    if (hasBuilder(project, builderId))
      return;

    IProjectDescription projectDescription = project.getDescription();
    ICommand[] buildSpec = projectDescription.getBuildSpec();

    ICommand newCommand = projectDescription.newCommand();
    newCommand.setBuilderName(builderId);

    ICommand[] newBuildSpec = new ICommand[buildSpec.length + 1];
    System.arraycopy(buildSpec, 0, newBuildSpec, 0, buildSpec.length);
    newBuildSpec[newBuildSpec.length - 1] = newCommand;

    projectDescription.setBuildSpec(newBuildSpec);
    project.setDescription(projectDescription, monitor);
  }

  public static void removeBuilder(IProject project, String builderId, IProgressMonitor monitor)
    throws CoreException
  {
    if (!hasBuilder(project, builderId))
      return;

    IProjectDescription projectDescription = project.getDescription();

    ICommand[] newBuildSpec = Stream.of(projectDescription.getBuildSpec())
        .filter(c -> !c.getBuilderName().equals(builderId)).toArray(ICommand[]::new);

    projectDescription.setBuildSpec(newBuildSpec);
    project.setDescription(projectDescription, monitor);
  }

}
