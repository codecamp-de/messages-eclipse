package de.codecamp.messages.eclipse.ide.actions;


import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.ui.IPackagesViewPart;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ISetSelectionTarget;


public class ShowMessageBundleDirectoryAction
{

  private final MessageKeyService messageKeyService;


  public ShowMessageBundleDirectoryAction(MessageKeyService messageKeyService)
  {
    this.messageKeyService = messageKeyService;
  }


  public void execute(IProject project, IProgressMonitor monitor)
    throws CoreException
  {
    IdeProjectConf projectConf = messageKeyService.getProjectConf(project).orElse(null);
    if (projectConf == null)
      return;

    IFolder bundleDir = projectConf.getBundleDirAsFolder().orElse(null);
    if (bundleDir == null)
      return;

    IWorkbenchPage workbenchPage =
        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

    boolean shown = false;
    IPackagesViewPart packagesViewPart =
        (IPackagesViewPart) workbenchPage.findView(JavaUI.ID_PACKAGES);
    if (packagesViewPart == null)
    {
      ISetSelectionTarget projectExporer =
          (ISetSelectionTarget) workbenchPage.findView(IPageLayout.ID_PROJECT_EXPLORER);
      if (projectExporer != null)
      {
        projectExporer.selectReveal(new StructuredSelection(bundleDir));
        shown = true;
      }
    }

    if (!shown)
    {
      packagesViewPart = (IPackagesViewPart) workbenchPage.showView(JavaUI.ID_PACKAGES);
      packagesViewPart.selectAndReveal(bundleDir);
    }
  }

}
