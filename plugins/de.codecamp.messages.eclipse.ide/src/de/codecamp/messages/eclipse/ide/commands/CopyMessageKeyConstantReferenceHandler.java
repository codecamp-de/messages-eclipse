package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.codegen.MessageCodegenUtils;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.lang.model.SourceVersion;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.ui.javaeditor.ClipboardOperationAction;
import org.eclipse.jdt.internal.ui.javaeditor.ClipboardOperationAction.ClipboardData;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Shell;


/**
 * Copies a reference to the constant of the selected message key to the clipboard in a way that
 * allows JDT's Java editor to automatically add the required import.
 */
@SuppressWarnings("restriction")
public class CopyMessageKeyConstantReferenceHandler
{

  @CanExecute
  @Evaluate
  public boolean canExecute(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    return (selection.getFirstElement() instanceof IdeMessageKey);
  }

  @Execute
  public void execute(@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
      @Named(IServiceConstants.ACTIVE_SHELL) Shell shell)
  {
    /*
     * JDT's Java editor provides copy/paste that can automatically add required imports on pasting.
     * Unfortunately it's not part of the public API, so implementing the copy-part here requires
     * some reflection and discouraged access.
     */
    try
    {
      IdeMessageKey messageKey = (IdeMessageKey) selection.getFirstElement();

      IJavaProject javaProject = JavaCore.create(messageKey.getProject());
      if (javaProject == null)
        return;

      IType type = javaProject.findType(messageKey.getSourceType());
      if (type == null)
        return;

      Clipboard clipboard = new Clipboard(shell.getDisplay());
      try
      {
        List<Object> datas = new ArrayList<>();
        List<ByteArrayTransfer> transfers = new ArrayList<>();

        String packagePart = type.getPackageFragment().getElementName();
        String classPart = type.getTypeQualifiedName('.');

        String messageConstantsSimpleTypeName =
            MessageCodegenUtils.getMessageConstantsSimpleTypeNameFor(classPart);

        // prepare an unqualified reference of the message key constant (the actual text that will
        // be pasted)

        String constantName = messageKey.getLocalPart();
        if (SourceVersion.isKeyword(constantName))
          constantName += "_";
        datas.add(messageConstantsSimpleTypeName + "." + constantName);
        transfers.add(TextTransfer.getInstance());


        // prepare the data that is used by the Java editor to add required imports

        ClipboardData clipboardData = new ClipboardData(type,
            new String[] {packagePart + "." + messageConstantsSimpleTypeName}, new String[] {});

        Field fgTransferInstanceField =
            ClipboardOperationAction.class.getDeclaredField("fgTransferInstance");
        fgTransferInstanceField.setAccessible(true);
        ByteArrayTransfer fgTransferInstance =
            (ByteArrayTransfer) fgTransferInstanceField.get(null);
        datas.add(clipboardData);
        transfers.add(fgTransferInstance);


        setClipboardContents(clipboard, datas.toArray(),
            transfers.toArray(new Transfer[transfers.size()]));
      }
      finally
      {
        clipboard.dispose();
      }
    }
    catch (JavaModelException | NoSuchFieldException | IllegalAccessException | RuntimeException ex)
    {
      StatusUtils.showError(getClass(), "", "Failed to copy message key to clipboard.", ex);
    }
  }

  private void setClipboardContents(Clipboard clipboard, Object[] datas, Transfer[] transfers)
  {
    try
    {
      clipboard.setContents(datas, transfers);
    }
    catch (SWTError e)
    {
      if (e.code != DND.ERROR_CANNOT_SET_CLIPBOARD)
      {
        throw e;
      }
      // silently fail. see e.g. https://bugs.eclipse.org/bugs/show_bug.cgi?id=65975
    }
  }

}
