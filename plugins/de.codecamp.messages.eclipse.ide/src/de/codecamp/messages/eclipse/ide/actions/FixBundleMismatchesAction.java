package de.codecamp.messages.eclipse.ide.actions;


import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import de.codecamp.messages.shared.bundle.BundleFile;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;


public class FixBundleMismatchesAction
{

  public FixBundleMismatchesAction()
  {
  }


  public int execute(IdeProjectConf projectConf, EclipseMessageBundleManager bundleManager)
  {
    boolean localBundleManager = false;
    if (bundleManager == null)
    {
      localBundleManager = true;
      bundleManager = projectConf.createEclipseMessageBundleManager();
    }

    int messagesMoved = 0;
    for (BundleFile<IFile, CoreException> bundleFile : bundleManager.getBundleFiles())
    {
      messagesMoved += processBundleFile(projectConf, bundleManager, bundleFile);
    }

    if (localBundleManager && messagesMoved > 0)
    {
      bundleManager.save();
    }

    return messagesMoved;
  }

  private int processBundleFile(IdeProjectConf projectConf,
      EclipseMessageBundleManager bundleManager, BundleFile<IFile, CoreException> bundleFile)
  {
    int messagesMoved = 0;
    for (String key : bundleFile.getKeys())
    {
      String expectedBundleName = projectConf.toTargetBundleName(key).orElse(null);
      if (expectedBundleName == null || bundleFile.getBundleName().equals(expectedBundleName))
        continue;

      BundleFile<IFile, CoreException> expectedBundleFile =
          bundleManager.getBundleFile(expectedBundleName, bundleFile.getLocale(), true);

      String message = bundleFile.getMessage(key);
      String comment = bundleFile.getComment(key);

      if (expectedBundleFile.hasMessageKey(key))
      {
        // target bundle file already contains message key

        String messageInTarget = expectedBundleFile.getMessage(key);
        if (StringUtils.isEmpty(messageInTarget))
        {
          // message in target bundle is empty -> overwrite
          bundleFile.removeMessage(key);
          expectedBundleFile.setMessage(key, message);
          expectedBundleFile.setComment(key, comment);
          messagesMoved++;
        }
        else if (StringUtils.isEmpty(message))
        {
          // misplaced key's message is empty -> removing from source bundle is enough
          bundleFile.removeMessage(key);
          messagesMoved++;
        }
        else if (message.equals(messageInTarget))
        {
          // both message are the same -> removing from source bundle is enough
          bundleFile.removeMessage(key);
          messagesMoved++;
        }
        else
        {
          // cannot reconcile automatically -> show error
          String msg = "Message key ''{0}'' does not match bundle file ''{1}''. However, the"
              + " message key already exists in ''{2}'' with a different message. You need to"
              + " manually reconcile this.";
          StatusUtils.showWarning(getClass(), null, msg, key, bundleFile.getDisplayPath(),
              expectedBundleFile.getDisplayPath());
        }
      }
      else
      {
        bundleFile.removeMessage(key);
        expectedBundleFile.setMessage(key, message);
        expectedBundleFile.setComment(key, comment);
        messagesMoved++;
      }
    }
    return messagesMoved;
  }

}
