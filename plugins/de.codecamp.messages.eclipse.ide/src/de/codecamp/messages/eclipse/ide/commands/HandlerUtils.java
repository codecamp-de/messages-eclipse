package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.eclipse.ide.services.AbstractItem;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import java.util.Optional;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Adapters;


public final class HandlerUtils
{

  private HandlerUtils()
  {
    // utility class
  }


  public static boolean hasProject(Object element)
  {
    return element instanceof IProject || element instanceof IResource
        || element instanceof AbstractItem || element instanceof IdeMessageKey
        || Adapters.adapt(element, IProject.class) != null;
  }

  public static Optional<IProject> getProject(Object element)
  {
    IProject project;
    if (element instanceof IProject)
      project = (IProject) element;
    else if (element instanceof IResource)
      project = ((IResource) element).getProject();
    else if (element instanceof AbstractItem)
      project = ((AbstractItem) element).getProject();
    else if (element instanceof IdeMessageKey)
      project = ((IdeMessageKey) element).getProject();
    else
      project = Adapters.adapt(element, IProject.class);

    return Optional.ofNullable(project);
  }

}
