package de.codecamp.messages.eclipse.ide.services;


@FunctionalInterface
public interface MessageKeyListener
{

  void onUpdate(MessageKeyEvent event);

}
