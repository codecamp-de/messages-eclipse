package de.codecamp.messages.eclipse.ide.ui;


import de.codecamp.messages.eclipse.ide.util.eclipse.E4CompatibilityUtils;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;


public class MessageKeyTreeShowOnlyProblemsHandler
{

  @CanExecute
  public boolean canExecute(MPart part, MItem item)
  {
    MessageKeyTreeAdapter adapter = E4CompatibilityUtils.adapt(part, MessageKeyTreeAdapter.class);
    if (adapter == null)
      return false;

    item.setSelected(adapter.getShowOnlyKeysWithProblems());

    return true;
  }

  @Execute
  public void execute(MPart part, MItem item)
  {
    MessageKeyTreeAdapter adapter = E4CompatibilityUtils.adapt(part, MessageKeyTreeAdapter.class);
    if (adapter == null)
      return;

    boolean flag = !adapter.getShowOnlyKeysWithProblems();

    adapter.setShowOnlyKeysWithProblems(flag);

    item.setSelected(flag);
  }

}
