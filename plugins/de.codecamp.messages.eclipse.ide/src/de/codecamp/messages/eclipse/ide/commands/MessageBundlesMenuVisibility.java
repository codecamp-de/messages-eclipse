package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import javax.inject.Inject;
import javax.inject.Named;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.IStructuredSelection;


public class MessageBundlesMenuVisibility
{

  @Inject
  private MessageKeyService messageKeyService;


  @Evaluate
  public boolean isVisible(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    // this menu is intended for the Package Explorer, Project Explorer or similar views
    if (selection.size() != 1)
      return false;

    Object element = selection.getFirstElement();
    if (element instanceof IJavaProject)
    {
      element = ((IJavaProject) element).getProject();
    }

    if (!(element instanceof IResource))
      return false;

    IResource resource = (IResource) element;

    IdeProjectConf projectConf =
        messageKeyService.getProjectConf(resource.getProject()).orElse(null);
    if (projectConf == null || !projectConf.getBundleDirAsFolder().isPresent())
      return false;

    return resource instanceof IProject || projectConf.isMessageBundleDir(resource)
        || projectConf.isMessageBundleFile(resource);
  }

}
