package de.codecamp.messages.eclipse.ide.services;


import java.util.Collections;
import java.util.Set;
import org.eclipse.core.resources.IProject;


public class MessageKeyEvent
{

  private final Type type;

  private final IProject project;

  private final Set<IdeMessageKey> addedKeys;

  private final Set<IdeMessageKey> removedKeys;


  private MessageKeyEvent(Type type, IProject project, Set<IdeMessageKey> addedKeys,
      Set<IdeMessageKey> removedKeys)
  {
    if (type == null)
      throw new IllegalArgumentException("type must not be null");
    if (project == null)
      throw new IllegalArgumentException("project must not be null");

    this.type = type;
    this.project = project;

    if (addedKeys == null || addedKeys.isEmpty())
      this.addedKeys = Collections.emptySet();
    else
      this.addedKeys = Collections.unmodifiableSet(addedKeys);

    if (removedKeys == null || removedKeys.isEmpty())
      this.removedKeys = Collections.emptySet();
    else
      this.removedKeys = Collections.unmodifiableSet(removedKeys);
  }


  public static MessageKeyEvent createProjectAdded(IProject project)
  {
    return new MessageKeyEvent(Type.PROJECT_ADDED, project, null, null);
  }

  public static MessageKeyEvent createProjectRemoved(IProject project)
  {
    return new MessageKeyEvent(Type.PROJECT_REMOVED, project, null, null);
  }

  public static MessageKeyEvent createProjectRefreshed(IProject project)
  {
    return new MessageKeyEvent(Type.PROJECT_REFRESHED, project, null, null);
  }

  public static MessageKeyEvent createProjectUpdated(IProject project)
  {
    return new MessageKeyEvent(Type.PROJECT_UPDATED, project, null, null);
  }

  public static MessageKeyEvent createProblemsUpdated(IProject project)
  {
    return new MessageKeyEvent(Type.PROBLEMS_UPDATED, project, null, null);
  }

  public static MessageKeyEvent createMessageKeysUpdated(IProject project,
      Set<IdeMessageKey> addedKeys, Set<IdeMessageKey> removedKeys)
  {
    return new MessageKeyEvent(Type.MESSAGE_KEYS_UPDATED, project, addedKeys, removedKeys);
  }


  public Type getType()
  {
    return type;
  }

  public IProject getProject()
  {
    return project;
  }

  public Set<IdeMessageKey> getAddedKeys()
  {
    return addedKeys;
  }

  public Set<IdeMessageKey> getRemovedKeys()
  {
    return removedKeys;
  }


  public enum Type
  {
    PROJECT_ADDED,
    PROJECT_REMOVED,
    PROJECT_REFRESHED,
    PROJECT_UPDATED,
    PROBLEMS_UPDATED,
    MESSAGE_KEYS_UPDATED,
  }

}
