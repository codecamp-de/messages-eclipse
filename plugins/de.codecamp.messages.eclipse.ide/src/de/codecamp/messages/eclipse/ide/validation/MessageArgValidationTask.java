package de.codecamp.messages.eclipse.ide.validation;


import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import de.codecamp.messages.shared.bundle.BundleFile;
import de.codecamp.messages.shared.messageformat.MessageFormatSupport;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaModelException;


public class MessageArgValidationTask
  implements
    BundleFileValidationTask
{

  private static final Map<String, String> PRIMITIVE_TYPES;
  static
  {
    Map<String, String> temp = new HashMap<>();
    temp.put(byte.class.getName(), Byte.class.getName());
    temp.put(short.class.getName(), Short.class.getName());
    temp.put(char.class.getName(), Character.class.getName());
    temp.put(int.class.getName(), Integer.class.getName());
    temp.put(long.class.getName(), Long.class.getName());
    temp.put(float.class.getName(), Float.class.getName());
    temp.put(double.class.getName(), Double.class.getName());
    temp.put(boolean.class.getName(), Boolean.class.getName());
    temp.put(void.class.getName(), Void.class.getName());
    PRIMITIVE_TYPES = Collections.unmodifiableMap(temp);
  }


  private final Map<String, IType> typeCache = new HashMap<>();


  @Override
  public void validate(ValidationResultAdapter result, BundleFileValidationContext context,
      BundleFile<IFile, CoreException> bundleFile, BundleFile<IFile, CoreException> rootBundleFile)
  {
    for (String key : bundleFile.getKeys())
    {
      IdeMessageKey mkWithSource = context.getDeclaredMessageKeys().get(key);
      if (mkWithSource == null)
        continue;

      String message = bundleFile.getMessage(key);
      if (message == null || message.isEmpty())
        continue;

      MessageFormatSupport messageFormatSupport =
          MessageFormatSupport.getSupport(context.getProjectConf());

      List<String> errors = messageFormatSupport.checkMessage(message, mkWithSource.getArgTypes(),
          mkWithSource.getArgNames(), (argTypeName, expectedArgTypeNames) ->
          {
            // replace primitive types with boxed version
            argTypeName = PRIMITIVE_TYPES.getOrDefault(argTypeName, argTypeName);

            IType argType = getType(context.getJavaProject(), argTypeName);
            if (argType == null)
              return false;

            try
            {
              ITypeHierarchy hierarchy = argType.newSupertypeHierarchy(null);
              for (IType st : hierarchy.getSupertypes(argType))
              {
                if (expectedArgTypeNames.contains(st.getFullyQualifiedName()))
                {
                  return true;
                }
              }
            }
            catch (JavaModelException ex)
            {
              StatusUtils.logError(getClass(), "Failed to determine format argument type.", ex);
            }

            return false;
          });

      for (String error : errors)
      {
        result.addBundleWarning(bundleFile, key, mkWithSource.getSourceType(), error);
      }
    }
  }

  private IType getType(IJavaProject javaProject, String typeName)
  {
    /*
     * Find the source type of the key to declare a dependency on it. If the source type changes,
     * the bundle file will automatically be validated again.
     */
    IType type = typeCache.get(typeName);
    if (type == null)
    {
      try
      {
        type = javaProject.findType(typeName);
        if (type != null)
        {
          typeCache.put(typeName, type);
        }
      }
      catch (JavaModelException ex)
      {
        StatusUtils.logError(getClass(), "Failed to find IType for ''{0}''.", typeName, ex);
      }
    }
    return type;
  }

}
