package de.codecamp.messages.eclipse.ide.ui;


import static de.codecamp.messages.eclipse.ide.ui.Constants.STATE_GROUP_BY_MODULES_DEFAULT;
import static de.codecamp.messages.eclipse.ide.ui.Constants.STATE_GROUP_BY_SOURCE_TYPE_DEFAULT;
import static de.codecamp.messages.eclipse.ide.ui.Constants.STATE_SHOW_IMPORTED_KEYS_DEFAULT;
import static de.codecamp.messages.eclipse.ide.ui.Constants.STATE_SHOW_ONLY_KEYS_WITH_PROBLEMS_DEFAULT;
import static de.codecamp.messages.eclipse.ide.ui.Constants.VIEWSTATE_GROUP_BY_MODULES;
import static de.codecamp.messages.eclipse.ide.ui.Constants.VIEWSTATE_GROUP_BY_SOURCE_TYPE;
import static de.codecamp.messages.eclipse.ide.ui.Constants.VIEWSTATE_SHOW_IMPORTED_KEYS;
import static de.codecamp.messages.eclipse.ide.ui.Constants.VIEWSTATE_SHOW_ONLY_KEYS_WITH_PROBLEMS;

import de.codecamp.messages.eclipse.ide.commands.OpenMessageKeyDeclarationHandler;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.IdeModule;
import de.codecamp.messages.eclipse.ide.services.IdeSourceType;
import de.codecamp.messages.eclipse.ide.services.MessageKeyEvent;
import de.codecamp.messages.eclipse.ide.services.MessageKeyEvent.Type;
import de.codecamp.messages.eclipse.ide.services.MessageKeyListener;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService.MessageAvailability;
import de.codecamp.messages.eclipse.ide.util.eclipse.PersistedStateUtils;
import de.codecamp.messages.eclipse.ide.util.jface.Stylers;
import de.codecamp.messages.eclipse.ide.util.swt.EventDelay;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.ui.di.PersistState;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.preference.JFacePreferences;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.BoldStylerProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;


public class MessageKeysView
  implements
    IAdaptable
{

  public static final String ID = "de.codecamp.messages.views.messagekeys";

  public static final String CONTEXTMENU_ID = "de.codecamp.messages.messagekeys.contextmenu";


  @Inject
  private ESelectionService selectionService;

  @Inject
  private MessageKeyService messageKeyService;

  @Inject
  private ECommandService commandService;

  @Inject
  private EHandlerService handlerService;


  private MessageKeyListener messageKeyListener;


  private Text searchBarText;

  private TreeViewer treeViewer;

  private MessageKeyTreeContentProvider treeViewerContentProvider;


  private Font localeColumnFont;

  private BoldStylerProvider localeColumnBoldStylerProvider;


  public MessageKeysView()
  {
  }


  @PostConstruct
  public void createControls(Composite parent, MPart part, EMenuService menuService)
  {
    localeColumnFont = JFaceResources.getTextFont();
    localeColumnBoldStylerProvider = new BoldStylerProvider(localeColumnFont);

    treeViewerContentProvider = new MessageKeyTreeContentProvider(messageKeyService);

    treeViewerContentProvider
        .setGroupBySourceType(PersistedStateUtils.getBoolean(part.getPersistedState(),
            VIEWSTATE_GROUP_BY_SOURCE_TYPE, STATE_GROUP_BY_SOURCE_TYPE_DEFAULT));
    treeViewerContentProvider.setShowImportedKeys(PersistedStateUtils.getBoolean(
        part.getPersistedState(), VIEWSTATE_SHOW_IMPORTED_KEYS, STATE_SHOW_IMPORTED_KEYS_DEFAULT));
    treeViewerContentProvider.setGroupByModules(PersistedStateUtils.getBoolean(
        part.getPersistedState(), VIEWSTATE_GROUP_BY_MODULES, STATE_GROUP_BY_MODULES_DEFAULT));
    treeViewerContentProvider
        .setShowOnlyKeysWithProblems(PersistedStateUtils.getBoolean(part.getPersistedState(),
            VIEWSTATE_SHOW_ONLY_KEYS_WITH_PROBLEMS, STATE_SHOW_ONLY_KEYS_WITH_PROBLEMS_DEFAULT));


    Composite searchBarContainer = new Composite(parent, SWT.BORDER);
    GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false)
        .applyTo(searchBarContainer);
    searchBarContainer.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));

    searchBarText = new Text(searchBarContainer, SWT.FLAT);
    GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, true)
        .applyTo(searchBarText);
    searchBarText.setMessage("Filter");
    searchBarText.addModifyListener(event ->
    {
      treeViewerContentProvider.setFilterText(searchBarText.getText());
    });

    Button searchBarClearButton = new Button(searchBarContainer, SWT.NONE);
    searchBarClearButton.setImage(
        PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_CLEAR));
    GridDataFactory.swtDefaults().grab(false, true).applyTo(searchBarClearButton);

    GridLayoutFactory.fillDefaults().numColumns(2).generateLayout(searchBarContainer);


    Button legendButton =
        MessageProblemLegendPopup.createLegendButton(parent, localeColumnBoldStylerProvider);
    GridDataFactory.fillDefaults().applyTo(legendButton);


    Composite treeContainer = new Composite(parent, SWT.NONE);
    GridDataFactory.fillDefaults().grab(true, true).span(2, 1).applyTo(treeContainer);
    TreeColumnLayout treeColumnLayout = new TreeColumnLayout();
    treeContainer.setLayout(treeColumnLayout);

    treeViewer =
        new TreeViewer(treeContainer, SWT.BORDER | SWT.SINGLE | SWT.FULL_SELECTION | SWT.VIRTUAL);
    treeViewer.setUseHashlookup(true);
    treeViewer.setContentProvider(treeViewerContentProvider);
    ColumnViewerToolTipSupport.enableFor(treeViewer);

    treeViewer.getTree().setHeaderVisible(true);

    TreeViewerColumn nameColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
    nameColumn.getColumn().setText("Name");
    nameColumn.setLabelProvider(new MessageKeyTreeNameColumnLabelProvider(messageKeyService));

    TreeViewerColumn targetLocalesColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
    targetLocalesColumn.getColumn().setText("Target Locales");
    targetLocalesColumn
        .setLabelProvider(new StyledCellLabelProvider(StyledCellLabelProvider.COLORS_ON_SELECTION)
        {

          @Override
          public void update(ViewerCell cell)
          {
            Object element = cell.getElement();

            if (element instanceof IdeModule)
            {
              IdeModule module = (IdeModule) element;

              IdeProjectConf projectConf =
                  messageKeyService.getProjectConf(module.getProject()).orElse(null);
              if (projectConf != null)
              {
                List<Locale> locales = new ArrayList<>();
                locales.add(Locale.ROOT);
                locales.addAll(projectConf.getTargetLocales());

                StringBuilder targetLocales = new StringBuilder();
                boolean first = true;
                for (Locale locale : locales)
                {
                  if (first)
                    first = false;
                  else
                    targetLocales.append("|");

                  String localeText;
                  if (Locale.ROOT.equals(locale))
                    localeText = "Base";
                  else
                    localeText = locale.toString();

                  targetLocales.append(localeText);
                }

                cell.setFont(localeColumnFont);
                cell.setText(targetLocales.toString());
                cell.setStyleRanges(null);
              }
              else
              {
                cell.setText(null);
                cell.setStyleRanges(null);
              }
            }

            else if (element instanceof IdeSourceType)
            {
              cell.setText(null);
              cell.setStyleRanges(null);
            }

            else if (element instanceof IdeMessageKey)
            {
              IdeMessageKey messageKey = (IdeMessageKey) element;

              IdeProjectConf projectConf =
                  messageKeyService.getProjectConf(messageKey.getProject(), true).orElse(null);
              if (projectConf != null)
              {
                StyledString ss = new StyledString();

                List<Locale> locales = new ArrayList<>();
                locales.add(Locale.ROOT);
                locales.addAll(projectConf.getTargetLocales());

                boolean first = true;
                for (Locale locale : locales)
                {
                  if (first)
                    first = false;
                  else
                    ss.append("|");

                  String localeText;
                  if (Locale.ROOT.equals(locale))
                    localeText = "Base";
                  else
                    localeText = locale.toString();

                  ss.append(localeText, getStyler(messageKeyService, messageKey, locale,
                      localeColumnBoldStylerProvider));
                }

                cell.setFont(localeColumnFont);
                cell.setText(ss.getString());
                cell.setStyleRanges(ss.getStyleRanges());
              }
            }

            super.update(cell);
          }

        });

    TreeViewerColumn sourceTypeColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
    sourceTypeColumn.getColumn().setText("Source Type");
    sourceTypeColumn.setLabelProvider(new ColumnLabelProvider()
    {

      @Override
      public String getText(Object element)
      {
        if (element instanceof IdeMessageKey)
        {
          return ((IdeMessageKey) element).getSourceType();
        }

        return null;
      }

      @Override
      public String getToolTipText(Object element)
      {
        if (element instanceof IdeMessageKey)
        {
          return ((IdeMessageKey) element).getSourceType();
        }

        return null;
      }

    });

    TreeViewerColumn bundleNameColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
    bundleNameColumn.getColumn().setText("Bundle Name");
    bundleNameColumn.setLabelProvider(new ColumnLabelProvider()
    {

      @Override
      public String getText(Object element)
      {
        if (element instanceof IdeMessageKey)
        {
          IdeMessageKey messageKey = (IdeMessageKey) element;

          IdeProjectConf projectConf =
              messageKeyService.getProjectConf(messageKey.getProject(), true).orElse(null);
          if (projectConf != null)
          {
            return toShortenedBundleNameWithLocale(messageKey, projectConf, null);
          }
        }

        return null;
      }

      @Override
      public String getToolTipText(Object element)
      {
        if (element instanceof IdeMessageKey)
        {
          IdeMessageKey messageKey = (IdeMessageKey) element;

          IdeProjectConf projectConf =
              messageKeyService.getProjectConf(messageKey.getProject(), true).orElse(null);
          if (projectConf != null)
          {
            return projectConf.toTargetBundleName(messageKey.getCode()).orElse(null);
          }
        }

        return null;
      }

    });


    treeColumnLayout.setColumnData(nameColumn.getColumn(), new ColumnWeightData(2, 400));
    treeColumnLayout.setColumnData(targetLocalesColumn.getColumn(), new ColumnWeightData(1, 150));
    treeColumnLayout.setColumnData(sourceTypeColumn.getColumn(), new ColumnWeightData(0, 300));
    treeColumnLayout.setColumnData(bundleNameColumn.getColumn(), new ColumnWeightData(0, 200));

    /*
     * Examines the children of the given composite, so this must be called after adding all
     * children!
     */
    GridLayoutFactory.swtDefaults().numColumns(2).generateLayout(parent);



    treeViewer.getTree().addKeyListener(new KeyAdapter()
    {

      @Override
      public void keyPressed(KeyEvent e)
      {
        if (e.stateMask == SWT.MOD1 && e.keyCode == 'f')
        {
          e.doit = false;
          searchBarText.selectAll();
          searchBarText.setFocus();
        }
      }

    });


    EventDelay searchBarTextEventDelay = new EventDelay(searchBarText);
    searchBarTextEventDelay.addListener(SWT.Modify, event ->
    {
      treeViewer.refresh();
    }, 1000);

    searchBarText.addSelectionListener(new SelectionAdapter()
    {

      @Override
      public void widgetDefaultSelected(SelectionEvent e)
      {
        searchBarTextEventDelay.flushAll();
      }

    });

    searchBarClearButton.setEnabled(false);
    searchBarText.addModifyListener(event ->
    {
      searchBarClearButton.setEnabled(!searchBarText.getText().isEmpty());
    });

    searchBarText.addKeyListener(new KeyAdapter()
    {

      @Override
      public void keyPressed(KeyEvent e)
      {
        if (e.keyCode == SWT.ESC)
        {
          searchBarText.setText("");
          searchBarTextEventDelay.flushAll();
        }
      }

    });

    searchBarClearButton.addSelectionListener(new SelectionAdapter()
    {

      @Override
      public void widgetSelected(SelectionEvent e)
      {
        searchBarText.setText("");
        searchBarTextEventDelay.flushAll();
      }

    });

    treeViewer.setAutoExpandLevel(2);

    treeViewer.addDoubleClickListener(event ->
    {
      IStructuredSelection selection = (IStructuredSelection) event.getSelection();
      Object element = selection.getFirstElement();
      if (element instanceof IdeModule || element instanceof IdeSourceType)
      {
        treeViewer.setExpandedState(element, !treeViewer.getExpandedState(element));
      }
      else if (element instanceof IdeMessageKey)
      {
        ParameterizedCommand command =
            commandService.createCommand(OpenMessageKeyDeclarationHandler.COMMAND_ID, null);
        handlerService.executeHandler(command);
      }
    });


    treeViewer.setInput(ResourcesPlugin.getWorkspace());


    messageKeyListener = new MessageKeyListener()
    {

      @Override
      public void onUpdate(MessageKeyEvent event)
      {
        messageKeyService.getModule(event.getProject()).ifPresent(module ->
        {
          treeViewer.getTree().getDisplay().asyncExec(() ->
          {
            if (event.getType() == Type.PROJECT_ADDED)
            {
              treeViewer.add(ResourcesPlugin.getWorkspace(), module);
            }
            else if (event.getType() == Type.PROJECT_REMOVED)
            {
              treeViewer.remove(module);
            }
            else if (event.getType() == Type.PROBLEMS_UPDATED)
            {
              treeViewer.getTree().clearAll(true);
            }
            else
            {
              treeViewer.refresh(module);
            }
          });
        });
      }

    };
    messageKeyService.addMessageKeyChangeListener(messageKeyListener);


    treeViewer.addSelectionChangedListener(event ->
    {
      if (!event.getStructuredSelection().isEmpty())
      {
        selectionService.setSelection(
            new StructuredSelection(event.getStructuredSelection().getFirstElement()));
      }
    });

    menuService.registerContextMenu(treeViewer.getControl(), CONTEXTMENU_ID);
  }

  public static Styler getStyler(MessageKeyService messageKeyService, IdeMessageKey messageKey,
      Locale locale, BoldStylerProvider boldStylerProvider)
  {
    IdeProjectConf projectConf =
        messageKeyService.getProjectConf(messageKey.getProject(), true).orElse(null);
    String targetBundleName = projectConf.toTargetBundleName(messageKey.getCode()).orElse(null);

    Set<Locale> requiredLocales =
        messageKeyService.getRequiredLocales(messageKey).orElse(Collections.emptySet());

    MessageAvailability messageAvailability;
    if (targetBundleName != null)
    {
      messageAvailability = messageKeyService.getMessageAvailability(messageKey.getProject(),
          targetBundleName, locale, messageKey.getCode());
    }
    else
    {
      messageAvailability = new MessageAvailability(false, false, false);
    }

    boolean isRequiredLocale = requiredLocales.contains(locale);
    Set<Locale> importedTargetLocales = messageKeyService.getTargetLocales(messageKey, true);

    List<Styler> stylers = new ArrayList<>();

    if (isRequiredLocale)
      stylers.add(boldStylerProvider.getBoldStyler());

    if (messageAvailability.isMessageKeyAvailable())
      stylers.add(Stylers.underline(SWT.UNDERLINE_SINGLE));

    IMarker marker = messageKeyService.findMostSevereBundleValidationMarker(messageKey, locale);
    if (marker != null && marker.getAttribute(IMarker.SEVERITY, -1) >= IMarker.SEVERITY_WARNING)
    {
      stylers.add(StyledString.createColorRegistryStyler(JFacePreferences.ERROR_COLOR, null));
    }
    else if (!messageAvailability.isBundleFileAvailable())
    {
      stylers.add(StyledString.QUALIFIER_STYLER);
    }

    if (messageKey.isImported() && importedTargetLocales.contains(locale))
    {
      stylers.add(Stylers.strikeout());
    }

    return Stylers.sequence(stylers);
  }

  @PersistState
  public void persistState(MPart part)
  {
    part.getPersistedState().put(VIEWSTATE_GROUP_BY_SOURCE_TYPE,
        Boolean.toString(treeViewerContentProvider.getGroupBySourceType()));
    part.getPersistedState().put(VIEWSTATE_SHOW_IMPORTED_KEYS,
        Boolean.toString(treeViewerContentProvider.getShowImportedKeys()));
    part.getPersistedState().put(VIEWSTATE_GROUP_BY_MODULES,
        Boolean.toString(treeViewerContentProvider.getGroupByModules()));
    part.getPersistedState().put(VIEWSTATE_SHOW_ONLY_KEYS_WITH_PROBLEMS,
        Boolean.toString(treeViewerContentProvider.getShowOnlyKeysWithProblems()));
  }

  @PreDestroy
  private void dispose()
  {
    if (messageKeyListener != null)
      messageKeyService.removeMessageKeyChangeListener(messageKeyListener);
    if (localeColumnBoldStylerProvider != null)
      localeColumnBoldStylerProvider.dispose();
  }



  public void selectAndReveal(String messageKeyCode)
  {
    TreePath treePath = treeViewerContentProvider.getTreePathFor(messageKeyCode);
    if (treePath == null)
      return;

    treeViewer.expandToLevel(treePath, 0);
    treeViewer.setSelection(new TreeSelection(treePath), true);
  }


  public static String toShortenedBundleNameWithLocale(IdeMessageKey messageKey,
      IdeProjectConf projectConf, Locale locale)
  {
    String bundleName = projectConf.toTargetBundleName(messageKey.getCode()).orElse(null);

    if (bundleName != null)
    {
      if (StringUtils.startsWith(bundleName, messageKey.getModuleName()))
      {
        // remove the prefixed module name for a more readable name
        bundleName = StringUtils.removeStart(bundleName, messageKey.getModuleName());
        bundleName = StringUtils.removeStart(bundleName, ".");
        bundleName = "..." + bundleName;
      }
      if (locale != null && !locale.equals(Locale.ROOT))
      {
        bundleName += "_" + locale.toString();
      }
    }

    return bundleName;
  }


  @Override
  public <T> T getAdapter(Class<T> adapter)
  {
    if (adapter == MessageKeyTreeAdapter.class)
      return adapter.cast(new MessageKeyTreeAdapter(treeViewer, treeViewerContentProvider));

    return null;
  }

}
