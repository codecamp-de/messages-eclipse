package de.codecamp.messages.eclipse.ide.validation;


import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.jdt.core.IJavaProject;


public class BundleFileValidationContext
{

  private final IdeProjectConf projectConf;

  private final IJavaProject javaProject;

  private final EclipseMessageBundleManager messageBundleManager;

  private final Map<String, IdeMessageKey> declaredMessageKeys;

  private final Map<String, Object> dataMap = new HashMap<>();


  public BundleFileValidationContext(IdeProjectConf projectConf, IJavaProject javaProject,
      EclipseMessageBundleManager messageBundleManager,
      Map<String, IdeMessageKey> declaredMessageKeys)
  {
    this.projectConf = projectConf;
    this.javaProject = javaProject;
    this.messageBundleManager = messageBundleManager;
    this.declaredMessageKeys = declaredMessageKeys;
  }


  public IdeProjectConf getProjectConf()
  {
    return projectConf;
  }

  public IJavaProject getJavaProject()
  {
    return javaProject;
  }

  public EclipseMessageBundleManager getMessageBundleManager()
  {
    return messageBundleManager;
  }

  public Map<String, IdeMessageKey> getDeclaredMessageKeys()
  {
    return declaredMessageKeys;
  }

  public void setData(String key, Object data)
  {
    dataMap.put(key, data);
  }

  public <T> void setData(Class<T> key, T data)
  {
    dataMap.put(key.getName(), data);
  }

  public Object getData(String key)
  {
    return dataMap.get(key);
  }

  public <T> T getData(Class<T> key)
  {
    return key.cast(dataMap.get(key.getName()));
  }

}
