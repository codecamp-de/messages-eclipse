package de.codecamp.messages.eclipse.ide.ui;


import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.ui.IEditorInput;


public interface IProjectEditorInput
  extends
    IEditorInput
{

  IProject getProject();

  IFile getLinkedFile();

  void setLinkedFile(IFile linkedFile);

}
