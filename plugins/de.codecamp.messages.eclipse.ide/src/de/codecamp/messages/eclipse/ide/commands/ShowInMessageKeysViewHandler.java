package de.codecamp.messages.eclipse.ide.commands;


import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.ui.MessageKeysView;
import javax.inject.Inject;
import javax.inject.Named;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.jface.viewers.IStructuredSelection;


/**
 * Opens the Message Keys view and selects and reveals the selected message key.
 */
public class ShowInMessageKeysViewHandler
{

  @Inject
  private EPartService partService;


  @CanExecute
  @Evaluate
  public boolean canExecute(
      @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
  {
    MPart activePart = partService.getActivePart();
    if (activePart != null && activePart.getObject() instanceof MessageKeysView)
      return false;

    return selection.getFirstElement() instanceof IdeMessageKey;
  }

  @Execute
  public void execute(@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection)
    throws CoreException
  {
    IdeMessageKey messageKey = (IdeMessageKey) selection.getFirstElement();

    MPart viewPart = partService.showPart(MessageKeysView.ID, PartState.ACTIVATE);
    MessageKeysView view = (MessageKeysView) viewPart.getObject();
    view.selectAndReveal(messageKey.getCode());
  }

}
