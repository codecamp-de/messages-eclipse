package de.codecamp.messages.eclipse.ide.util;


import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

import de.codecamp.messages.MessageKey;
import de.codecamp.messages.MessageKeyUtils;
import de.codecamp.messages.Messages;
import de.codecamp.messages.MessagesFor;
import de.codecamp.messages.ResolvableMessage;
import de.codecamp.messages.codegen.DeclaredMessageKey;
import de.codecamp.messages.codegen.MessageKeyConstants;
import de.codecamp.messages.codegen.MessageProxyMethod;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.IdeSourceType;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.jdt.JdtDomUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IAnnotationBinding;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.NormalAnnotation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;


public final class MessageKeyAstUtils
{

  private MessageKeyAstUtils()
  {
    // utility class
  }


  public static Optional<IdeMessageKey> findMessageKeyFromAstNode(IProject project,
      ASTNode coveringNode, MessageKeyService messageKeyService)
  {
    String messageKeyCode = null;

    if (coveringNode.getNodeType() == ASTNode.SIMPLE_NAME)
    {
      SimpleName simpleName = (SimpleName) coveringNode;

      IBinding binding = simpleName.resolveBinding();
      if (binding instanceof IVariableBinding)
      {
        IVariableBinding variableBinding = (IVariableBinding) binding;

        // quick assist on reference of DeclaredMessageKey message key constant
        if (variableBinding.getType().getQualifiedName().equals(DeclaredMessageKey.class.getName()))
        {
          String prefix = getSourceTypeFromConstantsClass(variableBinding.getDeclaringClass())
              .flatMap(MessageKeyAstUtils::getPrefixFromSourceType).orElse(null);
          if (prefix != null)
          {
            String localPart = variableBinding.getName();
            messageKeyCode = MessageKeyUtils.getKeyFor(prefix, localPart);
          }
        }

        // quick assist on reference of String message key constant
        else if (variableBinding.getType().getQualifiedName().equals(String.class.getName()))
        {
          String prefix = getSourceTypeFromConstantsClass(variableBinding.getDeclaringClass())
              .flatMap(MessageKeyAstUtils::getPrefixFromSourceType).orElse(null);
          if (prefix != null)
          {
            String localPart = StringUtils.removeEnd(variableBinding.getName(), "_");
            messageKeyCode = MessageKeyUtils.getKeyFor(prefix, localPart);
          }
        }

        // quick assist on a field annotated with @Messages
        else
        {
          messageKeyCode = getMessageKeyFromBinding(variableBinding, null, null);
        }
      }

      else if (binding instanceof IMethodBinding)
      {
        IMethodBinding methodBinding = (IMethodBinding) binding;

        if (methodBinding.getReturnType().getQualifiedName()
            .equals(ResolvableMessage.class.getName()))
        {
          String prefix = getSourceTypeFromConstantsClass(methodBinding.getDeclaringClass())
              .flatMap(MessageKeyAstUtils::getPrefixFromSourceType).orElse(null);
          if (prefix != null)
          {
            String localPart = methodBinding.getName();
            messageKeyCode = MessageKeyUtils.getKeyFor(prefix, localPart);
          }
        }
        else
        {
          // quick assist on bean getter method
          IAnnotationBinding typeMessagesAt =
              getMessagesAnnotationFromBinding(methodBinding.getDeclaringClass());
          if (typeMessagesAt != null)
          {
            if (JdtDomUtils.getAnnotationElementValue(typeMessagesAt, "forProperties",
                Boolean.class, false) && JdtDomUtils.isBeanPropertyGetter(methodBinding))
            {
              String prefix =
                  getPrefixFromSourceType(methodBinding.getDeclaringClass()).orElse(null);
              if (prefix != null)
              {
                String propertyName = JdtDomUtils.getBeanPropertyName(methodBinding);
                messageKeyCode = MessageKeyUtils.getKeyFor(prefix, propertyName);
              }
            }
          }
          else
          {
            for (IAnnotationBinding atBind : methodBinding.getAnnotations())
            {
              // quick assist on message key proxy method
              if (atBind.getAnnotationType().getQualifiedName()
                  .equals(MessageProxyMethod.class.getName()))
              {
                messageKeyCode =
                    JdtDomUtils.getAnnotationElementValue(atBind, "value", String.class);
                break;
              }
            }
          }
        }
      }

      else if (binding instanceof ITypeBinding)
      {
        ITypeBinding typeBinding = (ITypeBinding) binding;

        // quick assist directly on a @Messages annotation of a type
        if (typeBinding.isAnnotation())
        {
          if (typeBinding.getQualifiedName().equals(MessagesFor.class.getName()))
          {
            NormalAnnotation messagesForAt = (NormalAnnotation) simpleName.getParent();
            messageKeyCode =
                getMessageKeyForMessageForAnnotation(project, messagesForAt, messageKeyService);
          }
          else
          {
            messageKeyCode = getMessageKeyFromNode(simpleName.getParent(), null);
          }
        }
        else
        {
          NormalAnnotation messagesForAt = JdtDomUtils
              .findParent(simpleName, NormalAnnotation.class)
              .filter(
                  a -> a.getTypeName().getFullyQualifiedName().equals(MessagesFor.class.getName()))
              .orElse(null);
          if (messagesForAt != null)
          {
            messageKeyCode =
                getMessageKeyForMessageForAnnotation(project, messagesForAt, messageKeyService);
          }
          else
          {
            // quick assist on type with "forType"
            IAnnotationBinding messagesAt = getMessagesAnnotationFromBinding(typeBinding);
            if (messagesAt != null && BooleanUtils.isTrue(
                JdtDomUtils.getAnnotationElementValue(messagesAt, "forType", Boolean.class)))
            {
              String prefix = getPrefixFromSourceType(typeBinding).orElse(null);
              messageKeyCode = MessageKeyUtils.getKeyFor(prefix, MessageKey.TYPE_KEY_SUFFIX);
            }
          }
        }
      }
    }

    // quick assist on string literal in keys or value attribute of @Messages
    else if (coveringNode.getNodeType() == ASTNode.STRING_LITERAL)
    {
      StringLiteral stringLit = (StringLiteral) coveringNode;

      messageKeyCode = getMessageKeyFromNode(coveringNode.getParent(), stringLit.getLiteralValue());
    }

    if (messageKeyCode != null)
      return messageKeyService.findMessageKey(messageKeyCode, true, false);

    return Optional.empty();
  }

  private static String getMessageKeyForMessageForAnnotation(IProject project,
      NormalAnnotation messagesForAt, MessageKeyService messageKeyService)
  {
    IAnnotationBinding annotationBinding = messagesForAt.resolveAnnotationBinding();

    ITypeBinding sourceTypeBinding =
        JdtDomUtils.getAnnotationElementValue(annotationBinding, "type", ITypeBinding.class);
    Optional<IdeSourceType> sourceTypeOpt = messageKeyService.findSourceType(project,
        sourceTypeBinding.getQualifiedName(), true, false);
    if (sourceTypeOpt.isPresent())
    {
      return messageKeyService.getMessageKeys(sourceTypeOpt.get(), true, false, null, false)
          .findAny().map(IdeMessageKey::getCode).orElse(null);
    }
    return null;
  }

  private static Optional<ITypeBinding> getSourceTypeFromConstantsClass(ITypeBinding constantsClass)
  {
    ITypeBinding sourceType = null;

    if (constantsClass != null)
    {
      for (IAnnotationBinding atBind : constantsClass.getAnnotations())
      {
        if (atBind.getAnnotationType().getQualifiedName()
            .equals(MessageKeyConstants.class.getName()))
        {
          sourceType = JdtDomUtils.getAnnotationElementValue(atBind, "value", ITypeBinding.class);
          break;
        }
      }
    }

    return Optional.ofNullable(sourceType);
  }

  private static Optional<String> getPrefixFromSourceType(ITypeBinding sourceType)
  {
    String prefix = null;

    if (sourceType != null)
    {
      for (IAnnotationBinding at : sourceType.getAnnotations())
      {
        if (at.getAnnotationType().getQualifiedName().equals(Messages.class.getName()))
        {
          prefix = JdtDomUtils.getAnnotationElementValue(at, "prefix", String.class);
        }
      }

      if (prefix == null || prefix.equals(Messages.PREFIX_SOURCE_TYPE))
      {
        for (IAnnotationBinding at : sourceType.getAnnotations())
        {
          if (at.getAnnotationType().getQualifiedName().equals(Messages.class.getName()))
            continue;

          for (IAnnotationBinding atat : at.getAnnotationType().getAnnotations())
          {
            if (atat.getAnnotationType().getQualifiedName().equals(Messages.class.getName()))
            {
              prefix = JdtDomUtils.getAnnotationElementValue(atat, "prefix", String.class);
              if (!prefix.equals(Messages.PREFIX_SOURCE_TYPE))
                break;
            }
          }
        }
      }

      if (prefix == null || prefix.equals(Messages.PREFIX_SOURCE_TYPE))
        prefix = sourceType.getQualifiedName() + ".";
    }

    return Optional.ofNullable(prefix);
  }

  private static String getMessageKeyFromNode(ASTNode astNode, String localSuffix)
  {
    IAnnotationBinding annotationBinding = null;
    if (astNode instanceof Annotation)
      annotationBinding = ((Annotation) astNode).resolveAnnotationBinding();

    while (astNode != null && !(astNode instanceof Annotation))
      astNode = astNode.getParent();

    if (astNode == null)
      return null;


    IBinding binding = null;

    astNode = astNode.getParent();
    if (astNode.getNodeType() == ASTNode.METHOD_DECLARATION)
    {
      binding = ((MethodDeclaration) astNode).resolveBinding();
    }
    else if (astNode.getNodeType() == ASTNode.FIELD_DECLARATION)
    {
      FieldDeclaration fieldDeclaration = (FieldDeclaration) astNode;
      VariableDeclarationFragment variableDeclarationFragment =
          (VariableDeclarationFragment) fieldDeclaration.fragments().get(0);
      binding = variableDeclarationFragment.resolveBinding();
    }
    else if (astNode.getNodeType() == ASTNode.TYPE_DECLARATION)
    {
      binding = ((TypeDeclaration) astNode).resolveBinding();
    }
    else if (astNode.getNodeType() == ASTNode.ENUM_CONSTANT_DECLARATION)
    {
      EnumConstantDeclaration declaration = (EnumConstantDeclaration) astNode;
      binding = declaration.resolveVariable();
    }
    else if (astNode.getNodeType() == ASTNode.ENUM_DECLARATION)
    {
      EnumDeclaration declaration = (EnumDeclaration) astNode;
      binding = declaration.resolveBinding();
    }

    if (binding == null)
      return null;

    return getMessageKeyFromBinding(binding, localSuffix, annotationBinding);
  }

  private static String getMessageKeyFromBinding(IBinding binding, String localSuffix,
      IAnnotationBinding annotationBinding)
  {
    IAnnotationBinding messagesAt =
        getMessagesAnnotationFromBinding(annotationBinding != null ? annotationBinding : binding);
    if (messagesAt == null)
    {
      // quick assist on fields without @Messages annotation, where type has forProperties = true
      if (binding instanceof IVariableBinding)
      {
        IVariableBinding variableBinding = (IVariableBinding) binding;
        String prefix = getPrefixFromSourceType(variableBinding.getDeclaringClass()).orElse(null);
        if (prefix != null)
        {
          return MessageKeyUtils.getKeyFor(prefix, binding.getName(), null);
        }
      }
      return null;
    }


    String messageKey = null;

    if (localSuffix == null)
    {
      List<String> localSuffixes = new ArrayList<>();

      Object[] values = JdtDomUtils.getAnnotationElementValue(messagesAt, "value", Object[].class);
      if (values != null && values.length > 0)
      {
        for (Object value : values)
        {
          localSuffixes.add((String) value);
        }
      }

      values = JdtDomUtils.getAnnotationElementValue(messagesAt, "keys", Object[].class);
      if (values != null && values.length > 0)
      {
        for (Object value : values)
        {
          localSuffixes.add((String) value);
        }
      }

      /*
       * if no local suffix has been explicitly selected by user, just pick the first available one,
       * if any
       */
      if (!localSuffixes.isEmpty())
        localSuffix = localSuffixes.get(0);
    }

    ITypeBinding declaringClass = null;
    String localPrefix = null;

    if (binding.getKind() == IBinding.VARIABLE)
    {
      IVariableBinding variableBinding = (IVariableBinding) binding;

      declaringClass = variableBinding.getDeclaringClass();
      localPrefix = variableBinding.getName();
    }
    else if (binding.getKind() == IBinding.METHOD)
    {
      IMethodBinding methodBinding = (IMethodBinding) binding;

      declaringClass = methodBinding.getDeclaringClass();
    }
    else if (binding.getKind() == IBinding.TYPE)
    {
      declaringClass = (ITypeBinding) binding;
    }

    if (declaringClass != null)
    {
      String prefix = getPrefixFromSourceType(declaringClass).orElse(null);
      if (prefix != null)
      {
        // cut off message arguments
        localSuffix = trimToEmpty(substringBefore(substringBefore(localSuffix, "->"), ","));
        messageKey = MessageKeyUtils.getKeyFor(prefix, localPrefix, localSuffix);
      }
    }

    return messageKey;
  }

  private static IAnnotationBinding getMessagesAnnotationFromBinding(IBinding binding)
  {
    IAnnotationBinding[] annotations;

    if (binding instanceof IAnnotationBinding)
      annotations = new IAnnotationBinding[] {(IAnnotationBinding) binding};
    else
      annotations = binding.getAnnotations();

    for (IAnnotationBinding annotation : annotations)
    {
      if (annotation.getAnnotationType().getQualifiedName().equals(Messages.class.getName()))
      {
        return annotation;
      }
      else
      {
        for (IAnnotationBinding metaAnnotation : annotation.getAnnotationType().getAnnotations())
        {
          if (metaAnnotation.getAnnotationType().getQualifiedName()
              .equals(Messages.class.getName()))
          {
            return metaAnnotation;
          }
        }
      }
    }
    return null;
  }

}
