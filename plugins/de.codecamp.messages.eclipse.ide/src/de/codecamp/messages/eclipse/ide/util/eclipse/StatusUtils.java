package de.codecamp.messages.eclipse.ide.util.eclipse;


import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Map;
import java.util.WeakHashMap;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ui.statushandlers.IStatusAdapterConstants;
import org.eclipse.ui.statushandlers.StatusAdapter;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;


/**
 * Provides utility methods related to {@link IStatus}. They make logging and/or showing infos,
 * warnings and errors using the {@link StatusManager} much easier.
 * <p>
 * Across all methods, the last format argument can be a throwable and will be handled accordingly.
 * Messages and their arguments will be formated using {@link MessageFormat}.
 */
public final class StatusUtils
{

  private static final int SHOW_STYLE = StatusManager.SHOW | StatusManager.LOG;

  private static final int LOG_STYLE = StatusManager.LOG;

  private static final Map<Class<?>, String> CACHED_PLUGIN_IDS = new WeakHashMap<>();


  private StatusUtils()
  {
    // utility class
  }


  public static void logInfo(@NonNull Class<?> callingClass, @NonNull String message,
      Object... formatArgs)
  {
    log(IStatus.INFO, getPluginId(callingClass), message, formatArgs);
  }

  public static void logWarning(@NonNull Class<?> callingClass, @NonNull String message,
      Object... formatArgs)
  {
    log(IStatus.WARNING, getPluginId(callingClass), message, formatArgs);
  }

  public static void logError(@NonNull Class<?> callingClass, @NonNull String message,
      Object... formatArgs)
  {
    log(IStatus.ERROR, getPluginId(callingClass), message, formatArgs);
  }

  private static void log(int severity, @NonNull String pluginId, @NonNull String message,
      Object... formatArgs)
  {
    try
    {
      message = MessageFormat.format(message, formatArgs);
    }
    catch (IllegalArgumentException ex)
    {
      message = message + " " + Arrays.asList(formatArgs);
      log(IStatus.ERROR, pluginId, "Failed to format message.", ex);
    }

    Throwable ex = null;
    if (formatArgs != null && formatArgs.length > 0
        && formatArgs[formatArgs.length - 1] instanceof Throwable)
    {
      ex = (Throwable) formatArgs[formatArgs.length - 1];
    }

    IStatus status = new Status(severity, pluginId, message, ex);
    // Platform.getLog(FrameworkUtil.getBundle(callingClass)).log(status);
    StatusManager.getManager().handle(status, LOG_STYLE);
  }


  public static void showInfo(@NonNull Class<?> callingClass, String title, @NonNull String message,
      Object... formatArgs)
  {
    show(IStatus.INFO, false, getPluginId(callingClass), title, message, formatArgs);
  }

  public static void showWarning(@NonNull Class<?> callingClass, String title,
      @NonNull String message, Object... formatArgs)
  {
    show(IStatus.WARNING, false, getPluginId(callingClass), title, message, formatArgs);
  }

  public static void showError(@NonNull Class<?> callingClass, String title,
      @NonNull String message, Object... formatArgs)
  {
    show(IStatus.ERROR, false, getPluginId(callingClass), title, message, formatArgs);
  }

  public static void showBlockingInfo(@NonNull Class<?> callingClass, String title,
      @NonNull String message, Object... formatArgs)
  {
    show(IStatus.INFO, true, getPluginId(callingClass), title, message, formatArgs);
  }

  public static void showBlockingWarning(@NonNull Class<?> callingClass, String title,
      @NonNull String message, Object... formatArgs)
  {
    show(IStatus.WARNING, true, getPluginId(callingClass), title, message, formatArgs);
  }

  public static void showBlockingError(@NonNull Class<?> callingClass, String title,
      @NonNull String message, Object... formatArgs)
  {
    show(IStatus.ERROR, true, getPluginId(callingClass), title, message, formatArgs);
  }

  private static void show(int severity, boolean blocking, @NonNull String pluginId, String title,
      @NonNull String message, Object... formatArgs)
  {
    try
    {
      message = MessageFormat.format(message, formatArgs);
    }
    catch (IllegalArgumentException ex)
    {
      message = message + " " + Arrays.asList(formatArgs);
      log(IStatus.ERROR, pluginId, "Failed to format message.", ex);
    }

    Throwable ex = null;
    if (formatArgs != null && formatArgs.length > 0
        && formatArgs[formatArgs.length - 1] instanceof Throwable)
    {
      ex = (Throwable) formatArgs[formatArgs.length - 1];
    }

    IStatus status = new Status(severity, pluginId, message, ex);
    StatusAdapter adapter = new StatusAdapter(status);
    adapter.setProperty(IStatusAdapterConstants.TITLE_PROPERTY, title);
    adapter.setProperty(IStatusAdapterConstants.TIMESTAMP_PROPERTY, System.currentTimeMillis());
    StatusManager.getManager().handle(adapter, SHOW_STYLE | (blocking ? StatusManager.BLOCK : 0));
  }


  public static IStatus createError(@NonNull Class<?> callingClass, @NonNull String message,
      Object... formatArgs)
  {
    message = MessageFormat.format(message, formatArgs);
    Throwable ex = null;
    if (formatArgs != null && formatArgs.length > 0
        && formatArgs[formatArgs.length - 1] instanceof Throwable)
    {
      ex = (Throwable) formatArgs[formatArgs.length - 1];
    }

    return new Status(IStatus.ERROR, getPluginId(callingClass), message, ex);
  }


  private static String getPluginId(@NonNull Class<?> callingClass)
  {
    return CACHED_PLUGIN_IDS.computeIfAbsent(callingClass, cc ->
    {
      Bundle bundle = FrameworkUtil.getBundle(cc);
      if (bundle == null)
      {
        String msg = "Calling class %s has not been loaded by a plug-in.";
        msg = String.format(msg, cc.getName());
        throw new IllegalArgumentException(msg);
      }
      return bundle.getSymbolicName();
    });
  }

}
