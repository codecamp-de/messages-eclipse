package de.codecamp.messages.eclipse.ide.validation;


import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.shared.bundle.BundleFile;
import java.util.Locale;
import java.util.Set;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;


public class MessageValidationTask
  implements
    BundleFileValidationTask
{

  private final MessageKeyService messageKeyService;


  public MessageValidationTask(MessageKeyService messageKeyService)
  {
    this.messageKeyService = messageKeyService;
  }


  @Override
  public void validate(ValidationResultAdapter result, BundleFileValidationContext context,
      BundleFile<IFile, CoreException> bundleFile, BundleFile<IFile, CoreException> rootBundleFile)
  {
    for (IdeMessageKey messageKey : context.getDeclaredMessageKeys().values())
    {
      Set<Locale> requiredLocales = messageKeyService.getRequiredLocales(messageKey).orElse(null);
      if (requiredLocales == null)
        continue;

      if (!bundleFile.getBundleName()
          .equals(context.getProjectConf().toTargetBundleName(messageKey.getCode()).orElse(null)))
      {
        continue;
      }


      boolean localDefaultMessageAvailable =
          rootBundleFile != null && rootBundleFile.hasMessageKey(messageKey.getCode());

      boolean requiredInBundleFile = requiredLocales.contains(bundleFile.getLocale())
          && !messageKey.isDefaultMessageAvailable() && !localDefaultMessageAvailable;

      if (bundleFile.isMessageEmpty(messageKey.getCode()))
      {
        if (requiredInBundleFile)
        {
          String msg = "Message is missing.";
          result.addBundleWarning(bundleFile, messageKey.getCode(), messageKey.getSourceType(),
              msg);
        }
        else if (bundleFile.hasMessageKey(messageKey.getCode()))
        {
          /* A message key with empty message suggests that a message could be desired. */
          String msg =
              "Message is missing. Message not required, but found key with empty message.";
          result.addBundleWarning(bundleFile, messageKey.getCode(), messageKey.getSourceType(),
              msg);
        }
      }
    }
  }

}
