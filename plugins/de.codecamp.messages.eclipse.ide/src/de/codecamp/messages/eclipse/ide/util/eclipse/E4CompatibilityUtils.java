package de.codecamp.messages.eclipse.ide.util.eclipse;


import org.eclipse.core.runtime.Adapters;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.ui.internal.e4.compatibility.CompatibilityPart;


@SuppressWarnings("restriction")
public final class E4CompatibilityUtils
{

  private E4CompatibilityUtils()
  {
    // utility class
  }


  public static <T> T adapt(MPart part, Class<T> adapter)
  {
    Object sourceObject = part.getObject();

    if (sourceObject instanceof CompatibilityPart)
      sourceObject = ((CompatibilityPart) sourceObject).getPart();

    return Adapters.adapt(sourceObject, adapter);
  }

  public static Object getPartObject(MPart part)
  {
    Object object = null;
    if (part != null)
    {
      object = part.getObject();
      if (object instanceof CompatibilityPart)
        object = ((CompatibilityPart) object).getPart();
    }
    return object;
  }

}
