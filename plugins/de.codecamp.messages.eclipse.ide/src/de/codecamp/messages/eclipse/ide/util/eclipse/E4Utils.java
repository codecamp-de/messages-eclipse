package de.codecamp.messages.eclipse.ide.util.eclipse;


import javax.inject.Inject;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;


public final class E4Utils
{

  private E4Utils()
  {
    // utility class
  }


  /**
   * Applies E4-style dependency injection on classes that otherwise wouldn't support it, like
   * everything created through an extension point. Use {@link Inject} as usual, call this method in
   * the constructor and hope for the best.
   *
   * @param object
   *          the object to apply DI on
   */
  public static void injectWorkbenchContext(Object object)
  {
    ContextInjectionFactory.inject(object, getWorkbenchContext());
  }

  public static IEclipseContext getWorkbenchContext()
  {
    return getEclipseContext();
  }

  public static <S> S getWorkbenchService(Class<S> serviceInterface)
  {
    return getWorkbenchContext().get(serviceInterface);
  }


  public static void injectOsgiContext(Object object)
  {
    ContextInjectionFactory.inject(object, getOsgiContext());
  }

  public static IEclipseContext getOsgiContext()
  {
    BundleContext bundleContext = FrameworkUtil.getBundle(IWorkbench.class).getBundleContext();
    return EclipseContextFactory.getServiceContext(bundleContext);
  }


  private static IEclipseContext getEclipseContext()
  {
    BundleContext bundleContext = FrameworkUtil.getBundle(IWorkbench.class).getBundleContext();
    IEclipseContext bundleEclipseContext = EclipseContextFactory.getServiceContext(bundleContext);
    IWorkbench workbench = bundleEclipseContext.get(IWorkbench.class);

    if (workbench == null)
      throw new IllegalStateException("Workbench not yet available.");

    return workbench.getApplication().getContext();
  }

}
