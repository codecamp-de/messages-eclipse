package de.codecamp.messages.eclipse.ide.validation;


import static java.util.stream.Collectors.toMap;

import de.codecamp.messages.eclipse.ide.MessagesPlugin;
import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.AbstractResourceValidator;
import de.codecamp.messages.eclipse.ide.util.eclipse.E4Utils;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import de.codecamp.messages.eclipse.ide.validation.BundleFileValidationTask.ValidationResultAdapter;
import de.codecamp.messages.shared.bundle.BundleFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.inject.Inject;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.wst.validation.ValidationEvent;
import org.eclipse.wst.validation.ValidationResult;
import org.eclipse.wst.validation.ValidationState;
import org.eclipse.wst.validation.ValidatorMessage;


/**
 * Checks message bundle files to add useful markers within Eclipse. They're intended to replace the
 * equivalent warnings in the Java source files produced by the annotation processor.
 */
public class BundleFileValidator
  extends
    AbstractResourceValidator
{

  public static final String ID = MessagesPlugin.getPluginId() + ".validator.project";

  public static final String MARKER_ID_BUNDLE_PROBLEM =
      "de.codecamp.messages.eclipse.ide.problem.bundle";

  public static final String MARKER_ATTR_LOCALE = "locale";

  public static final String MARKER_ATTR_SOURCE_TYPE = "sourceType";

  public static final String MARKER_ATTR_MESSAGE_KEY = "messageKey";


  @Inject
  private MessageKeyService messageKeyService;


  public BundleFileValidator()
  {
    E4Utils.injectOsgiContext(this);
  }


  @Override
  protected String getMarkerId()
  {
    return MARKER_ID_BUNDLE_PROBLEM;
  }


  @Override
  public void validationStarting(IProject project, ValidationState state, IProgressMonitor monitor)
  {
    super.validationStarting(project, state, monitor);

    if (project == null)
      return;

    IdeProjectConf projectConf = messageKeyService.getProjectConf(project, true).orElse(null);
    if (projectConf == null || !projectConf.getBundleDirAsFolder().isPresent())
      return;

    IJavaProject javaProject = JavaCore.create(project);

    EclipseMessageBundleManager bundleManager = projectConf.createEclipseMessageBundleManager();

    Map<String, IdeMessageKey> declaredMessageKeys =
        messageKeyService.getMessageKeys(project, true, true, null, false)
            .collect(toMap(IdeMessageKey::getCode, k -> k));

    BundleFileValidationContext context = new BundleFileValidationContext(projectConf, javaProject,
        bundleManager, declaredMessageKeys);

    state.put(ID, context);
  }

  @Override
  public ValidationResult validate(ValidationEvent event, ValidationState state,
      IProgressMonitor monitor)
  {
    ValidationResult result = new ValidationResult();

    BundleFileValidationContext context = (BundleFileValidationContext) state.get(ID);
    if (context == null)
      return result;

    IdeProjectConf projectConf = context.getProjectConf();
    EclipseMessageBundleManager bundleManager = context.getMessageBundleManager();


    IResource resource = event.getResource();

    IProject project = resource.getProject();

    IFolder bundleDir = projectConf.getBundleDirAsFolder().orElse(null);
    if (bundleDir == null || !bundleDir.exists())
    {
      StatusUtils.logWarning(getClass(),
          "Could not find message directory ''{0}'' of project ''{1}'' in the workspace.",
          projectConf.getBundleDir(), project.getName());
      return result;
    }

    if (!projectConf.isMessageBundleFile(resource))
      return result;

    IFile bundleFileResource = (IFile) resource;

    BundleFile<IFile, CoreException> bundleFile = bundleManager.getBundleFileAt(bundleFileResource);
    if (bundleFile == null)
    {
      StatusUtils.logWarning(getClass(),
          "Message bundle file ''{0}'' not found even though it seems to be in the workspace. Try"
              + " to refresh your workspace.",
          bundleFileResource.getFullPath());
      return result;
    }

    // skip ignored bundles
    if (projectConf.getIgnoredBundles().contains(bundleFile.getBundleName()))
      return result;

    if (!bundleFile.getLocale().equals(Locale.ROOT)
        && !projectConf.getTargetLocales().contains(bundleFile.getLocale()))
    {
      addWarningMessage(result, bundleFileResource,
          "Bundle file locale ''{0}'' not part of the target locales.", bundleFile.getLocale());
    }



    BundleFile<IFile, CoreException> rootBundleFile = null;
    if (!bundleFile.getLocale().equals(Locale.ROOT))
    {
      rootBundleFile = bundleManager.getBundleFile(bundleFile.getBundleName(), Locale.ROOT);
    }

    ValidationResultAdapter resultAdapter = new ValidationResultAdapter()
    {

      @Override
      public void addBundleWarning(BundleFile<IFile, CoreException> bundleFile, String messageKey,
          String sourceType, String msg)
      {
        addBundleMessage(result, IMarker.SEVERITY_WARNING, bundleFile, messageKey, sourceType, msg);
      }

      @Override
      public void addBundleError(BundleFile<IFile, CoreException> bundleFile, String messageKey,
          String sourceType, String msg)
      {
        addBundleMessage(result, IMarker.SEVERITY_ERROR, bundleFile, messageKey, sourceType, msg);
      }

    };


    List<BundleFileValidationTask> validations = new ArrayList<>();
    validations.add(new UndeclaredKeysValidationTask());
    validations.add(new BundleMismatchValidationTask());
    validations.add(new MessageValidationTask(messageKeyService));
    validations.add(new MessageArgValidationTask());

    for (BundleFileValidationTask validation : validations)
    {
      validation.validate(resultAdapter, context, bundleFile, rootBundleFile);
    }

    if (rootBundleFile != null)
      result.setDependsOn(new IResource[] {rootBundleFile.getLocation()});

    return result;
  }

  private void addBundleMessage(ValidationResult result, int severity,
      BundleFile<IFile, CoreException> bundleFile, String messageKey, String sourceType, String msg)
  {
    String localeText;
    if (Locale.ROOT.equals(bundleFile.getLocale()))
      localeText = "Base";
    else
      localeText = bundleFile.getLocale().toString();
    Integer lineNumber = bundleFile.findLineNumberOfKey(messageKey);
    ValidatorMessage vm = addMessage(result, severity, bundleFile.getLocation(), lineNumber, msg,
        messageKey, localeText, bundleFile.getDisplayPath());
    vm.setAttribute(MARKER_ATTR_LOCALE, bundleFile.getLocale().toString());
    vm.setAttribute(MARKER_ATTR_SOURCE_TYPE, sourceType);
    vm.setAttribute(MARKER_ATTR_MESSAGE_KEY, messageKey);
  }

}
