package de.codecamp.messages.eclipse.ide.util.jdt;


import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceStatus;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;


/**
 * Finds and loads specific resources from a given Java project and all its transitive dependencies.
 */
public final class JavaProjectResourceLoader
{

  private JavaProjectResourceLoader()
  {
    // utility class
  }


  /**
   * Finds the first encountered resource with the given name on the classpath of the given project
   * and passes an input stream for it to the given consumer.
   *
   * @param javaProject
   *          the Java project to search
   * @param resourceName
   *          the resource name (incl. its path) to look for
   * @param streamConsumer
   *          the consumer of the resource input stream; might never be called
   */
  public static void loadResource(IJavaProject javaProject, String resourceName,
      BiConsumer<Object, InputStream> streamConsumer)
  {
    loadResources(javaProject, resourceName, (source, is) ->
    {
      streamConsumer.accept(source, is);
      return false;
    });
  }

  /**
   * Finds all resources with the given name on the classpath of the given project and passes an
   * input stream for it to the given consumer function. That consumer function should return false
   * if it is not interested in any more resources.
   *
   * @param javaProject
   *          the Java project to search
   * @param resourceName
   *          the resource name (incl. its path) to look for
   * @param streamConsumer
   *          the consumer of the resource input streams; might never be called; should return true
   *          to continue searching or false to stop
   */
  public static void loadResources(IJavaProject javaProject, String resourceName,
      BiFunction<Object, InputStream, Boolean> streamConsumer)
  {
    loadFromWorkspaceProject(javaProject, resourceName, streamConsumer);
  }

  private static boolean loadFromJar(File file, String resourceName,
      BiFunction<Object, InputStream, Boolean> streamConsumer)
  {
    boolean kontinue = true;

    try (JarFile jarFile = new JarFile(file))
    {
      ZipEntry entry = jarFile.getEntry(resourceName);
      if (entry != null)
      {
        try (InputStream is = jarFile.getInputStream(entry))
        {
          kontinue = streamConsumer.apply(file, is);
        }
      }
    }
    catch (IOException | RuntimeException ex)
    {
      StatusUtils.logError(JavaProjectResourceLoader.class,
          "Failed to load resource ''{0}'' from JAR file ''{1}''.", resourceName, file, ex);
    }

    return kontinue;
  }

  private static boolean loadFromWorkspaceProject(IJavaProject javaProject, String resourceName,
      BiFunction<Object, InputStream, Boolean> streamConsumer)
  {
    try
    {
      IClasspathEntry[] classpath = javaProject.getResolvedClasspath(true);
      for (IClasspathEntry entry : classpath)
      {
        if (entry.getEntryKind() == IClasspathEntry.CPE_LIBRARY
            && entry.getContentKind() == IPackageFragmentRoot.K_BINARY)
        {
          File file = entry.getPath().toFile();
          if (file.isFile() && file.toString().toLowerCase(Locale.ENGLISH).endsWith(".jar"))
          {
            boolean kontinue = loadFromJar(file, resourceName, streamConsumer);
            if (!kontinue)
              return false;
          }
        }
        else if (entry.getEntryKind() == IClasspathEntry.CPE_PROJECT)
        {
          String projectName = entry.getPath().segment(0);
          IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
          try
          {
            if (projectName != null)
            {
              if (project.isAccessible() && project.hasNature(JavaCore.NATURE_ID))
              {
                boolean kontinue = loadFromWorkspaceProject(JavaCore.create(project), resourceName,
                    streamConsumer);
                if (!kontinue)
                  return false;
              }
            }
          }
          catch (CoreException | RuntimeException ex)
          {
            StatusUtils.logError(JavaProjectResourceLoader.class,
                "Failed to load resource ''{0}'' from project dependency ''{1}''.", resourceName,
                projectName, ex);
          }
        }
      }
    }
    catch (JavaModelException ex)
    {
      StatusUtils.logError(JavaProjectResourceLoader.class,
          "Failed to get resolved classpath of project ''{0}''.",
          javaProject.getProject().getName(), ex);
    }

    return loadFromOutputFolder(javaProject, resourceName, streamConsumer);
  }

  private static boolean loadFromOutputFolder(IJavaProject javaProject, String resourceName,
      BiFunction<Object, InputStream, Boolean> streamConsumer)
  {
    boolean kontinue = true;
    try
    {
      IPath outputLocation = javaProject.getOutputLocation();
      if (outputLocation != null)
      {
        IFile resourceFile = ResourcesPlugin.getWorkspace().getRoot().getFolder(outputLocation)
            .getFile(resourceName);
        if (resourceFile.exists())
        {
          try (InputStream is = resourceFile.getContents(true))
          {
            kontinue = streamConsumer.apply(javaProject.getProject(), is);
          }
        }
      }
    }
    catch (CoreException ex)
    {
      if (ex.getStatus().getCode() != IResourceStatus.RESOURCE_NOT_FOUND)
      {
        StatusUtils.logError(JavaProjectResourceLoader.class,
            "Failed to load resource ''{0}'' from output location of project ''{1}''.",
            resourceName, javaProject.getProject().getName(), ex);
      }
    }
    catch (IOException | RuntimeException ex)
    {
      StatusUtils.logError(JavaProjectResourceLoader.class,
          "Failed to load resource ''{0}'' from output location of project ''{1}''.", resourceName,
          javaProject.getProject().getName(), ex);
    }
    return kontinue;
  }

}
