package de.codecamp.messages.eclipse.ide.build;


import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import de.codecamp.messages.eclipse.ide.actions.CleanUpBundleFileAction;
import de.codecamp.messages.eclipse.ide.actions.OpenKeysRemovedDialogAction;
import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyEvent;
import de.codecamp.messages.eclipse.ide.services.MessageKeyEvent.Type;
import de.codecamp.messages.eclipse.ide.services.MessageKeyListener;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.StatusUtils;
import de.codecamp.messages.shared.bundle.BundleFile;
import de.codecamp.messages.shared.conf.ProjectConf;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.wst.validation.ValidationFramework;


public class MessageKeyChangeHandler
  implements
    MessageKeyListener
{

  private final MessageKeyService messageKeyService;


  public MessageKeyChangeHandler(MessageKeyService messageKeyService)
  {
    this.messageKeyService = messageKeyService;
  }


  @Override
  public void onUpdate(MessageKeyEvent event)
  {
    IProject project = event.getProject();

    IdeProjectConf projectConf = messageKeyService.getProjectConf(project).orElse(null);
    if (projectConf == null)
      return;

    WorkspaceJob job = new WorkspaceJob(
        String.format("Processing message key changes in '%s'.", project.getName()))
    {

      @Override
      public IStatus runInWorkspace(IProgressMonitor monitor)
        throws CoreException
      {
        IProject project = event.getProject();

        IdeProjectConf projectConf = messageKeyService.getProjectConf(project, true).orElse(null);
        if (projectConf == null)
          return Status.OK_STATUS;

        if (!projectConf.getBundleDirAsFolder().isPresent())
          return Status.OK_STATUS;

        EclipseMessageBundleManager bundleManager = projectConf.createEclipseMessageBundleManager();

        CleanUpBundleFileAction cleanUpAction = new CleanUpBundleFileAction(messageKeyService);

        if (event.getType() == Type.PROJECT_ADDED || event.getType() == Type.PROJECT_REFRESHED)
        {
          cleanUpAction.execute(projectConf, bundleManager, null);
          bundleManager.save();
        }

        else if (event.getType() == Type.MESSAGE_KEYS_UPDATED)
        {
          cleanUpAction.execute(projectConf, bundleManager, event.getAddedKeys().stream());


          Set<IdeMessageKey> removedKeys = new HashSet<>(event.getRemovedKeys());

          for (Iterator<IdeMessageKey> it = removedKeys.iterator(); it.hasNext();)
          {
            IdeMessageKey messageKey = it.next();

            boolean messageFound = false;
            for (BundleFile<IFile, CoreException> bundleFile : bundleManager.getBundleFiles())
            {
              if (bundleFile.hasMessageKey(messageKey.getCode()))
              {
                if (bundleFile.isMessageEmpty(messageKey.getCode()))
                  bundleFile.removeMessage(messageKey.getCode());
                else
                  messageFound = true;
              }
            }

            if (!messageFound)
            {
              it.remove();
            }
          }

          if (!removedKeys.isEmpty())
          {
            OpenKeysRemovedDialogAction openKeysRemovedDialogAction =
                new OpenKeysRemovedDialogAction(messageKeyService);
            openKeysRemovedDialogAction.execute(project,
                event.getRemovedKeys().stream()
                    .collect(toMap(IdeMessageKey::getCode, IdeMessageKey::getSourceType)),
                event.getAddedKeys().stream().collect(
                    toMap(IdeMessageKey::getCode, IdeMessageKey::getSourceType)),
                bundleManager);
          }
          bundleManager.save();

          if (!event.getAddedKeys().isEmpty() || !event.getRemovedKeys().isEmpty())
          {
            validateBundleFilesFor(projectConf, bundleManager,
                Stream.concat(event.getAddedKeys().stream(), event.getRemovedKeys().stream()));
          }
        }

        return Status.OK_STATUS;
      }

    };
    job.setRule(project);
    job.schedule();
  }

  /**
   * Triggers validation on all message bundle files that are the target bundles for any of the
   * provided message keys. This ensures that changes to the message keys that ultimately don't
   * result in changes to the associated bundle files still trigger their validation.
   *
   * @param projectConf
   *          the project configuration
   * @param bundleManager
   *          the bundle manager
   * @param keyStream
   *          the keys to validate
   */
  protected void validateBundleFilesFor(ProjectConf projectConf,
      EclipseMessageBundleManager bundleManager, Stream<IdeMessageKey> keyStream)
  {
    Set<String> bundleNames = new HashSet<>();
    keyStream.forEach(key ->
    {
      projectConf.toTargetBundleName(key.getCode()).ifPresent(bundleNames::add);
    });
    Set<IFile> bundleFiles = bundleNames.stream().map(bundleManager::getBundleFiles)
        .flatMap(Set::stream).map(BundleFile<IFile, CoreException>::getLocation).collect(toSet());

    bundleFiles.forEach(file ->
    {
      try
      {
        ValidationFramework.getDefault().validate(file, null);
      }
      catch (CoreException ex)
      {
        StatusUtils.logError(getClass(),
            "Failed to trigger validation for message bundle file ''{0}''.", file, ex);
      }
    });
  }

}
