package de.codecamp.messages.eclipse.ide.ui;


import static com.googlecode.cqengine.query.QueryFactory.and;
import static com.googlecode.cqengine.query.QueryFactory.applyThresholds;
import static com.googlecode.cqengine.query.QueryFactory.attribute;
import static com.googlecode.cqengine.query.QueryFactory.equal;
import static com.googlecode.cqengine.query.QueryFactory.queryOptions;
import static com.googlecode.cqengine.query.QueryFactory.threshold;
import static de.codecamp.messages.eclipse.ide.ui.Constants.STATE_GROUP_BY_MODULES_DEFAULT;
import static de.codecamp.messages.eclipse.ide.ui.Constants.STATE_GROUP_BY_SOURCE_TYPE_DEFAULT;
import static de.codecamp.messages.eclipse.ide.ui.Constants.STATE_SHOW_IMPORTED_KEYS_DEFAULT;
import static de.codecamp.messages.eclipse.ide.ui.Constants.STATE_SHOW_ONLY_KEYS_WITH_PROBLEMS_DEFAULT;
import static de.codecamp.messages.eclipse.ide.ui.Constants.VIEWSTATE_GROUP_BY_MODULES;
import static de.codecamp.messages.eclipse.ide.ui.Constants.VIEWSTATE_GROUP_BY_SOURCE_TYPE;
import static de.codecamp.messages.eclipse.ide.ui.Constants.VIEWSTATE_SHOW_IMPORTED_KEYS;
import static de.codecamp.messages.eclipse.ide.ui.Constants.VIEWSTATE_SHOW_ONLY_KEYS_WITH_PROBLEMS;

import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.index.hash.HashIndex;
import com.googlecode.cqengine.index.navigable.NavigableIndex;
import com.googlecode.cqengine.query.Query;
import com.googlecode.cqengine.query.option.EngineThresholds;
import com.googlecode.cqengine.query.option.QueryOptions;
import de.codecamp.messages.eclipse.ide.MessagesPlugin;
import de.codecamp.messages.eclipse.ide.conf.EclipseMessageBundleManager;
import de.codecamp.messages.eclipse.ide.conf.IdeProjectConf;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.MessageKeyEvent;
import de.codecamp.messages.eclipse.ide.services.MessageKeyEvent.Type;
import de.codecamp.messages.eclipse.ide.services.MessageKeyListener;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.swt.EventDelay;
import de.codecamp.messages.shared.bundle.BundleFile;
import de.codecamp.messages.shared.conf.ProjectConfException;
import de.codecamp.messages.shared.messageformat.MessageFormatSupport;
import de.codecamp.messages.shared.messageformat.MessageFormatSupport.ArgInsert;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.BoldStylerProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.menus.IMenuService;
import org.eclipse.ui.part.EditorPart;


public class MessageBundleEditor
  extends
    EditorPart
{

  public static final String ID = "de.codecamp.messages.eclipse.ide.messageBundleEditor";

  public static final String EMPTY_MESSAGE = "\u200B";


  private static final Attribute<IdeMessage, Locale> MESSAGE_ATTR_LOCALE =
      attribute(IdeMessage.class, Locale.class, "locale", IdeMessage::getLocale);

  private static final Attribute<IdeMessage, String> MESSAGE_ATTR_MESSAGE_KEY_CODE =
      attribute(IdeMessage.class, String.class, "messageKeyCode", IdeMessage::getCode);

  private static final Attribute<IdeMessage, String> MESSAGE_ATTR_MESSAGE =
      attribute(IdeMessage.class, String.class, "message", IdeMessage::getMessage);


  /**
   * used to store the original message on a message field
   */
  private static final String DATA_ORIGINAL_MESSAGE = "originalMessage";

  /**
   * used to mark the key of a message field for deletion in the respective bundle file
   */
  private static final String DATA_REMOVE = "REMOVE";

  private static final String DATA_REQUIRED = "REQUIRED";

  private static final String DATA_LINE_COUNT = "LINE_COUNT";


  private IEclipseContext context;

  @Inject
  private MessageKeyService messageKeyService;

  private MessageKeyListener messageKeyListener;


  private ToolBarManager toolbarManager;

  private TreeViewer treeViewer;

  private MessageKeyTreeContentProvider treeViewerContentProvider;

  private final Map<Locale, StyledText> messageLabels = new HashMap<>();

  private final Map<Locale, Text> messageFields = new HashMap<>();

  private final Map<Locale, Button> messageEmptyCheckbox = new HashMap<>();

  private final Map<Locale, IAction> messageResetAction = new HashMap<>();

  private final Map<Locale, IAction> messageRemoveKeyAction = new HashMap<>();

  private final Map<Locale, IAction> messageArgMenu = new HashMap<>();

  private final Map<Locale, MenuManager> messageArgToolbar = new HashMap<>();

  private Font defaultFont;

  private BoldStylerProvider defaultBoldStylerProvider;


  // message key code -> locale -> message
  private final Map<String, Map<Locale, String>> modifiedMessages = new HashMap<>();


  private final IndexedCollection<IdeMessage> messages = new ConcurrentIndexedCollection<>();

  private Text searchBarText;

  private EventDelay searchBarTextEventDelay;

  private StyledText messageArgsInfo;

  private Composite messageFieldArea;

  private ScrolledComposite messageAreaScroll;


  private IdeMessageKey selectedKey;

  private Locale selectedLocale;

  private Point selectedTextLocation;


  public MessageBundleEditor()
  {
    messages.addIndex(HashIndex.onAttribute(MESSAGE_ATTR_LOCALE));
    messages.addIndex(NavigableIndex.onAttribute(MESSAGE_ATTR_MESSAGE_KEY_CODE));
    messages.addIndex(NavigableIndex.onAttribute(MESSAGE_ATTR_MESSAGE));
  }


  @Override
  public void init(IEditorSite site, IEditorInput input)
    throws PartInitException
  {
    if (!(input instanceof IProjectEditorInput || input instanceof IFileEditorInput))
      throw new PartInitException(input.getClass().getName() + " not supported");

    context = site.getService(IEclipseContext.class);
    ContextInjectionFactory.inject(this, context);

    setSite(site);
    setInput(input);

    try
    {
      if (!messageKeyService.getProjectConfChecked(getProjectFromInput()).isPresent())
        throw new PartInitException("No messages project configuration found.");
    }
    catch (ProjectConfException | CoreException ex)
    {
      throw new PartInitException("Messages project configuration could not be loaded.", ex);
    }

    setPartName(getProjectFromInput().getName());


    defaultFont = JFaceResources.getDialogFont();
    defaultBoldStylerProvider = new BoldStylerProvider(defaultFont);


    messageKeyListener = new MessageKeyListener()
    {

      @Override
      public void onUpdate(MessageKeyEvent event)
      {
        if (!getProjectFromInput().equals(event.getProject()))
          return;

        getSite().getShell().getDisplay().asyncExec(() ->
        {
          if (event.getType() == Type.PROBLEMS_UPDATED)
          {
            updateSelectedItem(event.getProject());
            /* seemed more reliable than refresh() */
            treeViewer.getTree().clearAll(true);
          }
          else
          {
            if (event.getType() == Type.PROJECT_REFRESHED)
              updateMessageAreaLocales();

            treeViewer.refresh();
            reloadMessageBundles();
          }

          IdeMessageKey selectedMessageKey = getSelectedMessageKey();
          updateMessageArea(selectedMessageKey);
          if (selectedMessageKey != null)
          {
            selectMessageKey(selectedMessageKey.getCode(), selectedLocale, selectedTextLocation);
          }
        });
      }

    };
    messageKeyService.addMessageKeyChangeListener(messageKeyListener);
  }

  /**
   * The {@link TreeViewer} seems to have issues replacing the {@link IdeMessageKey} associated with
   * a {@link org.eclipse.swt.widgets.TreeItem} (stored in
   * {@link org.eclipse.swt.widgets.TreeItem#getData()}) when it's currently contained in the
   * selection. But even clearing the selection before the refresh didn't help; if the new
   * IdeMessageKey is selected afterwards then the old IdeMessageKey ends up in the selection.
   * <p>
   * It's probably related to the fact that {@link Object#equals(Object)} reports
   * {@link IdeMessageKey} objects as equal even when their {@link IdeMessageKey#getArgNames()
   * arguments} have changed. Other attempts have failed; only calling
   * {@link TreeViewer#update(Object, String[])} seems to work so far.
   *
   * @param project
   *          the project
   */
  private void updateSelectedItem(IProject project)
  {
    IdeMessageKey selectedMessageKey = getSelectedMessageKey();

    if (selectedMessageKey != null)
    {
      IdeMessageKey freshMessageKey =
          messageKeyService.findMessageKey(project, selectedMessageKey.getCode(), true, false)
              .orElse(selectedMessageKey);
      treeViewer.update(freshMessageKey, null);
    }
  }

  @Override
  public void dispose()
  {
    super.dispose();

    if (defaultBoldStylerProvider != null)
      defaultBoldStylerProvider.dispose();

    if (messageKeyService != null && messageKeyListener != null)
      messageKeyService.removeMessageKeyChangeListener(messageKeyListener);

    if (toolbarManager != null)
    {
      getSite().getService(IMenuService.class).releaseContributions(toolbarManager);
      toolbarManager.dispose();
    }

    ContextInjectionFactory.uninject(this, context);

    saveState();
  }

  private IProject getProjectFromInput()
  {
    if (getEditorInput() instanceof IProjectEditorInput)
      return ((IProjectEditorInput) getEditorInput()).getProject();
    else if (getEditorInput() instanceof IFileEditorInput)
      return ((IFileEditorInput) getEditorInput()).getFile().getProject();
    else
      throw new IllegalStateException();
  }

  @Override
  public void createPartControl(Composite parent)
  {
    SashForm sashForm = new SashForm(parent, SWT.HORIZONTAL | SWT.SMOOTH);
    sashForm.setSashWidth(7);
    sashForm.setBackground(
        sashForm.getShell().getDisplay().getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));

    Composite treeArea = new Composite(sashForm, SWT.NONE);
    GridLayoutFactory.swtDefaults().applyTo(treeArea);
    treeArea.setBackground(
        treeArea.getShell().getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));

    ToolBar treeToolbar = new ToolBar(treeArea, SWT.FLAT);
    GridDataFactory.fillDefaults().align(SWT.RIGHT, SWT.CENTER).grab(true, false)
        .applyTo(treeToolbar);


    Composite searchBarContainer = new Composite(treeArea, SWT.BORDER);
    GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false)
        .applyTo(searchBarContainer);
    searchBarContainer.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));

    searchBarText = new Text(searchBarContainer, SWT.FLAT);
    GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, true)
        .applyTo(searchBarText);
    searchBarText.setMessage("Filter");
    searchBarText.addModifyListener(event ->
    {
      treeViewerContentProvider.setFilterText(searchBarText.getText());
    });

    Button searchBarClearButton = new Button(searchBarContainer, SWT.NONE);
    searchBarClearButton.setImage(
        PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_CLEAR));
    GridDataFactory.swtDefaults().grab(false, true).applyTo(searchBarClearButton);

    GridLayoutFactory.fillDefaults().numColumns(2).generateLayout(searchBarContainer);


    Composite treeContainer = new Composite(treeArea, SWT.NONE);
    GridDataFactory.fillDefaults().grab(true, true).applyTo(treeContainer);
    TreeColumnLayout treeColumnLayout = new TreeColumnLayout();
    treeContainer.setLayout(treeColumnLayout);

    treeViewer =
        new TreeViewer(treeContainer, SWT.BORDER | SWT.SINGLE | SWT.FULL_SELECTION | SWT.VIRTUAL);
    GridDataFactory.fillDefaults().grab(true, true).applyTo(treeViewer.getControl());
    ColumnViewerToolTipSupport.enableFor(treeViewer);
    treeViewerContentProvider = new MessageKeyTreeContentProvider(messageKeyService);
    treeViewerContentProvider.setGroupByModules(true);
    treeViewerContentProvider.setShowImportedKeys(true);
    treeViewer.setUseHashlookup(true);
    treeViewer.setContentProvider(treeViewerContentProvider);

    TreeViewerColumn nameColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
    nameColumn.setLabelProvider(new MessageKeyTreeNameColumnLabelProvider(messageKeyService,
        mk -> modifiedMessages.containsKey(mk.getCode())));

    treeColumnLayout.setColumnData(nameColumn.getColumn(), new ColumnWeightData(1, 750));


    Composite messageArea = new Composite(sashForm, SWT.NONE);
    messageArea.setBackground(
        messageArea.getShell().getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
    GridLayoutFactory.swtDefaults().numColumns(2).applyTo(messageArea);

    Composite messageArgArea = new Composite(messageArea, SWT.NONE);
    GridDataFactory.fillDefaults().span(2, 1).grab(true, false).applyTo(messageArgArea);
    GridLayoutFactory.fillDefaults().applyTo(messageArgArea);

    CLabel messageArgumentsHeader = new CLabel(messageArgArea, SWT.NONE);
    messageArgumentsHeader.setFont(defaultBoldStylerProvider.getBoldFont());
    messageArgumentsHeader.setText("Message Arguments:");
    messageArgumentsHeader.setMargins(0, 5, 0, 0);
    GridDataFactory.fillDefaults().grab(true, false).applyTo(messageArgumentsHeader);

    messageArgsInfo = new StyledText(messageArgArea, SWT.READ_ONLY | SWT.MULTI | SWT.WRAP);
    messageArgsInfo.setMargins(10, 0, 10, 0);
    GridDataFactory.fillDefaults().grab(true, false).hint(20, SWT.DEFAULT).applyTo(messageArgsInfo);


    Label messageAreaSeparator = new Label(messageArea, SWT.SEPARATOR | SWT.HORIZONTAL);
    GridDataFactory.fillDefaults().span(2, 1).grab(true, false).applyTo(messageAreaSeparator);


    CLabel messagesHeader = new CLabel(messageArea, SWT.NONE);
    messagesHeader.setFont(defaultBoldStylerProvider.getBoldFont());
    messagesHeader.setText("Messages:");
    messagesHeader.setMargins(0, 5, 0, 0);
    GridDataFactory.fillDefaults().grab(true, false).applyTo(messagesHeader);

    Button legendButton =
        MessageProblemLegendPopup.createLegendButton(messageArea, defaultBoldStylerProvider);
    GridDataFactory.fillDefaults().applyTo(legendButton);

    messageAreaScroll = new ScrolledComposite(messageArea, SWT.V_SCROLL | SWT.BORDER);
    GridDataFactory.fillDefaults().span(2, 1).grab(true, true).applyTo(messageAreaScroll);
    messageAreaScroll.setExpandHorizontal(true);
    messageAreaScroll.setExpandVertical(true);
    messageFieldArea = new Composite(messageAreaScroll, SWT.NONE);
    GridLayoutFactory.swtDefaults().numColumns(5).applyTo(messageFieldArea);
    messageAreaScroll.setContent(messageFieldArea);

    treeViewer.addSelectionChangedListener(event ->
    {
      Object element = event.getStructuredSelection().getFirstElement();
      if (element instanceof IdeMessageKey imk)
        selectedKey = imk;

      updateMessageArea(selectedKey);
    });



    searchBarTextEventDelay = new EventDelay(searchBarText);
    searchBarTextEventDelay.addListener(SWT.Modify, event ->
    {
      treeViewer.refresh();
    }, 1000);

    searchBarText.addSelectionListener(SelectionListener.widgetDefaultSelectedAdapter(event ->
    {
      searchBarTextEventDelay.flushAll();
    }));

    searchBarClearButton.setEnabled(false);
    searchBarText.addModifyListener(event ->
    {
      searchBarClearButton.setEnabled(!searchBarText.getText().isEmpty());
    });

    searchBarText.addKeyListener(new KeyAdapter()
    {

      @Override
      public void keyPressed(KeyEvent e)
      {
        if (e.keyCode == SWT.ESC)
        {
          searchBarText.setText("");
          searchBarTextEventDelay.flushAll();
        }
      }

    });

    searchBarClearButton.addSelectionListener(SelectionListener.widgetSelectedAdapter(event ->
    {
      searchBarText.setText("");
      searchBarTextEventDelay.flushAll();
    }));


    toolbarManager = new ToolBarManager(treeToolbar);
    getSite().getService(IMenuService.class).populateContributionManager(toolbarManager,
        "toolbar:de.codecamp.messages.messagebundleeditor.toolbar");

    MenuManager contextMenuManager = new MenuManager();
    Menu contextMenu = contextMenuManager.createContextMenu(treeViewer.getTree());
    treeViewer.getTree().setMenu(contextMenu);
    getSite().registerContextMenu(MessageKeysView.CONTEXTMENU_ID, contextMenuManager, treeViewer);
    getSite().setSelectionProvider(treeViewer);

    treeViewer.setInput(messageKeyService.getModule(getProjectFromInput()).get());

    updateMessageAreaLocales();
    reloadMessageBundles();

    treeViewer.getTree().getShell().getDisplay().asyncExec(() ->
    {
      /*
       * The Tree doesn't have a selection yet, but it automatically selects its first item some
       * time later. A Tree with SWT.SINGLE apparently always requires a selection as long as it has
       * items. However, this does not fire a selection event. So re-set the same selection to
       * trigger the event.
       */
      treeViewer.setSelection(treeViewer.getSelection());
    });

    getSite().getService(ISelectionService.class).addPostSelectionListener((part, selection) ->
    {
      if (selection.isEmpty())
        return;

      ICompilationUnit compilationUnit = null;

      if (JavaUI.ID_CU_EDITOR.equals(part.getSite().getId())
          && part instanceof IEditorPart editorPart)
      {
        IJavaElement javaElement = JavaUI.getEditorInputJavaElement(editorPart.getEditorInput());
        if (javaElement instanceof ICompilationUnit cu)
        {
          compilationUnit = cu;
        }
      }
      else if (selection instanceof IStructuredSelection ss)
      {
        if (ss.getFirstElement() instanceof ICompilationUnit cu)
          compilationUnit = cu;
      }

      if (compilationUnit != null)
      {
        try
        {
          IType[] types = compilationUnit.getTypes();
          if (types.length > 0)
          {
            setFilterText(types[0].getFullyQualifiedName());
          }
        }
        catch (@SuppressWarnings("unused") JavaModelException ex)
        {
          // ignore
        }
      }
    });

    loadState();
  }

  private IdeMessageKey getSelectedMessageKey()
  {
    Object element = treeViewer.getStructuredSelection().getFirstElement();
    if (element instanceof IdeMessageKey)
      return (IdeMessageKey) element;
    else
      return null;
  }

  public void setFilterText(String filterText)
  {
    searchBarText.setText(filterText);
    searchBarTextEventDelay.flushAll();
  }

  public void selectMessageKey(String messageKeyCode, Locale locale, Point location)
  {
    TreePath treePath = treeViewerContentProvider.getTreePathFor(messageKeyCode);
    if (treePath == null)
      return;

    setFilterText("");

    treeViewer.expandToLevel(treePath, 0);
    treeViewer.setSelection(new TreeSelection(treePath), true);

    if (locale == null)
      locale = Locale.ROOT;

    Text messageField = messageFields.get(locale);
    if (messageField != null)
    {
      messageField.setFocus();
      messageField.setSelection(messageField.getText().length());

      if (location != null)
        messageField.setSelection(location);
    }
  }

  private void updateMessageAreaLocales()
  {
    IProject project = getProjectFromInput();
    IdeProjectConf projectConf = messageKeyService.getProjectConf(project).get();

    messageFieldArea.setRedraw(false);
    try
    {
      Stream.of(messageFieldArea.getChildren()).forEach(Control::dispose);


      List<Locale> locales = new ArrayList<>();
      locales.add(Locale.ROOT);
      locales.addAll(projectConf.getTargetLocales());

      boolean first = true;
      for (Locale targetLocale : locales)
      {
        if (first)
          first = false;
        else
        {
          Label spacer = new Label(messageFieldArea, SWT.NONE);
          GridDataFactory.fillDefaults().span(5, 1).hint(SWT.DEFAULT, 10).applyTo(spacer);
        }

        StyledText messageLabel = new StyledText(messageFieldArea, SWT.NONE);
        GridDataFactory.fillDefaults().span(5, 1).grab(true, false).align(SWT.FILL, SWT.CENTER)
            .applyTo(messageLabel);
        messageLabel
            .setBackground(messageLabel.getDisplay().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
        messageLabels.put(targetLocale, messageLabel);

        Button emptyMessageCheckbox = new Button(messageFieldArea, SWT.CHECK);
        GridDataFactory.fillDefaults().span(2, 1).grab(true, false).align(SWT.END, SWT.FILL)
            .applyTo(emptyMessageCheckbox);
        emptyMessageCheckbox.setText("empty");
        emptyMessageCheckbox.setToolTipText(
            "Sets the message text to a zero-width whitespace character to mark it as intentionally empty.\nThat way validation will not complain about a missing translation.");
        messageEmptyCheckbox.put(targetLocale, emptyMessageCheckbox);

        ToolBarManager messageToolBar = new ToolBarManager(SWT.FLAT);
        GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER)
            .applyTo(messageToolBar.createControl(messageFieldArea));


        messageLabel.setEditable(false);
        messageLabel.setFont(defaultFont);

        Text messageField = new Text(messageFieldArea, SWT.BORDER | SWT.MULTI | SWT.H_SCROLL);
        GridDataFactory.fillDefaults().span(5, 1).hint(100, -1).grab(true, false)
            .applyTo(messageField);
        messageFields.put(targetLocale, messageField);

        // resize message field according to line count
        messageField.addModifyListener(event ->
        {
          Integer oldLineCount = (Integer) messageField.getData(DATA_LINE_COUNT);
          int newLineCount = messageField.getLineCount();
          if (oldLineCount == null || oldLineCount != newLineCount)
          {
            GridData layoutData = (GridData) messageField.getLayoutData();
            layoutData.heightHint = messageField.getLineHeight() * newLineCount;
            messageField.requestLayout();
            messageAreaScroll.setMinSize(messageFieldArea.computeSize(SWT.DEFAULT, SWT.DEFAULT));
            messageField.setData(DATA_LINE_COUNT, newLineCount);
          }
        });

        messageField.addFocusListener(FocusListener.focusGainedAdapter(event ->
        {
          selectedLocale = targetLocale;
        }));

        messageField.addListener(SWT.MouseUp, event ->
        {
          Text text = (Text) event.widget;
          selectedTextLocation = text.getSelection();
        });
        messageField.addListener(SWT.KeyUp, event ->
        {
          Text text = (Text) event.widget;
          selectedTextLocation = text.getSelection();
        });


        Action resetMessageAction = new Action()
        {

          @Override
          public void runWithEvent(Event event)
          {
            messageField.setText(
                StringUtils.defaultString((String) messageField.getData(DATA_ORIGINAL_MESSAGE)));
          }

        };
        resetMessageAction.setToolTipText("Reset Message");
        resetMessageAction
            .setImageDescriptor(MessagesPlugin.getDefault().getImageDescriptor("/icons/undo.png"));
        messageToolBar.add(resetMessageAction);
        messageResetAction.put(targetLocale, resetMessageAction);

        Action removeKeyAction = new Action()
        {

          @Override
          public void runWithEvent(Event event)
          {
            messageRemoveKeyAction.get(targetLocale).setEnabled(false);
            messageField.setData(DATA_REMOVE, DATA_REMOVE);
            messageField.setText("");
          }

        };
        removeKeyAction.setToolTipText(
            "Remove message key from bundle file of this locale. Only if locale is not required.");
        removeKeyAction.setImageDescriptor(
            MessagesPlugin.getDefault().getImageDescriptor("/icons/message_key_remove.png"));
        messageToolBar.add(removeKeyAction);
        messageRemoveKeyAction.put(targetLocale, removeKeyAction);

        MenuManager messageArgToolbarManager = new MenuManager();
        IAction messageArgMenuAction = new Action(null, IAction.AS_DROP_DOWN_MENU)
        {

          @Override
          public void runWithEvent(Event event)
          {
            Widget widget = event.widget;
            if (widget instanceof ToolItem)
            {
              ToolItem toolItem = (ToolItem) widget;
              Composite parent = toolItem.getParent();
              Rectangle rect = toolItem.getBounds();
              Point pos = parent.toDisplay(new Point(rect.x, rect.y + rect.height));
              Menu contextMenu = messageArgToolbarManager.createContextMenu(parent);
              contextMenu.setLocation(pos.x, pos.y);
              contextMenu.setVisible(true);
            }
          }

        };
        messageArgMenuAction.setToolTipText("Insert a message argument.");
        messageArgMenuAction.setImageDescriptor(
            MessagesPlugin.getDefault().getImageDescriptor("/icons/message_arguments.png"));
        messageArgMenu.put(targetLocale, messageArgMenuAction);
        messageArgToolbar.put(targetLocale, messageArgToolbarManager);
        messageToolBar.add(messageArgMenuAction);
        messageToolBar.update(false);


        emptyMessageCheckbox.addSelectionListener(SelectionListener.widgetSelectedAdapter(event ->
        {
          if (emptyMessageCheckbox.getSelection())
            messageField.setText(EMPTY_MESSAGE);
          else
            messageField.setText("");
        }));

        messageField.addModifyListener(event ->
        {
          String newMessage = messageField.getText();
          if (messageField.getData(DATA_REMOVE) != null)
          {
            messageField.setData(DATA_REMOVE, null);
            newMessage = null;
          }
          if (newMessage != null && !Boolean.TRUE.equals(messageField.getData(DATA_REQUIRED)))
          {
            messageRemoveKeyAction.get(targetLocale).setEnabled(true);
          }

          IdeMessageKey messageKey = getSelectedMessageKey();
          if (messageKey == null)
            return;

          String originalMessage = (String) messageField.getData(DATA_ORIGINAL_MESSAGE);

          boolean modified;
          if (Objects.equals(originalMessage, newMessage))
          {
            Map<Locale, String> modifiedMessagesForCode =
                modifiedMessages.get(messageKey.getCode());
            if (modifiedMessagesForCode != null)
            {
              modifiedMessagesForCode.remove(targetLocale);
              if (modifiedMessagesForCode.isEmpty())
                modifiedMessages.remove(messageKey.getCode());
            }
            modified = false;
          }
          else
          {
            Map<Locale, String> modifiedMessagesForCode =
                modifiedMessages.computeIfAbsent(messageKey.getCode(), k -> new HashMap<>());
            modifiedMessagesForCode.put(targetLocale, newMessage);
            modified = true;
          }

          if (EMPTY_MESSAGE.equals(newMessage))
          {
            emptyMessageCheckbox.setSelection(true);
            messageField.setEditable(false);
          }
          else
          {
            if (emptyMessageCheckbox.getSelection())
              emptyMessageCheckbox.setSelection(false);
            messageField.setEditable(true);
          }

          messageResetAction.get(targetLocale).setEnabled(modified);

          firePropertyChange(PROP_DIRTY);
          treeViewer.update(messageKey, null);
          updateMessageAreaLocaleLabel(messageKey, targetLocale, modified);
        });

        updateMessageAreaLocaleLabel(null, targetLocale, false);
      }

      messageAreaScroll.setMinSize(messageFieldArea.computeSize(SWT.DEFAULT, SWT.DEFAULT));
      messageFieldArea.layout();
    }
    finally
    {
      messageFieldArea.setRedraw(true);
    }
  }

  private void updateMessageAreaLocaleLabel(IdeMessageKey messageKey, Locale locale,
      boolean modified)
  {
    StyledString labelText = new StyledString();

    if (modified)
    {
      labelText.append("* ", defaultBoldStylerProvider.getBoldStyler());
    }


    String localeText;
    if (Locale.ROOT.equals(locale))
      localeText = "Base";
    else
      localeText = locale.toString();

    if (messageKey != null)
    {
      labelText.append(localeText, MessageKeysView.getStyler(messageKeyService, messageKey, locale,
          defaultBoldStylerProvider));
    }
    else
    {
      labelText.append(localeText, defaultBoldStylerProvider.getBoldStyler());
    }

    if (!Locale.ROOT.equals(locale))
    {
      labelText.append(" - ").append(locale.getDisplayLanguage());
      if (!locale.getDisplayCountry().isEmpty())
      {
        labelText.append(" (");
        labelText.append(locale.getDisplayCountry());
        if (!locale.getDisplayVariant().isEmpty())
        {
          labelText.append(", ");
          labelText.append(locale.getDisplayVariant());
        }
        labelText.append(")");
      }
    }

    StyledText messageLabel = messageLabels.get(locale);
    messageLabel.setText(labelText.getString());
    messageLabel.setStyleRanges(labelText.getStyleRanges());

    messageLabel.setToolTipText(
        MessageUiUtils.getBundleErrorsTooltipText(messageKeyService, messageKey, locale));
  }


  private void updateMessageArea(IdeMessageKey messageKey)
  {
    if (messageKey == null)
    {
      messageArgsInfo.setText("");
      messageFields.forEach((locale, messageField) ->
      {
        messageField.setEnabled(false);
        messageField.setData(DATA_ORIGINAL_MESSAGE, null);
        messageField.setData(DATA_REMOVE, null);
        messageField.setData(DATA_REQUIRED, null);
        messageField.setText("");
        messageResetAction.get(locale).setEnabled(false);
        messageEmptyCheckbox.get(locale).setEnabled(false);
        messageRemoveKeyAction.get(locale).setEnabled(false);
      });
    }
    else
    {
      IProject project = getProjectFromInput();
      IdeProjectConf projectConf = messageKeyService.getProjectConf(project, true).get();

      MessageFormatSupport messageFormatSupport = MessageFormatSupport.getSupport(projectConf);

      StyledString text = new StyledString();
      if (messageKey.hasArgs())
      {
        String[] argNames = messageKey.getArgNames();
        String[] argTypes = messageKey.getArgTypes();
        for (int i = 0; i < argNames.length; i++)
        {
          if (i > 0)
            text.append(" | ");

          String argName = argNames[i];
          String argType = argTypes[i];

          if (argName != null)
            // use non-breaking space to improve line wrapping
            text.append(argName).append("\u00A0");
          text.append(messageFormatSupport.formatArgType(argType), StyledString.COUNTER_STYLER);
        }
      }
      else
      {
        text.append("none", StyledString.QUALIFIER_STYLER);
      }
      messageArgsInfo.setText(text.getString());
      messageArgsInfo.setStyleRanges(text.getStyleRanges());
      messageArgsInfo.requestLayout();


      boolean hasTargetBundle = projectConf.toTargetBundleName(messageKey.getCode()).isPresent();

      messageFields.forEach((locale, messageField) ->
      {
        messageField.setEnabled(true);

        IdeMessage message = getMessage(messageKey, locale);

        String messageText = "";
        if (message != null)
        {
          messageText = message.getMessage();
        }
        messageField.setData(DATA_ORIGINAL_MESSAGE, messageText);
        messageField.setData(DATA_REMOVE, null);
        messageField.setData(DATA_REQUIRED, null);

        boolean modified = false;

        Map<Locale, String> modifiedMessagesForCode = modifiedMessages.get(messageKey.getCode());
        if (modifiedMessagesForCode != null)
        {
          String modifiedMessage = modifiedMessagesForCode.get(locale);
          if (modifiedMessage != null)
          {
            modified = true;
            messageText = modifiedMessage;
          }
        }


        MenuManager argToolbar = messageArgToolbar.get(locale);
        argToolbar.removeAll();
        List<ArgInsert> options = messageFormatSupport.getArgInsertOptions(messageKey);
        messageArgMenu.get(locale).setEnabled(!options.isEmpty());
        for (ArgInsert argInsert : options)
        {
          argToolbar.add(new Action(argInsert.getLabel())
          {

            @Override
            public void run()
            {
              messageField.insert(argInsert.getReference());
            }

          });
        }


        MenuManager messageFieldContextMenu = new MenuManager();
        messageFieldContextMenu.setRemoveAllWhenShown(true);
        messageFieldContextMenu.addMenuListener(new IMenuListener()
        {

          @Override
          public void menuAboutToShow(IMenuManager manager)
          {
            List<ArgInsert> options = messageFormatSupport.getArgInsertOptions(messageKey);
            for (ArgInsert argInsert : options)
            {
              manager.add(new Action(argInsert.getLabel())
              {

                @Override
                public void run()
                {
                  messageField.insert(argInsert.getReference());
                }

              });
            }
          }

        });
        messageField.setMenu(messageFieldContextMenu.createContextMenu(messageField));


        boolean required = false;

        Optional<Set<Locale>> requiredLocalesOpt = messageKeyService.getRequiredLocales(messageKey);
        if (requiredLocalesOpt.isPresent() && requiredLocalesOpt.get().contains(locale))
          required = true;
        if (!locale.equals(Locale.ROOT))
        {
          IdeMessage baseMessage = getMessage(messageKey, Locale.ROOT);
          if (baseMessage != null)
            required = false;
        }

        messageField.setData(DATA_REQUIRED, required);


        // this will implicitly trigger updateMessageAreaLocaleLabel
        messageField.setText(messageText);

        // move selection/cursor to end of message text
        messageField.setSelection(messageText.length());

        messageField.setEnabled(hasTargetBundle);
        messageResetAction.get(locale).setEnabled(hasTargetBundle && modified);
        messageEmptyCheckbox.get(locale).setEnabled(hasTargetBundle);
        messageRemoveKeyAction.get(locale)
            .setEnabled(hasTargetBundle && message != null && !required);
      });
    }
  }

  private void reloadMessageBundles()
  {
    IProject project = getProjectFromInput();
    IdeProjectConf projectConf = messageKeyService.getProjectConf(project).get();

    EclipseMessageBundleManager bundleManager = projectConf.createEclipseMessageBundleManager();

    messages.clear();

    boolean first = true;
    for (BundleFile<IFile, CoreException> bundleFile : bundleManager.getBundleFiles())
    {
      if (first)
      {
        if (getEditorInput() instanceof IProjectEditorInput)
          ((IProjectEditorInput) getEditorInput()).setLinkedFile(bundleFile.getLocation());
        first = false;
      }

      for (String code : bundleFile.getKeys())
      {
        messages.add(new IdeMessage(code, bundleFile.getLocale(), bundleFile.getMessage(code)));
      }
    }
  }

  public IdeMessage getMessage(IdeMessageKey messageKey, Locale locale)
  {
    Query<IdeMessage> query = and(equal(MESSAGE_ATTR_MESSAGE_KEY_CODE, messageKey.getCode()),
        equal(MESSAGE_ATTR_LOCALE, locale));

    QueryOptions queryOptions =
        queryOptions(applyThresholds(threshold(EngineThresholds.INDEX_ORDERING_SELECTIVITY, 1.0)));
    try (Stream<IdeMessage> stream = messages.retrieve(query, queryOptions).stream())
    {
      return stream.findFirst().orElse(null);
    }
  }

  @Override
  public String getTitleToolTip()
  {
    IProject project = getProjectFromInput();
    IdeProjectConf projectConf = messageKeyService.getProjectConf(project).orElse(null);
    if (projectConf != null)
    {
      return "Project name: " + project.getName() + "\n" + "Module name: "
          + projectConf.getModuleName();
    }
    else
    {
      return super.getTitleToolTip();
    }
  }


  @Override
  public void setFocus()
  {
    if (treeViewer != null)
      treeViewer.getControl().setFocus();
  }

  @Override
  public boolean isDirty()
  {
    return !modifiedMessages.isEmpty();
  }

  @Override
  public void doSave(IProgressMonitor monitor)
  {
    IProject project = getProjectFromInput();
    IdeProjectConf projectConf = messageKeyService.getProjectConf(project, true).get();

    EclipseMessageBundleManager bundleManager = projectConf.createEclipseMessageBundleManager();

    modifiedMessages.forEach((code, modifiedMessagesForCode) ->
    {
      modifiedMessagesForCode.forEach((locale, message) ->
      {
        if (message == null)
          bundleManager.removeMessage(code, locale);
        else
          bundleManager.setMessage(code, locale, message);
      });
    });

    bundleManager.save();
    modifiedMessages.clear();
  }

  @Override
  public boolean isSaveAsAllowed()
  {
    return false;
  }

  @Override
  public void doSaveAs()
  {
    throw new UnsupportedOperationException();
  }


  private void loadState()
  {
    IPreferenceStore preferenceStore = MessagesPlugin.getDefault().getPreferenceStore();

    preferenceStore.setDefault(VIEWSTATE_GROUP_BY_SOURCE_TYPE, STATE_GROUP_BY_SOURCE_TYPE_DEFAULT);
    preferenceStore.setDefault(VIEWSTATE_SHOW_IMPORTED_KEYS, STATE_SHOW_IMPORTED_KEYS_DEFAULT);
    preferenceStore.setDefault(VIEWSTATE_GROUP_BY_MODULES, STATE_GROUP_BY_MODULES_DEFAULT);
    preferenceStore.setDefault(VIEWSTATE_SHOW_ONLY_KEYS_WITH_PROBLEMS,
        STATE_SHOW_ONLY_KEYS_WITH_PROBLEMS_DEFAULT);


    treeViewerContentProvider
        .setGroupBySourceType(preferenceStore.getBoolean(VIEWSTATE_GROUP_BY_SOURCE_TYPE));
    treeViewerContentProvider
        .setShowImportedKeys(preferenceStore.getBoolean(VIEWSTATE_SHOW_IMPORTED_KEYS));
    treeViewerContentProvider
        .setGroupByModules(preferenceStore.getBoolean(VIEWSTATE_GROUP_BY_MODULES));
    treeViewerContentProvider.setShowOnlyKeysWithProblems(
        preferenceStore.getBoolean(VIEWSTATE_SHOW_ONLY_KEYS_WITH_PROBLEMS));
  }

  private void saveState()
  {
    IPreferenceStore preferenceStore = MessagesPlugin.getDefault().getPreferenceStore();

    preferenceStore.setValue(VIEWSTATE_GROUP_BY_SOURCE_TYPE,
        treeViewerContentProvider.getGroupBySourceType());
    preferenceStore.setValue(VIEWSTATE_SHOW_IMPORTED_KEYS,
        treeViewerContentProvider.getShowImportedKeys());
    preferenceStore.setValue(VIEWSTATE_GROUP_BY_MODULES,
        treeViewerContentProvider.getGroupByModules());
    preferenceStore.setValue(VIEWSTATE_SHOW_ONLY_KEYS_WITH_PROBLEMS,
        treeViewerContentProvider.getShowOnlyKeysWithProblems());
  }


  @Override
  public <T> T getAdapter(Class<T> adapter)
  {
    if (adapter == MessageKeyTreeAdapter.class)
      return adapter.cast(new MessageKeyTreeAdapter(treeViewer, treeViewerContentProvider));

    return super.getAdapter(adapter);
  }

}
