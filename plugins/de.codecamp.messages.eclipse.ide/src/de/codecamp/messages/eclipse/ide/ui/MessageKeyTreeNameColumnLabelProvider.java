package de.codecamp.messages.eclipse.ide.ui;


import de.codecamp.messages.eclipse.ide.MessagesPlugin;
import de.codecamp.messages.eclipse.ide.services.IdeMessageKey;
import de.codecamp.messages.eclipse.ide.services.IdeModule;
import de.codecamp.messages.eclipse.ide.services.IdeSourceType;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import java.util.function.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IMarker;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ViewerColumn;
import org.eclipse.swt.graphics.Image;


public class MessageKeyTreeNameColumnLabelProvider
  extends
    ColumnLabelProvider
{

  private final MessageKeyService messageKeyService;

  private final Predicate<IdeMessageKey> isModifiedProvider;

  private MessageKeyTreeContentProvider contentProvider;


  public MessageKeyTreeNameColumnLabelProvider(MessageKeyService messageKeyService)
  {
    this(messageKeyService, null);
  }

  public MessageKeyTreeNameColumnLabelProvider(MessageKeyService messageKeyService,
      Predicate<IdeMessageKey> isModifiedProvider)
  {
    this.messageKeyService = messageKeyService;
    this.isModifiedProvider = isModifiedProvider;
  }


  @Override
  protected void initialize(ColumnViewer viewer, ViewerColumn column)
  {
    super.initialize(viewer, column);
    this.contentProvider = (MessageKeyTreeContentProvider) viewer.getContentProvider();
  }


  @Override
  public String getText(Object element)
  {
    if (element instanceof IdeModule)
    {
      IdeModule module = (IdeModule) element;

      String label = "";
      if (!module.isImported())
        label = module.getProject().getName();

      if (module.getModuleName() != null)
      {
        if (!label.isEmpty())
          label += " / ";

        label += module.getModuleName();
      }

      label += " [" + messageKeyService.countMessageKeys(module, false, null,
          contentProvider.getShowOnlyKeysWithProblems());

      if (contentProvider.getShowImportedKeys())
      {
        label += " / " + messageKeyService.countMessageKeys(module, true, null,
            contentProvider.getShowOnlyKeysWithProblems());
      }
      label += "]";

      return label;
    }
    else if (element instanceof IdeSourceType)
    {
      IdeSourceType sourceType = (IdeSourceType) element;
      return sourceType.getSourceTypeName();
    }
    else if (element instanceof IdeMessageKey)
    {
      IdeMessageKey messageKey = (IdeMessageKey) element;

      StringBuilder sb = new StringBuilder();
      if (isModifiedProvider != null && isModifiedProvider.test(messageKey))
        sb.append("* ");

      if (contentProvider.getGroupBySourceType())
      {
        if (messageKey.getCode().equals(messageKey.getSourceType()))
        {
          sb.append(Dialog.ELLIPSIS)
              .append(StringUtils.substringAfterLast(messageKey.getCode(), "."));
        }
        else
        {
          String withoutPrefix =
              StringUtils.removeStart(messageKey.getCode(), messageKey.getSourceType() + ".");

          if (!withoutPrefix.equals(messageKey.getCode()))
            sb.append(Dialog.ELLIPSIS);

          sb.append(withoutPrefix);
        }
      }
      else
      {
        sb.append(messageKey.getCode());
      }
      return sb.toString();
    }
    else
    {
      return super.getText(element);
    }
  }

  @Override
  public Image getImage(Object element)
  {
    String baseImage = null;
    if (element instanceof IdeModule)
    {
      IdeModule module = (IdeModule) element;
      if (module.isImported())
      {
        if (module.isFromWorkspace())
          baseImage = "/icons/module_workspace_imported.png";
        else
          baseImage = "/icons/module_jar_imported.png";
      }
      else
      {
        if (module.isFromWorkspace())
          baseImage = "/icons/module_workspace.png";
        else
          baseImage = "/icons/module_jar.png";
      }
    }
    else if (element instanceof IdeSourceType)
    {
      IdeSourceType sourceType = (IdeSourceType) element;
      if (sourceType.isImported())
        baseImage = "/icons/source_type_imported.png";
      else
        baseImage = "/icons/source_type.png";
    }
    else if (element instanceof IdeMessageKey)
    {
      IdeMessageKey messageKey = (IdeMessageKey) element;
      if (messageKey.isImported())
      {
        if (messageKey.isFromWorkspace())
          baseImage = "/icons/messagekey_project.png";
        else
          baseImage = "/icons/messagekey_jar.png";
      }
      else
      {
        baseImage = "/icons/messagekey.png";
      }
    }

    if (baseImage != null)
    {
      IMarker marker = null;

      if (element instanceof IdeModule)
      {
        marker = messageKeyService
            .findMostSevereBundleValidationMarker(((IdeModule) element).getProject());
      }

      else if (element instanceof IdeSourceType)
      {
        IdeSourceType sourceType = (IdeSourceType) element;
        marker = messageKeyService.findMostSevereBundleValidationMarker(sourceType);
      }

      else if (element instanceof IdeMessageKey)
      {
        IdeMessageKey messageKey = (IdeMessageKey) element;
        marker = messageKeyService.findMostSevereBundleValidationMarker(messageKey, null);
      }

      int severity = marker == null ? -1 : marker.getAttribute(IMarker.SEVERITY, -1);

      String overlayImage = null;
      if (severity >= IMarker.SEVERITY_ERROR)
      {
        overlayImage = "platform:/plugin/org.eclipse.ui/icons/full/ovr16/error_ovr.png";
      }
      else if (severity >= IMarker.SEVERITY_WARNING)
      {
        /*
         * In the context of the editor or view, make problems more visible by using the error
         * overlay even for warnings.
         */
        overlayImage = "platform:/plugin/org.eclipse.ui/icons/full/ovr16/error_ovr.png";
        // overlayImage = "platform:/plugin/org.eclipse.ui/icons/full/ovr16/warning_ovr.png";
      }

      if (overlayImage != null)
      {
        return MessagesPlugin.getDefault().getOverlayImage(baseImage, overlayImage,
            IDecoration.BOTTOM_RIGHT);
      }
      else
      {
        return MessagesPlugin.getDefault().getImage(baseImage);
      }
    }
    else
    {
      return MessagesPlugin.getDefault().getImage("icons/blank_16.png");
    }
  }

  @Override
  public String getToolTipText(Object element)
  {
    return MessageUiUtils.getBundleErrorsTooltipText(messageKeyService, element);
  }

}
