package de.codecamp.messages.eclipse.ide.services;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.eclipse.core.resources.IProject;


public class IdeSourceType
  extends
    AbstractItem
{

  private final String sourceTypeName;


  public IdeSourceType(IProject project, String moduleName, String sourceTypeName, boolean imported,
      boolean fromWorkspace)
  {
    super(project, moduleName, imported, fromWorkspace);
    this.sourceTypeName = sourceTypeName;
  }


  public String getSourceTypeName()
  {
    return sourceTypeName;
  }


  @Override
  public int hashCode()
  {
    final int prime = 31;
    int result = 0;
    result = prime * result + getProject().getName().hashCode();
    result = prime * result + getModuleName().hashCode();
    result = prime * result + getSourceTypeName().hashCode();
    return result;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
      return false;
    if (obj == this)
      return true;
    if (getClass() != obj.getClass())
      return false;

    IdeSourceType other = (IdeSourceType) obj;
    if (!getProject().getName().equals(other.getProject().getName()))
      return false;
    if (!getModuleName().equals(other.getModuleName()))
      return false;
    if (!getSourceTypeName().equals(other.getSourceTypeName()))
      return false;

    return true;
  }


  @Override
  public String toString()
  {
    return new ToStringBuilder(this).append("project", getProject().getName())
        .append("module", getModuleName()).append("sourceTypeName", getSourceTypeName())
        .append("isImported", isImported()).append("isFromWorkspace", isFromWorkspace()).build();
  }

}
