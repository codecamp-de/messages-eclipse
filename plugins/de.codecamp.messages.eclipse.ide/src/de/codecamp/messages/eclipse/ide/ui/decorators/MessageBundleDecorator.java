package de.codecamp.messages.eclipse.ide.ui.decorators;


import de.codecamp.messages.eclipse.ide.MessagesPlugin;
import de.codecamp.messages.eclipse.ide.services.MessageKeyService;
import de.codecamp.messages.eclipse.ide.util.eclipse.E4Utils;
import javax.inject.Inject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;


public class MessageBundleDecorator
  implements
    ILightweightLabelDecorator
{

  private final ImageDescriptor descriptor =
      MessagesPlugin.getDefault().getImageDescriptor("/icons/language-small.png");


  @Inject
  private MessageKeyService messageKeyService;


  public MessageBundleDecorator()
  {
    E4Utils.injectOsgiContext(this);
  }


  @Override
  public void addListener(ILabelProviderListener listener)
  {
    // nothing to do
  }

  @Override
  public void removeListener(ILabelProviderListener listener)
  {
    // nothing to do
  }

  @Override
  public boolean isLabelProperty(Object element, String property)
  {
    return true;
  }

  @Override
  public void dispose()
  {
    // nothing to do
  }

  @Override
  public void decorate(Object element, IDecoration decoration)
  {
    if (!(element instanceof IResource))
      return;

    IResource resource = (IResource) element;
    messageKeyService.getProjectConf(resource.getProject()).ifPresent(pc ->
    {
      if (pc.isMessageBundleDir(resource) || pc.isMessageBundleFile(resource))
      {
        decoration.addOverlay(descriptor, IDecoration.BOTTOM_RIGHT);
      }
    });
  }

}
