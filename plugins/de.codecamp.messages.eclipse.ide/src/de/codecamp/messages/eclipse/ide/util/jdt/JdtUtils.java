package de.codecamp.messages.eclipse.ide.util.jdt;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.internal.ui.search.JavaSearchQuery;
import org.eclipse.jdt.internal.ui.search.JavaSearchScopeFactory;
import org.eclipse.jdt.ui.search.ElementQuerySpecification;
import org.eclipse.jdt.ui.search.QuerySpecification;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.NewSearchUI;


@SuppressWarnings("restriction")
public final class JdtUtils
{

  private JdtUtils()
  {
    // utility class
  }


  public static boolean isJavaProject(IProject project)
  {
    try
    {
      return project.hasNature(JavaCore.NATURE_ID);
    }
    catch (CoreException ex)
    {
      return false;
    }
  }

  public static Optional<IJavaProject> getJavaProject(IProject project)
  {
    if (!isJavaProject(project))
      return Optional.empty();

    return Optional.ofNullable(JavaCore.create(project));
  }

  public static boolean inSourceFolder(IResource resource)
    throws JavaModelException
  {
    if (!isJavaProject(resource.getProject()))
      return false;

    boolean inSourceFolder = false;
    IJavaProject javaProject = JavaCore.create(resource.getProject());
    for (IPackageFragmentRoot root : javaProject.getAllPackageFragmentRoots())
    {
      if (root.getKind() == IPackageFragmentRoot.K_SOURCE
          && root.getPath().isPrefixOf(resource.getFullPath()))
      {
        inSourceFolder = true;
        break;
      }
    }
    return inSourceFolder;
  }

  /**
   * Starts a search for references to the given Java elements and presents the results in the
   * Search view.
   *
   * @param javaElements
   *          the Java elements to search for
   */
  public static void searchForReferences(List<IJavaElement> javaElements)
  {
    JavaSearchScopeFactory factory = JavaSearchScopeFactory.getInstance();

    List<QuerySpecification> querySpecs = new ArrayList<>();
    for (IJavaElement javaElement : javaElements)
    {
      boolean isInsideJRE = factory.isInsideJRE(javaElement);
      IJavaSearchScope scope = factory.createWorkspaceScope(isInsideJRE);
      String description = factory.getWorkspaceScopeDescription(isInsideJRE);
      QuerySpecification querySpec = new ElementQuerySpecification(javaElement,
          IJavaSearchConstants.REFERENCES, scope, description);
      querySpecs.add(querySpec);
    }

    ISearchQuery javaSearch = new JavaSearchQuery(querySpecs);

    NewSearchUI.runQueryInBackground(javaSearch);
  }

}
