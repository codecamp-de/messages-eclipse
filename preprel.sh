if [ $# -ne 2 ]
  then
    echo "Release and next snapshot version not specified."
    exit
fi

RELEASE_VERSION=$1
NEXT_VERSION=$2
RELEASE_TAG="release-${RELEASE_VERSION}"

./mvnw org.eclipse.tycho:tycho-versions-plugin:set-version -DnewVersion=${RELEASE_VERSION}
sed -i "s:<version>.*</version>:<version>${RELEASE_VERSION}</version>:g" README.md
sed -i "s:<messages.version>.*</messages.version>:<messages.version>${RELEASE_VERSION}</messages.version>:g" README.md

git add -u
git commit -m "RELEASE: ${RELEASE_VERSION}"
git tag -a -m "RELEASE: ${RELEASE_VERSION}" ${RELEASE_TAG}


./mvnw org.eclipse.tycho:tycho-versions-plugin:set-version -DnewVersion=${NEXT_VERSION}-SNAPSHOT --offline
sed -i "s:<version>.*</version>:<version>${NEXT_VERSION}-SNAPSHOT</version>:g" README.md
sed -i "s:<messages.version>.*</messages.version>:<messages.version>${NEXT_VERSION}-SNAPSHOT</messages.version>:g" README.md

git add -u
git commit -m "Prepare next snapshot version."
