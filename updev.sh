cd ../messages

./mvnw clean install

cd ../messages-eclipse

./mvnw -pl releng/targetplatform clean install

# remove the --offline if the build doesn't work (required for the first time and major changes)
if [ "$1" = "full" ];
then
  echo "---- online build ----"
  ./mvnw dependency:copy-dependencies -pl plugins/de.codecamp.messages.eclipse.ide
else
  echo "---- offline build ----"
  ./mvnw dependency:copy-dependencies -pl plugins/de.codecamp.messages.eclipse.ide --offline
fi
